//---------------------------------------------------------------------------

#ifndef Osc3H
#define Osc3H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <Buttons.hpp>
#include <Grids.hpp>
#include <Graphics.hpp>
#include "Word_2K_SRVR.h"
#include <OleServer.hpp>
#include <Db.hpp>
#include <DBGrids.hpp>
#include <DBTables.hpp>
#include <DB.hpp>
#include <Dialogs.hpp>

//---------------------------------------------------------------------------
class TFMain : public TForm
{
__published:	// IDE-managed Components
   TMainMenu *MainMenu1;
   TMenuItem *File1;
   TMenuItem *Options1;
   TMenuItem *ReSetAudio1;
   TMenuItem *Help1;
   TMenuItem *Tutor1;
   TMenuItem *About1;
   TMenuItem *Open1;
   TMenuItem *Save1;
   TMenuItem *Exit1;
   TTimer *TimerToRead;
   TPanel *Panel3;
   TPanel *PanTri;
   TImage *ImTri;
   TPanel *Panel2;
   TPanel *Panel4;
   TPanel *Panel5;
   TPanel *PanAudioPref;
   TLabel *LSFormat;
   TRadioGroup *RGSFormat;
   TComboBox *CBDevice;
   TButton *Hide;
   TPanel *Panel1;
   TImage *ImFA;
   TImage *ImS;
   TPanel *Panel6;
   TPanel *Panel7;
   TLabel *LProcessStatus;
   TLabel *LSFileName;
   TSpeedButton *SpeedButton3;
   TSpeedButton *SpeedButton4;
   TLabel *LOfftU;
   TSpeedButton *SpeedButton1;
   TSpeedButton *SpeedButton2;
   TLabel *LDenc;
   TSpeedButton *SpeedButton6;
   TSpeedButton *SpeedButton5;
   TLabel *LUZoom;
   TLabel *Label6;
   TCheckBox *CBLX;
   TCheckBox *CBLY;
   TSpeedButton *SpeedButton7;
   TSpeedButton *SpeedButton8;
   TPanel *PanS;
   TPanel *PanFA;
   TPanel *PanFB;
   TPanel *PanFC;
   TLabel *Lvoff;
   TDBGrid *DBGrid1;
   TTable *TableA;
   TDatabase *Database1;
   TDataSource *DataSource1;
   TQuery *Query1;
   TPanel *Panel8;
   TButton *BGrHide;
   TButton *BCalc;
   TImage *IgA;
   TImage *IgB;
   TImage *IgC;
   TImage *IiA;
   TImage *IiB;
   TImage *IiC;
   TImage *IgtA;
   TImage *IgtB;
   TImage *IgtC;
   TImage *IitA;
   TImage *IitB;
   TImage *IitC;
   TSpeedButton *SpeedButton9;
   TSpeedButton *SpeedButton10;
   TLabel *Lioff;
   TLabel *LIZoom;
   TCheckBox *CBUZ;
   TCheckBox *CBIZ;
   TLabel *LOfftI;
   TSpeedButton *SpeedButton11;
   TSpeedButton *SpeedButton12;
   TSpeedButton *BRecord;
    TLabel *LUi;
  TMenuItem *ParItm;
    TPanel *PanAB;
    TPanel *PanBC;
    TPanel *PanCA;
    TTable *TableB;
  TImage *IgAB;
  TImage *IgtAB;
  TImage *IgBC;
  TImage *IgtBC;
  TImage *IgCA;
  TImage *IgtCA;
  TMenuItem *GarmItm;
  TMenuItem *N1;
    TEdit *NEdit;
    TEdit *PEdit;
    TLabel *Label1;
    TDataSource *DataSource2;
    TLabel *Label2;
    TLabel *LUA;
    TLabel *LUB;
    TLabel *LUC;
    TLabel *LIA;
    TLabel *LIB;
    TLabel *LIC;
    TLabel *LUAB;
    TLabel *LUBC;
    TLabel *LUCA;
    TMenuItem *N2;
    TMenuItem *N3;
    TColorDialog *ColorDialog1;
    TPanel *ColorPan;
    TLabel *Label19;
    TLabel *Label3;
    TLabel *Label4;
    TLabel *Label5;
    TShape *UASh;
    TLabel *Label7;
    TLabel *Label8;
    TLabel *Label9;
    TLabel *Label20;
    TLabel *Label21;
    TLabel *Label22;
    TLabel *Label23;
    TLabel *Label24;
    TLabel *Label25;
    TLabel *Label26;
    TLabel *Label27;
    TLabel *Label28;
    TLabel *Label29;
    TLabel *Label30;
    TLabel *Label10;
    TLabel *Label11;
    TLabel *Label12;
    TLabel *Label13;
    TLabel *Label14;
    TLabel *Label15;
    TLabel *Label16;
    TLabel *Label17;
    TShape *UBSh;
    TShape *UCSh;
    TShape *UAgSh;
    TShape *UBgSh;
    TShape *UCgSh;
    TShape *IASh;
    TShape *IBSh;
    TShape *ICSh;
    TShape *IAgSh;
    TShape *IBgSh;
    TShape *ICgSh;
    TShape *UABSh;
    TShape *UBCSh;
    TShape *UCASh;
    TShape *UABgSh;
    TShape *UBCgSh;
    TShape *UCAgSh;
    TPanel *Panel9;
    TLabel *Label18;
    TLabel *Label31;
    TLabel *Label32;
    TLabel *Label33;
    TLabel *Label34;
    TShape *FonSh;
    TShape *AxisSh;
    TShape *SpSh;
    TShape *OXSh;
    TShape *OYSh;
    TShape *NetSh;
    TLabel *Label35;
    TLabel *Label36;
    TShape *TimeSh;
    TPanel *KoefPan;
    TEdit *Eksc;
    TEdit *EGarm;
    TEdit *EEth;
    TLabel *Label37;
    TLabel *Label38;
    TLabel *Label39;
    TEdit *EKUa;
    TLabel *Label40;
    TEdit *EKUb;
    TLabel *Label41;
    TEdit *EKUc;
    TLabel *Label42;
    TEdit *EKIa;
    TLabel *Label43;
    TEdit *EKIb;
    TLabel *Label44;
    TEdit *EKIc;
    TLabel *Label45;
    TEdit *EKUab;
    TLabel *Label46;
    TEdit *EKUbc;
    TLabel *Label47;
    TEdit *EKUca;
    TLabel *Label48;
    TLabel *Label49;
  TLabel *Label50;
  TLabel *Label51;
  TEdit *EESKoef;
        TLabel *Label52;
        TLabel *Label53;
   void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
   void __fastcall RGSFormatClick(TObject *Sender);
   void __fastcall BRecordClick(TObject *Sender);
   void __fastcall FormCreate(TObject *Sender);
   void __fastcall ReSetAudio1Click(TObject *Sender);
   void __fastcall HideClick(TObject *Sender);
   void __fastcall SpeedButton1Click(TObject *Sender);
   void __fastcall SpeedButton2Click(TObject *Sender);
   void __fastcall SpeedButton6Click(TObject *Sender);
   void __fastcall SpeedButton5Click(TObject *Sender);
   void __fastcall SpeedButton4Click(TObject *Sender);
   void __fastcall SpeedButton3Click(TObject *Sender);
   void __fastcall SpeedButton7Click(TObject *Sender);
   void __fastcall SpeedButton8Click(TObject *Sender);
   void __fastcall BCalcClick(TObject *Sender);
   void __fastcall BGrHideClick(TObject *Sender);
   void __fastcall CBLXClick(TObject *Sender);
   void __fastcall CBLYClick(TObject *Sender);
   void __fastcall ImFAClick(TObject *Sender);
   void __fastcall FormResize(TObject *Sender);
   void __fastcall PanSMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
   void __fastcall IitAMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
   void __fastcall DBGrid1CellClick(TColumn *Column);
   void __fastcall TimerToReadTimer(TObject *Sender);
   void __fastcall SpeedButton9Click(TObject *Sender);
   void __fastcall SpeedButton10Click(TObject *Sender);
   void __fastcall SpeedButton11Click(TObject *Sender);
   void __fastcall SpeedButton12Click(TObject *Sender);
   void __fastcall Open1Click(TObject *Sender);
   void __fastcall About1Click(TObject *Sender);
   void __fastcall Save1Click(TObject *Sender);
   void __fastcall PanSClick(TObject *Sender);
  void __fastcall Options1Click(TObject *Sender);
  void __fastcall N1Click(TObject *Sender);
    void __fastcall ParItmClick(TObject *Sender);
    void __fastcall GarmItmClick(TObject *Sender);
    void __fastcall N3Click(TObject *Sender);
    void __fastcall UAShMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
    void __fastcall EkscChange(TObject *Sender);
private:	// User declarations
   void __fastcall MyIdleHandler(TObject *Sender, bool &Done);
public:		// User declarations
   __fastcall TFMain(TComponent* Owner);
OnInitDevicesDialog();
HRESULT InitDirectSound( HWND hDlg, GUID* pDeviceGuid );
//VOID    ConvertWaveFormatToString( WAVEFORMATEX* pwfx, AnsiString );
HRESULT InitNotifications();
HRESULT CreateCaptureBuffer( PWAVEFORMATEX );
HRESULT StartOrStopRecord( BOOL bStartRecording );
HRESULT RecordCapturedData();
int OnSaveSoundFile();

   void DrawGrid(TImage *Im,int dX,int dY,bool Lx,bool Ly);
   void RepaintF();
   void RepaintS();
   void Calculate();
   void CalculateEngineSpeed();
   void CutTo2pi();

   void ResetColor(TForm* _ptr);
   void ResetKoef();
int CreateNewTable(AnsiString _TabName);

double ksc;                 /////////////////////// ���������� ���������� �������� �����
int _nFGar;                 // ���-�� ��������
//--------------------  E vars----------------------------
double Eth_g;          // ��������� ��� ����������

double KUa,KUb,KUc;
double KIa,KIb,KIc;
double KUab,KUbc,KUca;

TColor AxisCol,GridCol,LablICol,LablUCol,BkCol,SCol,ACol,BCol,CCol,TimeCol,
       AICol,BICol,CICol,
       AgCol,BgCol,CgCol,
       AIgCol,BIgCol,CIgCol,
       ABCol,BCCol,CACol,
       ABgCol,BCgCol,CAgCol;


};
//---------------------------------------------------------------------------
extern PACKAGE TFMain *FMain;
//---------------------------------------------------------------------------
#endif
