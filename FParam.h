//---------------------------------------------------------------------------

#ifndef FParamH
#define FParamH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TParForm : public TForm
{
__published:	// IDE-managed Components
  TTabControl *TabControl1;
  TMemo *Memo1;
  void __fastcall TabControl1Change(TObject *Sender);
  void __fastcall FormActivate(TObject *Sender);
private:	// User declarations
public:		// User declarations
  double  PA,PB,PC,dP,P,P0,Md,Nd;    // ��������� ���������
  double  U1,U2,U0,K2U,K0U,sU;     // ������������ �����������
  double  KUA,KUB,KUC;          // ������������ ������������������
  __fastcall TParForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TParForm *ParForm;
//---------------------------------------------------------------------------
#endif
