//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FParam.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TParForm *ParForm;
//---------------------------------------------------------------------------
__fastcall TParForm::TParForm(TComponent* Owner)
  : TForm(Owner)
{

}
//---------------------------------------------------------------------------
void __fastcall TParForm::TabControl1Change(TObject *Sender)
{
   Memo1->Clear();
   AnsiString tmpS;
   if(TabControl1->TabIndex==0){
     Memo1->Lines->Add("P0  ="+tmpS.sprintf("%.2E", P0));
     Memo1->Lines->Add("PA  ="+tmpS.sprintf("%.2E", PA));
     Memo1->Lines->Add("PB  ="+tmpS.sprintf("%.2E", PB));
     Memo1->Lines->Add("PC  ="+tmpS.sprintf("%.2E", PC));
     Memo1->Lines->Add("P  ="+tmpS.sprintf("%.2E", P));
     Memo1->Lines->Add("dP  ="+tmpS.sprintf("%.2E", dP));
     Memo1->Lines->Add("M   ="+tmpS.sprintf("%.2E", Md));
     Memo1->Lines->Add("����="+tmpS.sprintf("%.2E", Nd));
   }
   if(TabControl1->TabIndex==1){
     Memo1->Lines->Add("U1(1) ="+tmpS.sprintf("%.2E", U1));
     Memo1->Lines->Add("U2(1) ="+tmpS.sprintf("%.2E", U2));
     Memo1->Lines->Add("U0(1) ="+tmpS.sprintf("%.2E", U0));
     Memo1->Lines->Add("K2U   ="+tmpS.sprintf("%.2E", K2U));
     Memo1->Lines->Add("K0U   ="+tmpS.sprintf("%.2E", K0U));
   }
   if(TabControl1->TabIndex==2){
     Memo1->Lines->Add("KUA ="+tmpS.sprintf("%.2E", KUA));
     Memo1->Lines->Add("KUB ="+tmpS.sprintf("%.2E", KUB));
     Memo1->Lines->Add("KUC ="+tmpS.sprintf("%.2E", KUC));
   
   }
}
//---------------------------------------------------------------------------
void __fastcall TParForm::FormActivate(TObject *Sender)
{
   TabControl1Change(NULL);
   BringToFront();
}
//---------------------------------------------------------------------------
