object KoefForm: TKoefForm
  Left = 324
  Top = 159
  Width = 351
  Height = 221
  Caption = '������������'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 114
    Height = 13
    Caption = '����. �������� �����'
  end
  object Label2: TLabel
    Left = 16
    Top = 40
    Width = 111
    Height = 13
    Caption = '���������� ��������'
  end
  object Label3: TLabel
    Left = 16
    Top = 64
    Width = 85
    Height = 13
    Caption = '��� ����������'
  end
  object Label4: TLabel
    Left = 16
    Top = 120
    Width = 15
    Height = 13
    Caption = 'UA'
  end
  object Label5: TLabel
    Left = 16
    Top = 144
    Width = 15
    Height = 13
    Caption = 'UB'
  end
  object Label6: TLabel
    Left = 16
    Top = 168
    Width = 15
    Height = 13
    Caption = 'UC'
  end
  object Label7: TLabel
    Left = 112
    Top = 120
    Width = 10
    Height = 13
    Caption = 'IA'
  end
  object Label8: TLabel
    Left = 112
    Top = 144
    Width = 10
    Height = 13
    Caption = 'IB'
  end
  object Label9: TLabel
    Left = 112
    Top = 168
    Width = 10
    Height = 13
    Caption = 'IC'
  end
  object Label10: TLabel
    Left = 208
    Top = 120
    Width = 22
    Height = 13
    Caption = 'UAB'
  end
  object Label11: TLabel
    Left = 208
    Top = 144
    Width = 22
    Height = 13
    Caption = 'UBC'
  end
  object Label12: TLabel
    Left = 208
    Top = 168
    Width = 22
    Height = 13
    Caption = 'UCA'
  end
  object Label13: TLabel
    Left = 16
    Top = 88
    Width = 214
    Height = 13
    Caption = '--------- ������������ ��� ������� ������'
  end
  object Eksc: TEdit
    Left = 160
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 0
    Text = '0.48'
    Visible = False
    OnChange = EkscChange
  end
  object EGarm: TEdit
    Left = 160
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 1
    Text = '40'
    Visible = False
    OnChange = EkscChange
  end
  object EEth: TEdit
    Left = 160
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '0.75'
    Visible = False
    OnChange = EkscChange
  end
  object EKUa: TEdit
    Left = 40
    Top = 112
    Width = 57
    Height = 21
    TabOrder = 3
    Text = '102'
    Visible = False
    OnChange = EkscChange
  end
  object EKUb: TEdit
    Left = 40
    Top = 136
    Width = 57
    Height = 21
    TabOrder = 4
    Text = '102'
    Visible = False
    OnChange = EkscChange
  end
  object EKUc: TEdit
    Left = 40
    Top = 160
    Width = 57
    Height = 21
    TabOrder = 5
    Text = '102'
    Visible = False
    OnChange = EkscChange
  end
  object EKIa: TEdit
    Left = 136
    Top = 112
    Width = 57
    Height = 21
    TabOrder = 6
    Text = '102'
    Visible = False
    OnChange = EkscChange
  end
  object EKIb: TEdit
    Left = 136
    Top = 136
    Width = 57
    Height = 21
    TabOrder = 7
    Text = '102'
    Visible = False
    OnChange = EkscChange
  end
  object EKIc: TEdit
    Left = 136
    Top = 160
    Width = 57
    Height = 21
    TabOrder = 8
    Text = '102'
    Visible = False
    OnChange = EkscChange
  end
  object EKUab: TEdit
    Left = 240
    Top = 112
    Width = 57
    Height = 21
    TabOrder = 9
    Text = '102'
    Visible = False
    OnChange = EkscChange
  end
  object EKUbc: TEdit
    Left = 240
    Top = 136
    Width = 57
    Height = 21
    TabOrder = 10
    Text = '102'
    Visible = False
    OnChange = EkscChange
  end
  object EKUca: TEdit
    Left = 240
    Top = 160
    Width = 57
    Height = 21
    TabOrder = 11
    Text = '102'
    Visible = False
    OnChange = EkscChange
  end
end
