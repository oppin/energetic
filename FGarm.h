//---------------------------------------------------------------------------

#ifndef FGarmH
#define FGarmH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
//---------------------------------------------------------------------------
class TGarmForm : public TForm
{
__published:	// IDE-managed Components
    TDBGrid *DBGrid1;
private:	// User declarations
public:		// User declarations
    __fastcall TGarmForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TGarmForm *GarmForm;
//---------------------------------------------------------------------------
#endif
