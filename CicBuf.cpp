//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CicBuf.h"

//---------------------------------------------------------------------------
CCicBuf::CCicBuf(unsigned long n){
      size=n; head=0; ccount=wcount=0;
      wData=new unsigned short[size];
      cData=(unsigned char*)wData;
};
// ----------------------
CCicBuf::~CCicBuf(){
      delete wData;
};
// ----------------------
unsigned short CCicBuf::get_w(unsigned long n){
     if( n>wcount) return 0;
     if(n+head<size)  return wData[n+head];
     else return wData[n+head-size];
};
// ----------------------
unsigned char CCicBuf::get_c(unsigned long n){
     if( n>ccount) return 0;
     if(n+2*head<2*size)  return cData[n+head*2];
     else return wData[n+head*2-size*2];
}
// ----------------------
void CCicBuf::Addc(unsigned char* cbuf,unsigned long sz){
      if(sz>size*2){
            memcpy(cData,cbuf,size*2);
            head=0;ccount=2*size;wcount=size;
   return;
      }
      if(sz<(size-head-wcount)*2){
            memcpy(cData+2*(head+wcount),cbuf,sz);
            ccount+=sz;wcount+=(sz/2+sz%2);
            if(ccount>2*size){
               head+=(ccount-size*2)/2+ccount%2;
               if(head>size) head-=size;
               ccount=2*size;wcount=size;
            }
   return;
      }
      unsigned long rest=sz-(size-head-wcount)*2;
      memcpy(cData+2*head,cbuf,(size-head-wcount)*2);
      memcpy(cData,cbuf,rest);
      ccount+=sz;wcount+=(sz/2+sz%2);
      if(ccount>2*size){
         head+=(ccount-size*2)/2+ccount%2;
         if(head>size) head-=size;
         ccount=2*size;wcount=size;
      }
  return;
}
// ----------------------
void CCicBuf::Addw(unsigned short* wbuf,unsigned long sz){
      Addc((unsigned char*)wbuf,sz*2);
  return;
}
// ----------------------
void CCicBuf::Clear(){
      ZeroMemory(wData,size*2);
      head=ccount=wcount=0;
}
void CCicBuf::erasec(int off,int sz){
  if(sz<=0) return;
  char* tmp=new char[size];
  if(sz>ccount){ Clear();return;}
  if(head==0){
     memcpy(tmp,cData+head,off);
     memcpy(tmp+off,cData+head+off+sz,size-(off+head+sz));
     ccount-=sz;
     memcpy(cData,tmp,ccount);
     delete tmp;
     return;
  }
  if(off+head<size){
     memcpy(tmp,cData+head,off);
     if(off+head+sz<size){
         memcpy(tmp+off,cData+head+off+sz,size-(off+head+sz));
         memcpy(tmp+off,cData+head+off+sz,size-(off+head+sz));
     }
     else{
        memcpy(tmp+off,cData+head+off+sz,size-(off+head+sz));
        memcpy(tmp+off,cData+(off+head+sz-size),head-(off+head+sz-size));
     }
     memcpy(cData,tmp,ccount);
     delete tmp;
     return;
  }
  memcpy(tmp,cData,(size-head-off));
  memcpy(tmp,cData+(size-head-off)+sz,head-(size-head-off)-sz);
  memcpy(cData,tmp,ccount);
  delete tmp;
  return;

}

#pragma package(smart_init)
