//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FColor.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
#include "Osc3.h"
TColorForm *ColorForm;
//---------------------------------------------------------------------------
__fastcall TColorForm::TColorForm(TComponent* Owner)
  : TForm(Owner)
{

}
//---------------------------------------------------------------------------
void __fastcall TColorForm::UAShMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
    TShape* ptr=(TShape*)Sender;
    ColorDialog1->Execute();
    if(ptr==UASh)UASh->Brush->Color=ColorDialog1->Color;
    if(ptr==UBSh)UBSh->Brush->Color=ColorDialog1->Color;
    if(ptr==UCSh)UCSh->Brush->Color=ColorDialog1->Color;
    if(ptr==UAgSh)UAgSh->Brush->Color=ColorDialog1->Color;
    if(ptr==UBgSh)UBgSh->Brush->Color=ColorDialog1->Color;
    if(ptr==UCgSh)UCgSh->Brush->Color=ColorDialog1->Color;

    if(ptr==IASh)IASh->Brush->Color=ColorDialog1->Color;
    if(ptr==IBSh)IBSh->Brush->Color=ColorDialog1->Color;
    if(ptr==ICSh)ICSh->Brush->Color=ColorDialog1->Color;
    if(ptr==IAgSh)IAgSh->Brush->Color=ColorDialog1->Color;
    if(ptr==IBgSh)IBgSh->Brush->Color=ColorDialog1->Color;
    if(ptr==ICgSh)ICgSh->Brush->Color=ColorDialog1->Color;

    if(ptr==UABSh)UABSh->Brush->Color=ColorDialog1->Color;
    if(ptr==UBCSh)UBCSh->Brush->Color=ColorDialog1->Color;
    if(ptr==UCASh)UCASh->Brush->Color=ColorDialog1->Color;
    if(ptr==UABgSh)UABgSh->Brush->Color=ColorDialog1->Color;
    if(ptr==UBCgSh)UBCgSh->Brush->Color=ColorDialog1->Color;
    if(ptr==UCAgSh)UCAgSh->Brush->Color=ColorDialog1->Color;

    if(ptr==FonSh)FonSh->Brush->Color=ColorDialog1->Color;
    if(ptr==SpSh)SpSh->Brush->Color=ColorDialog1->Color;
    if(ptr==AxisSh)AxisSh->Brush->Color=ColorDialog1->Color;
    if(ptr==OXSh)OXSh->Brush->Color=ColorDialog1->Color;
    if(ptr==OYSh)OYSh->Brush->Color=ColorDialog1->Color;
    if(ptr==NetSh)NetSh->Brush->Color=ColorDialog1->Color;

    FMain->ResetColor(this);
}
//---------------------------------------------------------------------------

int TColorForm::SetAll(TForm* _f)
{
TFMain* _ptr=(TFMain*)_f;
       AxisSh->Brush->Color=_ptr->AxisCol;
       NetSh->Brush->Color=_ptr->GridCol;
       OXSh->Brush->Color=_ptr->LablICol;
       OYSh->Brush->Color=_ptr->LablUCol;
       FonSh->Brush->Color=_ptr->BkCol;
       AxisSh->Brush->Color=_ptr->SCol;
       UASh->Brush->Color=_ptr->ACol;
       UBSh->Brush->Color=_ptr->BCol;
       UCSh->Brush->Color=_ptr->CCol;
       TimeSh->Brush->Color=_ptr->TimeCol;
       IASh->Brush->Color=_ptr->AICol;
       IBSh->Brush->Color=_ptr->BICol;
       ICSh->Brush->Color=_ptr->CICol;
       UAgSh->Brush->Color=_ptr->AgCol;
       FonSh->Brush->Color=_ptr->BgCol;
       UCgSh->Brush->Color=_ptr->CgCol;
       IAgSh->Brush->Color=_ptr->AIgCol;
       IBgSh->Brush->Color=_ptr->BIgCol;
       ICgSh->Brush->Color=_ptr->CIgCol;
       UABSh->Brush->Color=_ptr->ABCol;
       UBCSh->Brush->Color=_ptr->BCCol;
       UCASh->Brush->Color=_ptr->CACol;
       UABgSh->Brush->Color=_ptr->ABgCol;
       UBCgSh->Brush->Color=_ptr->BCgCol;
       UCAgSh->Brush->Color=_ptr->CAgCol;

}
//---------------------------------------------------------------------------

