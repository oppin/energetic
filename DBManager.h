//---------------------------------------------------------------------------

#ifndef DBManagerH
#define DBManagerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TDBView : public TForm
{
__published:	// IDE-managed Components
   TListBox *ListBox1;
private:	// User declarations
public:		// User declarations
   __fastcall TDBView(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TDBView *DBView;
//---------------------------------------------------------------------------
#endif
