//---------------------------------------------------------------------------

#ifndef FAskH
#define FAskH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFAskName : public TForm
{
__published:	// IDE-managed Components
   TEdit *ENewName;
   TLabel *Label1;
   TButton *Button1;
   void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TFAskName(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFAskName *FAskName;
//---------------------------------------------------------------------------
#endif
