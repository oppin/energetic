//---------------------------------------------------------------------------

#ifndef FColorH
#define FColorH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>


//---------------------------------------------------------------------------
class TColorForm : public TForm
{
__published:	// IDE-managed Components
  TColorDialog *ColorDialog1;
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TShape *UASh;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label19;
  TLabel *Label20;
  TLabel *Label21;
  TLabel *Label22;
  TLabel *Label23;
  TLabel *Label24;
  TLabel *Label25;
  TLabel *Label26;
  TLabel *Label27;
  TLabel *Label28;
  TLabel *Label29;
  TLabel *Label30;
  TLabel *Label7;
  TLabel *Label8;
  TLabel *Label9;
  TLabel *Label10;
  TLabel *Label11;
  TLabel *Label12;
  TLabel *Label13;
  TLabel *Label14;
  TShape *UBSh;
  TShape *UCSh;
  TShape *UAgSh;
  TShape *UBgSh;
  TShape *UCgSh;
  TShape *IASh;
  TShape *IBSh;
  TShape *ICSh;
  TShape *IAgSh;
  TShape *IBgSh;
  TShape *ICgSh;
  TShape *UABSh;
  TShape *UBCSh;
  TShape *UCASh;
  TShape *UABgSh;
  TShape *UBCgSh;
  TShape *UCAgSh;
  TPanel *Panel1;
  TLabel *Label15;
  TLabel *Label16;
  TLabel *Label17;
  TLabel *Label18;
  TLabel *Label31;
  TShape *FonSh;
  TShape *AxisSh;
  TShape *SpSh;
  TShape *OXSh;
  TShape *OYSh;
  TShape *NetSh;
  TLabel *Label32;
    TLabel *Label33;
    TShape *TimeSh;
  void __fastcall UAShMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
private:	// User declarations
public:		// User declarations
  __fastcall TColorForm(TComponent* Owner);

  int SetAll(TForm* _ptr);
};
//---------------------------------------------------------------------------
extern PACKAGE TColorForm *ColorForm;
//---------------------------------------------------------------------------
#endif
