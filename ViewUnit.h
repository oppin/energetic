//---------------------------------------------------------------------------

#ifndef ViewUnitH
#define ViewUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TDBView : public TForm
{
__published:	// IDE-managed Components
   TListBox *ListBox1;
   TButton *Button1;
   TButton *Button2;
   TButton *BDel;
   TButton *Button3;
   void __fastcall Button1Click(TObject *Sender);
   void __fastcall Button2Click(TObject *Sender);
   void __fastcall BDelClick(TObject *Sender);
   void __fastcall Button3Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TDBView(TComponent* Owner);
   bool Deleted;
   bool Rename;
   AnsiString NewName;   
};
//---------------------------------------------------------------------------
extern PACKAGE TDBView *DBView;
//---------------------------------------------------------------------------
#endif
