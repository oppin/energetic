//---------------------------------------------------------------------------
//#include "ForCodog.h"

#include <vcl.h>
#include "DSUtil.h"
#include <dsound.h>
#include <math.h>
#pragma hdrstop

#include "Osc3.h"
#include "TransFunc.h"                     
#include "FAsk.h"
#include "FParam.h"
#include "FColor.h"
#include "FKoef.h"
#include "FGarm.h"
#include "ViewUnit.h"
#include "CicBuf.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma link "Word_2K_SRVR"
#pragma resource "*.dfm"


#define NUM_REC_NOTIFICATIONS  16
AnsiString TabName;
bool Recalc=false;
/*
double ksc=0.48;                 /////////////////////// ���������� ���������� �������� �����
int _nFGar=52;                 // ���-�� ��������
//--------------------  E vars----------------------------
double Eth_g=0.75;          // ��������� ��� ����������

double KUa=102,KUb=102,KUc=102;
double KIa=102,KIb=102,KIc=102;
double KUab=102,KUbc=102,KUca=102;

*/

double Ua0=0,Ub0=0,Uc0=0;
double Ia0=0,Ib0=0,Ic0=0;
double Uab0=0,Ubc0=0,Uca0=0;
//double Iab0=0,Ibc0=0,Ica0=0;

double Pa0=0,Pb0=0,Pc0=0;
double Qa0=0,Qb0=0,Qc0=0;
double Sa0=0,Sb0=0,Sc0=0;

int cntOP=0;
int A_state=0,B_state=0,C_state=0;

        //    ��������� �����������
  double  lPA,lPB,lPC,ldP,lP,lP0,lM,lNd;    // ��������� ���������
  double  lsU,lU1,lU2,lU0,lK2U,lK0U;     // ������������ �����������
  double  lKUA,lKUB,lKUC;          // ������������ ������������������
double  ESKoef=1;
//--------------------DB vars----------------------------
AnsiString CurrTab="";

//--------------------GDI vars----------------------------
bool usedR=false,Initializing=false;
/*
TColor AxisCol=clBlue,GridCol=clGreen,LablICol=clFuchsia,LablUCol=clWhite,BkCol=clBlack,
       SCol=clTeal, ACol=0xAAFF00,BCol=0xAA00AA,CCol=0x00FFAA,TimeCol=clWhite,
       AICol=0xAAFFCC,BICol=0xAACCAA,CICol=0xCCFFAA,
       AgCol=0xAAFF00,BgCol=0xAA00AA,CgCol=0x00FFAA,
       AIgCol=0xAAFFCC,BIgCol=0xAACCAA,CIgCol=0xCCFFAA,
       ABCol=0xAAFF00,BCCol=0xAA00AA,CACol=0x00FFAA,
       ABgCol=0xAAFF00,BCgCol=0xAA00AA,CAgCol=0x00FFAA;
*/
double mulU=500,mulI=500,mulS=1;
int Denc=1,OfftU=0,OfftS=0,OfftI=0,offU=0,offI=0,offGU=0,offGI=0;
double UA=220,UB=220,UC=220;
double IA=210,IB=220,IC=234;
double UAB=380,UBC=380,UCA=380;
bool Calculated=false;
HINSTANCE                  g_hInst              = NULL;
HANDLE                     g_hNotificationEvent;//[NUM_REC_NOTIFICATIONS+1];
DSBPOSITIONNOTIFY          g_aPosNotify[ NUM_REC_NOTIFICATIONS + 1 ];
    static GUID  AudioDriverGUIDs[20];
    static DWORD dwAudioDriverIndex = 0;
//--------------------Sound vars----------------------------
int sound_regim=1;

GUID                       g_guidCaptureDevice  = GUID_NULL;
DWORD                      g_dwCaptureBufferSize;
DWORD                      g_dwNotifySize;
WAVEFORMATEX              g_wfxInput;
WAVEFORMATEX              wfxCapture;
HANDLE                  SFile;
CWaveFile*                  g_pWaveFile;
LPDIRECTSOUNDCAPTURE       g_pDSCapture         = NULL;
LPDIRECTSOUNDCAPTUREBUFFER g_pDSBCapture        = NULL;
BOOL                       g_bRecording;
LPDIRECTSOUNDNOTIFY        g_pDSNotify          = NULL;
DWORD                      g_dwNextCaptureOffset;
INT_PTR CALLBACK DSoundEnumCallback( GUID* pGUID, const char* strDesc, const char* strDrvName,
                                     VOID* pContext );
//--------------------Local vars----------------------------
CCicBuf *WDataA,*WDataB,*WDataC;
CCicBuf *WDataAi,*WDataBi,*WDataCi;

CCicBuf *WDataAB,*WDataBC,*WDataCA;
//CCicBuf *WDataABi,*WDataBCi,*WDataCAi;

CCicBuf *WDataAiG,*WDataBiG,*WDataCiG;
CCicBuf *WDataAG,*WDataBG,*WDataCG;
CCicBuf *WDataES;
CCicBuf *WDataABG,*WDataBCG,*WDataCAG;

CCicBuf* Ptr_RBuf=NULL;             // ��������� �� ������� �����
unsigned int arrF[20];
TFMain *FMain;
//---------------------------------------------------------------------------
void __fastcall TFMain::MyIdleHandler(TObject *Sender, bool &bDone)
{
DWORD dwResult,hr;
bDone = FALSE;
DWORD off,pos;
    while( !bDone )
    {
         dwResult = MsgWaitForMultipleObjects( 1, &g_hNotificationEvent,
                                              FALSE, INFINITE, QS_ALLEVENTS );
        switch( dwResult )
        {
            case WAIT_OBJECT_0 + 0:
                // g_hNotificationEvents[0] is signaled

                // This means that DirectSound just finished playing
                // a piece of the buffer, so we need to fill the circular
                // buffer with new sound from the wav file

                if( FAILED( hr = RecordCapturedData() ) )
                {
                    MessageBox( this, "Error handling DirectSound notifications. "
                               "Sample will now exit.", "DirectSound Sample",
                               MB_OK | MB_ICONERROR );
                    bDone = TRUE;
                }
                break;

            case WAIT_OBJECT_0 + 1:
                  bDone = TRUE;
                break;
        }
    }
}
//---------------------------------------------------------------------------

void __fastcall TFMain::FormCreate(TObject *Sender)
{
   randomize();
   DWORD a;
   HANDLE F=NULL;
   F=CreateFile("options.ini",GENERIC_READ,0,0,OPEN_EXISTING,0,0);
   if(F!=INVALID_HANDLE_VALUE){
    ReadFile(F,&ksc,sizeof(double),&a,0);
    ReadFile(F,&_nFGar,sizeof(int),&a,0);
    ReadFile(F,&Eth_g,sizeof(double),&a,0);
    ReadFile(F,&KUa,sizeof(double),&a,0);
    ReadFile(F,&KUb,sizeof(double),&a,0);
    ReadFile(F,&KUc,sizeof(double),&a,0);
    ReadFile(F,&KIa,sizeof(double),&a,0);
    ReadFile(F,&KIb,sizeof(double),&a,0);
    ReadFile(F,&KIc,sizeof(double),&a,0);
    ReadFile(F,&KUab,sizeof(double),&a,0);
    ReadFile(F,&KUbc,sizeof(double),&a,0);
    ReadFile(F,&KUca,sizeof(double),&a,0);

 int tmp;
    ReadFile(F,&tmp,sizeof(int),&a,0); NEdit->Text=IntToStr(tmp);
    ReadFile(F,&tmp,sizeof(int),&a,0); PEdit->Text=IntToStr(tmp);
    ReadFile(F,&AxisCol,sizeof(TColor),&a,0);
    ReadFile(F,&GridCol,sizeof(TColor),&a,0);
    ReadFile(F,&LablICol,sizeof(TColor),&a,0);
    ReadFile(F,&LablUCol,sizeof(TColor),&a,0);
    ReadFile(F,&BkCol,sizeof(TColor),&a,0);
    ReadFile(F,&SCol,sizeof(TColor),&a,0);
    ReadFile(F,&ACol,sizeof(TColor),&a,0);
    ReadFile(F,&BCol,sizeof(TColor),&a,0);
    ReadFile(F,&CCol,sizeof(TColor),&a,0);
    ReadFile(F,&AICol,sizeof(TColor),&a,0);
    ReadFile(F,&BICol,sizeof(TColor),&a,0);
    ReadFile(F,&CICol,sizeof(TColor),&a,0);
    ReadFile(F,&AgCol,sizeof(TColor),&a,0);
    ReadFile(F,&BgCol,sizeof(TColor),&a,0);
    ReadFile(F,&CgCol,sizeof(TColor),&a,0);
    ReadFile(F,&AIgCol,sizeof(TColor),&a,0);
    ReadFile(F,&BIgCol,sizeof(TColor),&a,0);
    ReadFile(F,&CIgCol,sizeof(TColor),&a,0);
    ReadFile(F,&ABCol,sizeof(TColor),&a,0);
    ReadFile(F,&BCCol,sizeof(TColor),&a,0);
    ReadFile(F,&CACol,sizeof(TColor),&a,0);
    ReadFile(F,&ABgCol,sizeof(TColor),&a,0);
    ReadFile(F,&BCgCol,sizeof(TColor),&a,0);
    ReadFile(F,&CAgCol,sizeof(TColor),&a,0);
    ReadFile(F,&TimeCol,sizeof(TColor),&a,0);
    ReadFile(F,&ESKoef,sizeof(double),&a,0);

    Initializing=true;

    EKIa->Text=FloatToStr(KIa,3);    EKIb->Text=FloatToStr(KIb,3);
    EKIc->Text=FloatToStr(KIc,3);    EKUab->Text=FloatToStr(KUab,3);
    EKUbc->Text=FloatToStr(KUbc,3);    EKUca->Text=FloatToStr(KUca,3);
    Eksc->Text=FloatToStr(ksc,3);    EGarm->Text=IntToStr(_nFGar);
    EESKoef->Text=FloatToStr(ESKoef,3);
    EEth->Text=FloatToStr(Eth_g,3);    EKUa->Text=FloatToStr(KUa,3);
    EKUb->Text=FloatToStr(KUb,3);    EKUc->Text=FloatToStr(KUc,3);

    UASh->Brush->Color=ACol;    UBSh->Brush->Color=BCol;
    UCSh->Brush->Color=CCol;    UAgSh->Brush->Color=AgCol;
    UBgSh->Brush->Color=BgCol;    UCgSh->Brush->Color=CgCol;
    IASh->Brush->Color=AICol;     IBSh->Brush->Color=BICol;
    ICSh->Brush->Color=CICol;     IAgSh->Brush->Color=AIgCol;
    IBgSh->Brush->Color=BIgCol;    ICgSh->Brush->Color=CIgCol;
    UABSh->Brush->Color=ABCol;     UBCSh->Brush->Color=BCCol;
    UCASh->Brush->Color=CACol;    UABgSh->Brush->Color=ABgCol;
    UBCgSh->Brush->Color=BCgCol;    UCAgSh->Brush->Color=CAgCol;
    FonSh->Brush->Color=BkCol;      AxisSh->Brush->Color=AxisCol;
    NetSh->Brush->Color=GridCol;    SpSh->Brush->Color=SCol;
    OXSh->Brush->Color=LablICol;    OYSh->Brush->Color=LablUCol;
    TimeSh->Brush->Color=TimeCol;

    Initializing=false;
        
    CloseHandle(F);
   }
   else{
        ksc=0.48;                 /////////////////////// ���������� ���������� �������� �����
        _nFGar=52;                 // ���-�� ��������
        //--------------------  E vars----------------------------
            Eth_g=0.75;          // ��������� ��� ����������
        KUa=KUb=KUc=KIa=KIb=KIc=KUab=KUbc=KUca=102;

       AxisCol=clBlue;GridCol=clGreen;LablICol=clFuchsia;LablUCol=clWhite;BkCol=clBlack;
       SCol=clTeal; ACol=0xAAFF00;BCol=0xAA00AA;CCol=0x00FFAA;TimeCol=clWhite;
       AICol=0xAAFFCC;BICol=0xAACCAA;CICol=0xCCFFAA;
       AgCol=0xAAFF00;BgCol=0xAA00AA;CgCol=0x00FFAA;
       AIgCol=0xAAFFCC;BIgCol=0xAACCAA;CIgCol=0xCCFFAA;
       ABCol=0xAAFF00;BCCol=0xAA00AA;CACol=0x00FFAA;
       ABgCol=0xAAFF00;BCgCol=0xAA00AA;CAgCol=0x00FFAA;

   }

   g_hNotificationEvent = CreateEvent( NULL, FALSE, FALSE, NULL );

    // Get the GUID attached to the combo box item
    DirectSoundCaptureEnumerate( DSoundEnumCallback,this->Handle );

   InitDirectSound( this->Handle , &AudioDriverGUIDs[CBDevice->ItemIndex] );
   Application->OnIdle=MyIdleHandler;
   WDataA=new CCicBuf(1000000);  WDataB=new CCicBuf(1000000);
   WDataC=new CCicBuf(1000000);  WDataAi=new CCicBuf(1000000);
   WDataBi=new CCicBuf(1000000); WDataCi=new CCicBuf(1000000);
   WDataAG=new CCicBuf(1000000); WDataBG=new CCicBuf(1000000);
   WDataCG=new CCicBuf(1000000); WDataAiG=new CCicBuf(1000000);
   WDataBiG=new CCicBuf(1000000);WDataCiG=new CCicBuf(1000000);

   WDataAB=new CCicBuf(1000000);  WDataBC=new CCicBuf(1000000);
   WDataCA=new CCicBuf(1000000);  WDataABG=new CCicBuf(1000000);
   WDataBCG=new CCicBuf(1000000);   WDataCAG=new CCicBuf(1000000);

   WDataES=new CCicBuf(1000000);

   DrawGrid(ImFA,20,10,true,true);
   Panel4->Height-=200;
   if(offU>0)  Lvoff->Caption="U+"+IntToStr(offU);else Lvoff->Caption="U"+IntToStr(offU);
   if(offI>0)  Lioff->Caption="I+"+IntToStr(offI);else Lioff->Caption="I"+IntToStr(offI);
}
//---------------------------------------------------------------------------
__fastcall TFMain::TFMain(TComponent* Owner)
   : TForm(Owner)
{
//   g_hNotificationEvent = CreateEvent( NULL, FALSE, FALSE, NULL );
  FMain->Resizing(wsMaximized);
  ImFA->Align=alClient;
}
//---------------------------------------------------------------------------
//--------------------To help little tricks--------------
VOID GetWaveFormatFromIndex( INT nIndex, WAVEFORMATEX* pwfx ){
    INT iSampleRate = nIndex / 4; INT iType = nIndex % 4;
    switch( iSampleRate ){ case 0: pwfx->nSamplesPerSec = 48000; break; case 1: pwfx->nSamplesPerSec = 44100; break; case 2: pwfx->nSamplesPerSec = 22050; break; case 3: pwfx->nSamplesPerSec = 11025; break; case 4: pwfx->nSamplesPerSec =  8000; break; }
    switch( iType ){ case 0: pwfx->wBitsPerSample =  8; pwfx->nChannels = 1; break; case 1: pwfx->wBitsPerSample = 16; pwfx->nChannels = 1; break; case 2: pwfx->wBitsPerSample =  8; pwfx->nChannels = 2; break; case 3: pwfx->wBitsPerSample = 16; pwfx->nChannels = 2; break;}
    pwfx->nBlockAlign = pwfx->nChannels * ( pwfx->wBitsPerSample / 8 ); pwfx->nAvgBytesPerSec = pwfx->nBlockAlign * pwfx->nSamplesPerSec;
}
AnsiString GetStringFromIndex( INT nIndex){
AnsiString S;
    INT iSampleRate = nIndex / 4; INT iType = nIndex % 4;
    switch( iSampleRate ){ case 0: S = "48000 Hz"; break; case 1: S = "44100 Hz"; break; case 2: S = "22050 Hz"; break; case 3: S = "11025 Hz"; break; case 4: S = "8000 Hz"; break; }
    switch( iType ){ case 0: S += "8-bit Mono"; break; case 1: S += "16-bit Mono"; break; case 2: S += "8-bit Stereo"; break; case 3: S += "16-bit Stereo"; break;}
return S;
}
//---------------------------------------------------------------------------
HRESULT TFMain::InitDirectSound( HWND hDlg, GUID* pDeviceGuid ){
    HRESULT hr;
    ZeroMemory( &g_aPosNotify, sizeof(DSBPOSITIONNOTIFY) *
                               (NUM_REC_NOTIFICATIONS + 1) );
    g_dwCaptureBufferSize = 0;
    g_dwNotifySize        = 0;
    g_pWaveFile           = NULL;
    // Initialize COM
    if( FAILED( hr = CoInitialize(NULL) ) )
        return MessageBox(NULL, "The DSound is not installed or corrupted.\n Do You want to close application?",
                     "Error",MB_YESNO||MB_ICONERROR );
    // Create IDirectSoundCapture using the preferred capture device
    if( FAILED( hr = DirectSoundCaptureCreate( pDeviceGuid, &g_pDSCapture, NULL ) ) )
        return MessageBox(NULL, "The DSound can not initialize.\n Do You want to close application?",
                     "Error",MB_YESNO||MB_ICONERROR );

    WAVEFORMATEX wfxInput;
    AnsiString   strInputFormat;
    HWND         hInputFormatText;
//----------------------------------
//--- ���� ����� ���������� ��������
    WAVEFORMATEX  wfx;
    HCURSOR       hCursor;
    DSCBUFFERDESC dscbd;
    LPDIRECTSOUNDCAPTUREBUFFER pDSCaptureBuffer = NULL;

    ZeroMemory( &wfx, sizeof(wfx));
    wfx.wFormatTag = WAVE_FORMAT_PCM;

    ZeroMemory( &dscbd, sizeof(dscbd) );
    dscbd.dwSize = sizeof(dscbd);
    // Try 20 different standard formats to see if they are supported
    if(!RGSFormat->Items->Count)
    for( INT iIndex = 0; iIndex < 20; iIndex++ )
    {
        GetWaveFormatFromIndex( iIndex, &wfx );
        // To test if a capture format is supported, try to create a
        // new capture buffer using a specific format.  If it works
        // then the format is supported, otherwise not.
        dscbd.dwBufferBytes = wfx.nAvgBytesPerSec;
        dscbd.lpwfxFormat = &wfx;
        if(!FAILED( hr = g_pDSCapture->CreateCaptureBuffer( &dscbd,
                                                            &g_pDSBCapture,
                                                            NULL ) ) ){
           RGSFormat->Items->Add(GetStringFromIndex( iIndex));
           arrF[RGSFormat->Items->Count]=iIndex;
        }
        SAFE_RELEASE( g_pDSBCapture );
    }
//----------------------------------
        memcpy(&g_wfxInput,&wfx,sizeof(WAVEFORMATEX));
        RGSFormat->ItemIndex=sound_regim;
        GetWaveFormatFromIndex( arrF[0], &wfx );
        g_bRecording = FALSE;

    return S_OK;
}
//-------------------------------------------------------------------------------------------
HRESULT TFMain::CreateCaptureBuffer(PWAVEFORMATEX pwfxInput )
{
    HRESULT hr;
    DSCBUFFERDESC dscbd;

    SAFE_RELEASE( g_pDSNotify );
    SAFE_RELEASE( g_pDSBCapture );

    // Set the notification size
    g_dwNotifySize = pwfxInput->nAvgBytesPerSec / 8 ;
    if(g_dwNotifySize<1024)g_dwNotifySize=1024;

    g_dwNotifySize -= g_dwNotifySize % pwfxInput->nBlockAlign;

    // Set the buffer sizes
    g_dwCaptureBufferSize = g_dwNotifySize * NUM_REC_NOTIFICATIONS;

    SAFE_RELEASE( g_pDSNotify );
    SAFE_RELEASE( g_pDSBCapture );

    // Create the capture buffer
    ZeroMemory( &dscbd, sizeof(dscbd) );
    dscbd.dwSize        = sizeof(dscbd);
    dscbd.dwBufferBytes = g_dwCaptureBufferSize;
    dscbd.lpwfxFormat   = pwfxInput; // Set the format during creatation

    if( FAILED( hr = g_pDSCapture->CreateCaptureBuffer( &dscbd,
                                                        &g_pDSBCapture,
                                                        NULL ) ) )
        return MessageBox(NULL, "CreateCaptureBuffer","Error",MB_ICONERROR );

    g_dwNextCaptureOffset = 0;

    if( FAILED( hr = InitNotifications() ) )
        return MessageBox(NULL, "InitNotifications","Error",MB_ICONERROR );

    return S_OK;
}
//-----------------------
HRESULT TFMain::InitNotifications()
{
    HRESULT hr;

    if( NULL == g_pDSBCapture )
        return E_FAIL;

    // Create a notification event, for when the sound stops playing
    if( FAILED( hr = g_pDSBCapture->QueryInterface( IID_IDirectSoundNotify,
                                                    (VOID**)&g_pDSNotify ) ) )
        return MessageBox(NULL, "QueryInterface","Error",MB_ICONERROR );
    // Setup the notification positions
    for( INT i = 0; i < NUM_REC_NOTIFICATIONS; i++ )
    {
        g_aPosNotify[i].dwOffset = (g_dwNotifySize * i) + g_dwNotifySize - 1;
        g_aPosNotify[i].hEventNotify = g_hNotificationEvent;
    }

    // Tell DirectSound when to notify us. the notification will come in the from
    // of signaled events that are handled in WinMain()
    if( FAILED( hr = g_pDSNotify->SetNotificationPositions( NUM_REC_NOTIFICATIONS,
                                                            g_aPosNotify ) ) )
        return MessageBox(NULL, "SetNotificationPositions","Error",MB_ICONERROR );

    return S_OK;
}

void __fastcall TFMain::FormClose(TObject *Sender, TCloseAction &Action)
{
   TableA->Close();
   TableB->Close();
    usedR=true;
    Calculated=false;
    delete WDataA; delete WDataB; delete WDataC;
    delete WDataAB; delete WDataBC; delete WDataCA;
    delete WDataAi; delete WDataBi; delete WDataCi;
    delete WDataABG; delete WDataBCG; delete WDataCAG;
    delete WDataES;
    delete WDataAG; delete WDataBG; delete WDataCG;
    delete WDataAiG; delete WDataBiG; delete WDataCiG;

    SAFE_DELETE( g_pWaveFile );

    // Release DirectSound interfaces
    SAFE_RELEASE( g_pDSNotify );
    SAFE_RELEASE( g_pDSBCapture );
    SAFE_RELEASE( g_pDSCapture );

    CloseHandle( g_hNotificationEvent );
    // Release COM
    CoUninitialize();

   DWORD a;
   HANDLE F=NULL;
   F=CreateFile("options.ini",GENERIC_WRITE,0,0,CREATE_ALWAYS,0,0);
   if(F!=INVALID_HANDLE_VALUE){
    WriteFile(F,&ksc,sizeof(double),&a,0);
    WriteFile(F,&_nFGar,sizeof(int),&a,0);
    WriteFile(F,&Eth_g,sizeof(double),&a,0);
    WriteFile(F,&KUa,sizeof(double),&a,0);
    WriteFile(F,&KUb,sizeof(double),&a,0);
    WriteFile(F,&KUc,sizeof(double),&a,0);
    WriteFile(F,&KIa,sizeof(double),&a,0);
    WriteFile(F,&KIb,sizeof(double),&a,0);
    WriteFile(F,&KIc,sizeof(double),&a,0);
    WriteFile(F,&KUab,sizeof(double),&a,0);
    WriteFile(F,&KUbc,sizeof(double),&a,0);
    WriteFile(F,&KUca,sizeof(double),&a,0);

 int tmp;
    tmp=NEdit->Text.ToInt();
    WriteFile(F,&tmp,sizeof(int),&a,0);
    tmp=PEdit->Text.ToInt();
    WriteFile(F,&tmp,sizeof(int),&a,0);
    WriteFile(F,&AxisCol,sizeof(TColor),&a,0);
    WriteFile(F,&GridCol,sizeof(TColor),&a,0);
    WriteFile(F,&LablICol,sizeof(TColor),&a,0);
    WriteFile(F,&LablUCol,sizeof(TColor),&a,0);
    WriteFile(F,&BkCol,sizeof(TColor),&a,0);
    WriteFile(F,&SCol,sizeof(TColor),&a,0);
    WriteFile(F,&ACol,sizeof(TColor),&a,0);
    WriteFile(F,&BCol,sizeof(TColor),&a,0);
    WriteFile(F,&CCol,sizeof(TColor),&a,0);
    WriteFile(F,&AICol,sizeof(TColor),&a,0);
    WriteFile(F,&BICol,sizeof(TColor),&a,0);
    WriteFile(F,&CICol,sizeof(TColor),&a,0);
    WriteFile(F,&AgCol,sizeof(TColor),&a,0);
    WriteFile(F,&BgCol,sizeof(TColor),&a,0);
    WriteFile(F,&CgCol,sizeof(TColor),&a,0);
    WriteFile(F,&AIgCol,sizeof(TColor),&a,0);
    WriteFile(F,&BIgCol,sizeof(TColor),&a,0);
    WriteFile(F,&CIgCol,sizeof(TColor),&a,0);
    WriteFile(F,&ABCol,sizeof(TColor),&a,0);
    WriteFile(F,&BCCol,sizeof(TColor),&a,0);
    WriteFile(F,&CACol,sizeof(TColor),&a,0);
    WriteFile(F,&ABgCol,sizeof(TColor),&a,0);
    WriteFile(F,&BCgCol,sizeof(TColor),&a,0);
    WriteFile(F,&CAgCol,sizeof(TColor),&a,0);
    WriteFile(F,&TimeCol,sizeof(TColor),&a,0);
    WriteFile(F,&ESKoef,sizeof(double),&a,0);
    CloseHandle(F);
   }
}
//---------------------------------------------------------------------------

void __fastcall TFMain::RGSFormatClick(TObject *Sender)
{
    WAVEFORMATEX  wfx;
    HCURSOR       hCursor;
    DSCBUFFERDESC dscbd;

    SAFE_RELEASE( g_pDSBCapture );
    ZeroMemory( &wfx, sizeof(wfx));
    wfx.wFormatTag = WAVE_FORMAT_PCM;

    ZeroMemory( &dscbd, sizeof(dscbd) );
    dscbd.dwSize = sizeof(dscbd);
    // Try 20 different standard formats to see if they are supported
    GetWaveFormatFromIndex( RGSFormat->ItemIndex, &wfx );
    dscbd.dwBufferBytes = wfx.nAvgBytesPerSec;
    dscbd.lpwfxFormat = &wfx;
    g_pDSCapture->CreateCaptureBuffer( &dscbd,&g_pDSBCapture,NULL );
    AnsiString S=IntToStr(wfx.nSamplesPerSec)+"Hz "+IntToStr(wfx.wBitsPerSample)+"-bit ";
   ( wfx.nChannels == 1 ) ? S+="Mono" : S+="Stereo";
    LSFormat->Caption=S;
    sound_regim=RGSFormat->ItemIndex;
    g_pDSBCapture->GetFormat( &wfxCapture, sizeof(WAVEFORMATEX), NULL );
//    OnSaveSoundFile();
}
//---------------------------------------------------------------------------
HRESULT TFMain::StartOrStopRecord( BOOL bStartRecording )
{
    if( bStartRecording )
    {
        CreateCaptureBuffer( &g_wfxInput );
        g_pDSBCapture->Start( DSCBSTART_LOOPING );
    }
    else
    {
        if( NULL == g_pDSBCapture )
            return S_OK;

        g_pDSBCapture->Stop();
        // Close the wav file
        SAFE_DELETE( g_pWaveFile );
    }

    return S_OK;
}
//-----------
int TFMain::OnSaveSoundFile()
{
    HRESULT hr;
    static TCHAR strFileName[MAX_PATH] = TEXT("");
    static TCHAR strPath[MAX_PATH] = TEXT("");

    if( g_bRecording )
    {
        // Stop the capture and read any data that
        // was not caught by a notification
        StartOrStopRecord( FALSE );
        g_bRecording = FALSE;
    }

    // Update the UI controls to show the sound as loading a file
    LProcessStatus->Caption="ProcessStatus: Recording & Saving . . .";
    BRecord->Enabled=false;

    SAFE_DELETE( g_pWaveFile );
    g_pWaveFile = new CWaveFile;
    if( NULL == g_pWaveFile )
        return 0;
    switch(cntOP){
      case 0: MessageBox(Handle,"����� ������ ������� � �����������\n ���� � ������� '��'","���������!",MB_OK);
               WDataA->Clear();  WDataC->Clear();  WDataB->Clear();
               WDataAB->Clear();  WDataBC->Clear();  WDataCA->Clear();
               WDataAi->Clear();  WDataCi->Clear();  WDataBi->Clear();
               WDataABG->Clear();  WDataBCG->Clear();  WDataCAG->Clear();
               WDataES->Clear();
               WDataAG->Clear(); WDataCG->Clear(); WDataBG->Clear();
               WDataAiG->Clear(); WDataCiG->Clear(); WDataBiG->Clear();
               BCalc->Enabled=true;
              Ptr_RBuf=WDataA;
              strcpy(strFileName,"base\\fazeA.wav"); break;
      case 1: MessageBox(Handle,"����� ������ ������� � �����������\n ���� B ������� '��'","���������!",MB_OK);
              Ptr_RBuf=WDataB;
              strcpy(strFileName,"base\\fazeB.wav"); break;
      case 2: MessageBox(Handle,"����� ������ ������� � �����������\n ���� C ������� '��'","���������!",MB_OK);
              Ptr_RBuf=WDataC;
              strcpy(strFileName,"base\\fazeC.wav"); break;
      case 6: MessageBox(Handle,"����� ������ ������� � �����������\n ���� ���� � ������� '��'","���������!",MB_OK);
              Ptr_RBuf=WDataAi;
              strcpy(strFileName,"base\\currA.wav"); break;
      case 7: MessageBox(Handle,"����� ������ ������� � �����������\n ���� ���� � ������� '��'","���������!",MB_OK);
              Ptr_RBuf=WDataBi;
              strcpy(strFileName,"base\\currB.wav"); break;
      case 8: MessageBox(Handle,"����� ������ ������� � �����������\n ���� ���� � ������� '��'","���������!",MB_OK);
              Ptr_RBuf=WDataCi;
              strcpy(strFileName,"base\\currC.wav"); break;
      case 3: MessageBox(Handle,"����� ������ ������� � �����������\n ���������� �������� ��� � � � ������� '��'","���������!",MB_OK);
              Ptr_RBuf=WDataAB;
              strcpy(strFileName,"base\\difAB.wav"); break;
      case 4: MessageBox(Handle,"����� ������ ������� � �����������\n ���������� �������� ��� B � C ������� '��'","���������!",MB_OK);
              Ptr_RBuf=WDataBC;
              strcpy(strFileName,"base\\difBC.wav"); break;
      case 5: MessageBox(Handle,"����� ������ ������� � �����������\n ���������� �������� ��� � � C ������� '��'","���������!",MB_OK);
              Ptr_RBuf=WDataCA;
              strcpy(strFileName,"base\\difCA.wav"); break;

      case 9: MessageBox(Handle,"����� ������ ������� � �����������\n �������� �������� ��������� ������� '��'","���������!",MB_OK);
              Ptr_RBuf=WDataES;
              strcpy(strFileName,"base\\difES.wav"); break;
/*
      case 10: MessageBox(Handle,"����� ������ ������� � �����������\n ���������� �������� ���� ��� B � C ������� '��'","���������!",MB_OK);
              Ptr_RBuf=WDataBCi;
              strcpy(strFileName,"base\\difBCi.wav"); break;
      case 11: MessageBox(Handle,"����� ������ ������� � �����������\n ���������� �������� ���� ��� B � C ������� '��'","���������!",MB_OK);
              Ptr_RBuf=WDataCAi;
              strcpy(strFileName,"base\\difCAi.wav"); break;
*/
      default:hr=MessageBox(Handle,"������ ��� ��������\n ������ ������ �������?","��������������",MB_YESNO|MB_ICONEXCLAMATION);
              if(hr==IDYES)
                     {cntOP=0;OnSaveSoundFile();BCalc->Caption="������";return 1;}
              return 0;
    }
    // Get the format of the capture buffer in g_wfxCaptureWaveFormat
//    WAVEFORMATEX wfxCaptureWaveFormat;
//    memcpy( &wfxCaptureWaveFormat,&g_wfxInput, sizeof(WAVEFORMATEX) );
//    g_pDSBCapture->GetFormat( &wfxCaptureWaveFormat, sizeof(WAVEFORMATEX), NULL );
    // Load the wave file
    if( FAILED( hr = g_pWaveFile->Open( strFileName, &wfxCapture, WAVEFILE_WRITE ) ) )
    {
        MessageBox(this,"Open","Error",MB_ICONERROR);
        LProcessStatus->Caption="Can not create wave file.";
        return 0;
    }
    cntOP++;
    // Update the UI controls to show the sound as the file is loaded
    LSFileName->Caption=strFileName;
    BRecord->Enabled=true;

    // Remember the path for next time
    strcpy( strPath, strFileName );
    char* strLastSlash = strrchr( strPath, '\\' );
    if( strLastSlash )
        strLastSlash[0] = '\0';
    return 1;
}
//---------------
//-----------------------------------------------------------------------------
// Name: RecordCapturedData()
// Desc: Copies data from the capture buffer to the output buffer
//-----------------------------------------------------------------------------
HRESULT TFMain::RecordCapturedData()
{
    usedR=true;
    HRESULT hr;
    VOID*   pbCaptureData    = NULL;
    DWORD   dwCaptureLength;
    VOID*   pbCaptureData2   = NULL;
    DWORD   dwCaptureLength2;
    UINT    dwDataWrote;
    DWORD   dwReadPos;
    DWORD   dwCapturePos;
    LONG lLockSize;

    if( NULL == g_pDSBCapture ){
        usedR=false;
        return S_FALSE;
    }
    if( NULL == g_pWaveFile ){
        usedR=false;
        return S_FALSE;
    }

    if( FAILED( hr = g_pDSBCapture->GetCurrentPosition( &dwCapturePos, &dwReadPos ) ) ){
        usedR=false;
        return MessageBox(NULL,"GetCurrentPosition","Error",MB_ICONERROR);
    }

    lLockSize = dwReadPos - g_dwNextCaptureOffset;
    if( lLockSize < 0 )
        lLockSize += g_dwCaptureBufferSize;

    // Block align lock size so that we are always write on a boundary
    lLockSize -= (lLockSize % g_dwNotifySize);

    if( lLockSize == 0 ){
        usedR=false;
        return S_FALSE;
    }

    // Lock the capture buffer down
    if( FAILED( hr = g_pDSBCapture->Lock( g_dwNextCaptureOffset, lLockSize,
                                          &pbCaptureData, &dwCaptureLength,
                                          &pbCaptureData2, &dwCaptureLength2, 0L ) ) ){
        usedR=false;
        return MessageBox(NULL,"Lock","Error",MB_ICONERROR);
    }

    // Write the data into the wav file
    if( FAILED( hr = g_pWaveFile->Write( dwCaptureLength,
                                              (BYTE*)pbCaptureData,
                                              &dwDataWrote ) ) ){
        usedR=false;
        return MessageBox(NULL,"Write","Error",MB_ICONERROR);
    }
    Ptr_RBuf->Addc((BYTE*)pbCaptureData,dwCaptureLength);
    // Move the capture offset along
    g_dwNextCaptureOffset += dwCaptureLength;
    g_dwNextCaptureOffset %= g_dwCaptureBufferSize; // Circular buffer

    if( pbCaptureData2 != NULL )
    {
        // Write the data into the wav file
        if( FAILED( hr = g_pWaveFile->Write( dwCaptureLength2,
                                                  (BYTE*)pbCaptureData2,
                                                  &dwDataWrote ) ) ){
            usedR=false;
            return MessageBox(NULL,"Write","Error",MB_ICONERROR);
        }
        // Move the capture offset along
        g_dwNextCaptureOffset += dwCaptureLength2;
        g_dwNextCaptureOffset %= g_dwCaptureBufferSize; // Circular buffer
    }
    Ptr_RBuf->Addc((BYTE*)pbCaptureData2,dwCaptureLength2);

    // Unlock the capture buffer
    g_pDSBCapture->Unlock( pbCaptureData,  dwCaptureLength,
                           pbCaptureData2, dwCaptureLength2 );


    usedR=false;
    return S_OK;
}
void __fastcall TFMain::BRecordClick(TObject *Sender)
{
  g_bRecording=!g_bRecording;
  if(g_bRecording){
      if(OnSaveSoundFile()){
         BRecord->Caption="����";
   //      Timer1->Enabled=false;
         BRecord->Enabled=false;
         TimerToRead->Enabled=true;
         g_bRecording=1;
         Calculated=false;
         BCalc->Caption="������";
         if(DBGrid1->Visible){
            DBGrid1->Visible=false;
            BGrHide->Visible=false;
            Panel4->Height-=200;
         }
      }
      else{
            BRecord->Enabled=true;
            TimerToRead->Enabled=false;
            BRecord->Caption="������";
            g_bRecording=0;
//            return;
      }
  }
  else{
      BRecord->Enabled=true;
      TimerToRead->Enabled=false;
      BRecord->Caption="Record";
      RepaintF();
//      Timer1->Enabled=true;
      g_bRecording=0;
  }
  if( FAILED( StartOrStopRecord( g_bRecording ) ) ){
       LProcessStatus->Caption="StartOrStopRecord";
       MessageBox( this, "Error with DirectSoundCapture buffer."
              "Sample will now exit.", "DirectSound Sample",
               MB_OK | MB_ICONERROR );
   }
}
//---------------------------------------------------------------------------
INT_PTR CALLBACK DSoundEnumCallback( GUID* pGUID, const char* strDesc, const char* strDrvName,
                                  VOID* pContext )
{
    // Set aside static storage space for 20 audio drivers

    GUID* pTemp  = NULL;

    if( pGUID )
    {
        if( dwAudioDriverIndex >= 20 )
            return TRUE;

        pTemp = &AudioDriverGUIDs[dwAudioDriverIndex++];
        memcpy( pTemp, pGUID, sizeof(GUID) );
        FMain->CBDevice->Items->Add(strDesc);
        // Add the string to the combo box

        // Get the index of the string in the combo box
        FMain->CBDevice->ItemIndex=0;
    }
    return TRUE;

}
//---------------------------------------------------------------------------


void __fastcall TFMain::ReSetAudio1Click(TObject *Sender)
{
/*
   TableA->Close();
    usedR=true;
    Calculated=false;
    SAFE_DELETE( g_pWaveFile );

    // Release DirectSound interfaces
    SAFE_RELEASE( g_pDSNotify );
    SAFE_RELEASE( g_pDSBCapture );
    SAFE_RELEASE( g_pDSCapture );

    CloseHandle( g_hNotificationEvent );
    // Release COM
    CoUninitialize();

    g_hNotificationEvent = CreateEvent( NULL, FALSE, FALSE, NULL );
    // Get the GUID attached to the combo box item
//    DirectSoundCaptureEnumerate( DSoundEnumCallback,this->Handle );
    InitDirectSound( this->Handle , &AudioDriverGUIDs[CBDevice->ItemIndex] );
*/
  PanAudioPref->Visible=true;
  PanAudioPref->BringToFront();
}
//---------------------------------------------------------------------------

void __fastcall TFMain::HideClick(TObject *Sender)
{
  PanAudioPref->Visible=false;
}
//---------------------------------------------------------------------------
//--------------------------------Graph--------------------------------
//---------------------------------------------------------------------------

void TFMain::DrawGrid(TImage *Im,int dX,int dY,bool Lx,bool Ly){
//  if(offU>0)Lvoff->Caption="+"+IntToStr(offU);
//  else Lvoff->Caption=IntToStr(offU);
  LOfftU->Caption=IntToStr(OfftU);
  LDenc->Caption="1:"+IntToStr(Denc);
    Im->CleanupInstance();
//    Im->Canvas->Font->Color=LablUCol;
    Im->Canvas->Font->Size=5;
    Im->Canvas->Brush->Color=BkCol;
    Im->Canvas->Pen->Color=BkCol;
    Im->Canvas->FillRect(TRect(0,0,Panel1->Width,Panel1->Height));
    Im->Canvas->Pen->Color= AxisCol;
    Im->Canvas->MoveTo(10,5);
    Im->Canvas->LineTo(10,Panel1->Height-5);
    Im->Canvas->MoveTo(500,5);
    Im->Canvas->LineTo(500,Panel1->Height-5);
    if(PanS->BevelOuter!=bvLowered){
       Im->Canvas->MoveTo(10,Panel1->Height/2);
       Im->Canvas->LineTo(Panel1->Width-10,Panel1->Height/2);
    }
    else{
       Im->Canvas->MoveTo(10,Panel1->Height-dY);
       Im->Canvas->LineTo(Panel1->Width-10,Panel1->Height-dY);
    }
    Im->Canvas->Pen->Color= GridCol;
    Im->Canvas->Font->Color=TimeCol;
    for(int i=1;i<Panel1->Width/dX;i++){
       Im->Canvas->MoveTo(10+i*dX,5);
       Im->Canvas->LineTo(10+i*dX,Panel1->Height-5);
       if(i%2==0)
       if(CBLX->Checked){
          if(PanS->BevelOuter!=bvLowered)
            Im->Canvas->TextOutA(10+i*dX,Panel1->Height/2-10,IntToStr(OfftU+(i*dX)/Denc));
          else{

              Im->Canvas->TextOutA(10+(i-1)*dX,Panel1->Height-20,"U�"+IntToStr(OfftS+i));
          }
       }
    }
   if(PanS->BevelOuter!=bvLowered){
    for(int i=1;i<Panel1->Height/(2*dY);i++){
       Im->Canvas->MoveTo(10,Panel1->Height/2+i*dY);
       Im->Canvas->LineTo(Panel1->Width-10,Panel1->Height/2+i*dY);
       Im->Canvas->MoveTo(10,Panel1->Height/2-i*dY);
       Im->Canvas->LineTo(Panel1->Width-10,Panel1->Height/2-i*dY);
    }
    if(CBLY->Checked){
                     // Placing Labels on Axis
     for(int i=0;i<Panel1->Height/dY;i++)
     {
      if(i%2==0){
              if((PanFA->BevelOuter==bvLowered)||
                 (PanFB->BevelOuter==bvLowered)||
                 (PanFC->BevelOuter==bvLowered)  )
              {
                Im->Canvas->Font->Color=LablUCol;
                if( ((PanFA->BevelOuter!=bvLowered)||((!IiA->Visible)&&(!IitA->Visible)))&&
                    ((PanFB->BevelOuter!=bvLowered)||((!IiB->Visible)&&(!IitB->Visible)))&&
                    ((PanFC->BevelOuter!=bvLowered)||((!IiC->Visible)&&(!IitC->Visible)))   )

                     Im->Canvas->TextOutA(0,i*dY,IntToStr((int)(offU+((int)Panel1->Height/2-i*dY)*mulU/Panel1->Height)));
                else{
                   if(((PanFA->BevelOuter==bvLowered)&&(IitA->Visible)&&(!IgA->Visible))||
                      ((PanFB->BevelOuter==bvLowered)&&(IitB->Visible)&&(!IgB->Visible))||
                      ((PanFC->BevelOuter==bvLowered)&&(IitC->Visible)&&(!IgC->Visible))||
                       ((PanFA->BevelOuter==bvLowered)&&(!IiA->Visible)&&(!IitA->Visible))||
                       ((PanFB->BevelOuter==bvLowered)&&(!IiB->Visible)&&(!IitB->Visible))||
                       ((PanFC->BevelOuter==bvLowered)&&(!IiC->Visible)&&(!IitC->Visible)) )
                     {
                     Im->Canvas->TextOutA(0,i*dY,myFloatToStr((float)offU+(float)(Panel1->Height/2-i*dY)*mulU/(float)Panel1->Height,2));
                     Im->Canvas->Font->Color=LablICol;
                     Im->Canvas->TextOutA(2*dX,i*dY,myFloatToStr((float)offI+(float)(Panel1->Height/2-i*dY)*mulI/(float)Panel1->Height,2));
                   }
                   else{
                     Im->Canvas->Font->Color=LablICol;
                     Im->Canvas->TextOutA(0,i*dY,myFloatToStr((float)offI+(float)(Panel1->Height/2-i*dY)*mulI/(float)Panel1->Height,2));
                   }
                }
              }//A
          } // if i%2 = 0    (parity)
        }// for ABC labels
     }// if labels enabled
   }// if !Spectr


   /*
   if(PanS->BevelOuter==bvLowered){
    TableA->Open();
    TableA->First();
    double val1=0;
    if(PanFA->BevelOuter==bvLowered)val1=TableA->FieldByName("UAi")->AsFloat;
    if(PanFB->BevelOuter==bvLowered)val1=TableA->FieldByName("UBi")->AsFloat;
    if(PanFC->BevelOuter==bvLowered)val1=TableA->FieldByName("UCi")->AsFloat;
    for(int i=1;i<Panel1->Height/dY;i++){
       Im->Canvas->MoveTo(10,Panel1->Height-i*dY);
       Im->Canvas->LineTo(Panel1->Width-10,Panel1->Height-i*dY);
       if(CBLY->Checked)
            Im->Canvas->TextOutA(0,Panel1->Height-i*dY-20,myFloatToStr(2*val1*mulS*i*dY/Panel1->Height,2));
    }
   }
   */
   ImTri->Canvas->Brush->Color=PanTri->Color;
   ImTri->Canvas->FillRect(TRect(0,0,ImTri->Width,ImTri->Height));
   ImTri->Canvas->Brush->Color=clRed;
   ImTri->Canvas->FillRect(TRect(ImTri->Width/2-2,ImTri->Height/2-2,
                                  ImTri->Width/2+2,ImTri->Height/2+2 ));
   ImTri->Canvas->Pen->Color=clBlack;
   float maxU;
   //if(UA>UB) maxU=UA; else maxU=UB;  if(maxU<UC) maxU=UC;
   maxU=UA;
   int A=50,Bx,By,Cx,Cy;
   if(!maxU) maxU=1;
   ImTri->Canvas->MoveTo(ImTri->Width/2,ImTri->Height/2);
   ImTri->Canvas->LineTo(ImTri->Width/2,ImTri->Height/2-A);
   ImTri->Canvas->FillRect(TRect(ImTri->Width/2-2,ImTri->Height/2-2-A,
                                  ImTri->Width/2+2,ImTri->Height/2+2-A ));
   Cx=(cos(-M_PI/6)*50.0)*UC/maxU;
   Cy=-(sin(-M_PI/6)*50.0)*UC/maxU;
   ImTri->Canvas->MoveTo(ImTri->Width/2,ImTri->Height/2);
   ImTri->Canvas->LineTo(ImTri->Width/2-Cx,ImTri->Height/2+Cy);
   ImTri->Canvas->FillRect(TRect(ImTri->Width/2-Cx-2,ImTri->Height/2+Cy-2,
                                 ImTri->Width/2-Cx+2,ImTri->Height/2+Cy+2 ));
   Bx=(cos(M_PI/6)*50.0)*UB/maxU;
   By=(sin(M_PI/6)*50.0)*UB/maxU;
   ImTri->Canvas->MoveTo(ImTri->Width/2,ImTri->Height/2);
   ImTri->Canvas->LineTo(ImTri->Width/2+Bx,ImTri->Height/2+By);
   ImTri->Canvas->FillRect(TRect(ImTri->Width/2+Bx-2,ImTri->Height/2+By-2,
                                 ImTri->Width/2+Bx+2,ImTri->Height/2+By+2 ));
   ImTri->Canvas->MoveTo(ImTri->Width/2,ImTri->Height/2-A);
   ImTri->Canvas->LineTo(ImTri->Width/2-Cx,ImTri->Height/2+Cy);
   ImTri->Canvas->LineTo(ImTri->Width/2+Bx,ImTri->Height/2+By);
   ImTri->Canvas->LineTo(ImTri->Width/2,ImTri->Height/2-A);

   ImTri->Canvas->Brush->Color=PanTri->Color;
   ImTri->Canvas->TextOutA(ImTri->Width/2,ImTri->Height/2-A,"A");
   ImTri->Canvas->TextOutA(ImTri->Width/2-Cx,ImTri->Height/2+Cy+5,"C");
   ImTri->Canvas->TextOutA(ImTri->Width/2+Bx,ImTri->Height/2+By-5,"B");
}

void TFMain::RepaintS(){
  if(!Calculated) return;
  if(PanS->BevelOuter!=bvLowered) return;

  ImFA->Visible=false;
  ImS->Canvas->CleanupInstance();
  ImS->Visible=true;
  DrawGrid(ImS,20,40,true,true);
  ImS->Canvas->Brush->Color=SCol;
  TableA->Open();
  TableA->First();
  double cell;
  double mul;
  AnsiString FS;
  if((PanFA->BevelOuter!=bvLowered)&&(PanFB->BevelOuter!=bvLowered)&&(PanFC->BevelOuter!=bvLowered)
     &&(PanAB->BevelOuter!=bvLowered)&&(PanBC->BevelOuter!=bvLowered)&&(PanCA->BevelOuter!=bvLowered))
      return;
  if(PanCA->BevelOuter==bvLowered) FS="UCA";
  if(PanBC->BevelOuter==bvLowered) FS="UBC";
  if(PanAB->BevelOuter==bvLowered) FS="UAB";
  if(PanFC->BevelOuter==bvLowered){
     if(IiC->Visible)FS="ICi";
     else FS="UCi";
  }
  if(PanFB->BevelOuter==bvLowered){
     if(IiB->Visible)FS="IBi";
     else FS="UBi";
  }
  if(PanFA->BevelOuter==bvLowered){
     if(IiA->Visible)FS="IAi";
     else FS="UAi";
  }
  mul=TableA->FieldByName(FS)->AsFloat;
  int i=0;
  while(i<OfftU){i++;TableA->Next();if(TableA->Eof) break;}
  if(i<OfftU){
    TableA->First();TableA->MoveBy(i-ImS->Width/20);
    OfftU=i-ImS->Width/20;
  }
  for(i=OfftU;i<_nFGar;i++){
   cell=TableA->FieldByName(FS)->AsFloat;
   ImS->Canvas->FillRect(TRect(10+(i-OfftS)*20+2 ,ImS->Height-ImS->Height*cell/(2*mul*mulS)-20,
                                  10+(i-OfftS+1)*20-2 ,ImS->Height-20));
   TableA->Next();
   if(TableA->Eof) break;
  }
   if(PanS->BevelOuter==bvLowered){
    TableA->Open();
    TableA->First();
    cell=TableA->FieldByName(FS)->AsFloat;
    for(int i=1;i<Panel1->Height/40;i++){
       ImS->Canvas->MoveTo(10,Panel1->Height-i*40);
       ImS->Canvas->LineTo(Panel1->Width-10,Panel1->Height-i*40);
       if(CBLY->Checked)
            ImS->Canvas->TextOutA(0,Panel1->Height-i*40-20,myFloatToStr(2*cell*mulS*i*40/Panel1->Height,2));
    }
   }
  LUi->Caption=FS;
  LUi->Visible=true;
}
//---------------------------------------------------------------------------
void TFMain::RepaintF(){
double Val;
if(usedR) return;
if(PanS->BevelOuter==bvLowered) return;
    ImFA->Canvas->CleanupInstance();
    ImS->Visible=false;
    ImFA->Visible=true;
    DrawGrid(ImFA,20,10,true,true);
bool vU,vUG,vI,vIG;
  LUi->Visible=false;
// ----------------- A --------------
     if(PanFA->BevelOuter==bvLowered){

        if((IiA->Visible==false)&&(IitA->Visible==false)){ vI=false;vIG=false;
            if((IgA->Visible==false)&&(IgtA->Visible==false))
                {vU=true;vUG=false;}
            else{vUG=true;
                 if(IgA->Visible==false) vU=true;
                 else vU=false;
            }
        }
        else{
          if(IitA->Visible==false){  vU=vUG=false;
             if(IgA->Visible==true)
               {vI=false;vIG=true;}
             else{
               if(IgtA->Visible==true){vI=vIG=true;}
               else {vI=true;vIG=false;}
             }
          }
          else{
             if(IgA->Visible==true){vI=vU=false;vIG=vUG=true;}
             else{
               if(IgtA->Visible==true) vI=vIG=vU=vUG=true;
               else{ vI=vU=true;   vIG=vUG=false;}
             }
          }
        }
        if(vU){
           if(OfftU>WDataA->wcount)OfftU=0;
           ImFA->Canvas->Pen->Color=ACol;
           int tmpOff =480;
           ImFA->Canvas->MoveTo(10+(tmpOff),ImFA->Height/2);
           for(int i=OfftU;i<WDataA->wcount-1;i++){
              Val=(signed short)WDataA->get_w(i);
              Val/=KUa;
              Val-=offU;
              Val*=(ImFA->Height/mulU);
              ImFA->Canvas->LineTo(10+(tmpOff)*Denc+(i-OfftU)*Denc*1.15,ImFA->Height/2-Val);
           }
        }
        if(vI){
           if(OfftU>WDataAi->wcount)OfftI=0;
           ImFA->Canvas->Pen->Color=AICol;
           int tmpOff =480;
           ImFA->Canvas->MoveTo(10+(tmpOff),ImFA->Height/2);
           for(int i=OfftI;i<WDataAi->wcount-1;i++){
              Val=(signed short)WDataAi->get_w(i);
              Val/=KIa;
              Val=(ImFA->Height*(Val-offI)/mulI);
              ImFA->Canvas->LineTo(10+(tmpOff)*Denc+(i-OfftI)*Denc*1.15,ImFA->Height/2-Val);
           }
        }
        if(vUG){
           if(OfftU>WDataAG->wcount)OfftU=0;
           ImFA->Canvas->Pen->Color=AgCol;
           ImFA->Canvas->MoveTo(10,ImFA->Height/2);
           for(int i=OfftU;i<WDataAG->wcount-1;i++){
              Val=(signed short)WDataAG->get_w(i);
              ImFA->Canvas->LineTo(10+(i-OfftU)*Denc,ImFA->Height/2-double(ImFA->Height)*(Val-offU)/mulU);
           }
        }
        if(vIG){
           if(OfftU>WDataAiG->wcount)OfftI=0;
           ImFA->Canvas->Pen->Color=AIgCol;
           ImFA->Canvas->MoveTo(10,ImFA->Height/2);
           for(int i=OfftI;i<WDataAiG->wcount-1;i++){
              Val=(signed short)WDataAiG->get_w(i);
              ImFA->Canvas->LineTo(10+(i-OfftI)*Denc,ImFA->Height/2-double(ImFA->Height)*(Val-offI)/mulI);
           }
        }

        PanS->BevelOuter=bvRaised;
        ImFA->BringToFront();
     }
    //------------------ B --
     if(PanFB->BevelOuter==bvLowered){

        if((IiB->Visible==false)&&(IitB->Visible==false)){ vI=false;vIG=false;
            if((IgB->Visible==false)&&(IgtB->Visible==false))
                {vU=true;vUG=false;}
            else{vUG=true;
                 if(IgB->Visible==false) vU=true;
                 else vU=false;
            }
        }
        else{
          if(IitB->Visible==false){  vU=vUG=false;
             if(IgB->Visible==true)
               {vI=false;vIG=true;}
             else{
               if(IgtB->Visible==true){vI=vIG=true;}
               else {vI=true;vIG=false;}
             }
          }
          else{
             if(IgB->Visible==true){vI=vU=false;vIG=vUG=true;}
             else{
               if(IgtB->Visible==true) vI=vIG=vU=vUG=true;
               else{ vI=vU=true;   vIG=vUG=false;}
             }
          }
        }
        if(vU){
           if(OfftU>WDataB->wcount)OfftU=0;
           ImFA->Canvas->Pen->Color=BCol;
           int tmpOff =240;
           ImFA->Canvas->MoveTo(10+(tmpOff),ImFA->Height/2);
           for(int i=OfftU;i<WDataB->wcount-1;i++){
              Val=((signed short)WDataB->get_w(i))/KUb;
              ImFA->Canvas->LineTo(10+(tmpOff)*Denc+(i+OfftU)*Denc*1.15,ImFA->Height/2-double(ImFA->Height)*(Val-offU)/mulU);
             }
             }
        if(vI){
           if(OfftU>WDataBi->wcount)OfftI=0;
           ImFA->Canvas->Pen->Color=BICol;
            int tmpOff =240;
           ImFA->Canvas->MoveTo(10+(tmpOff),ImFA->Height/2);
           for(int i=OfftI;i<WDataBi->wcount-1;i++){
              Val=((signed short)WDataBi->get_w(i))/KIb;
              ImFA->Canvas->LineTo(10+(tmpOff)*Denc+(i-OfftI)*Denc*1.15,ImFA->Height/2-double(ImFA->Height)*(Val-offI)/mulI);
           }
        }
        if(vUG){
           if(OfftU>WDataBG->wcount)OfftU=0;
           ImFA->Canvas->Pen->Color=BgCol;
           ImFA->Canvas->MoveTo(10,ImFA->Height/2);
           for(int i=OfftU;i<WDataBG->wcount-1;i++){
              Val=(signed short)WDataBG->get_w(i);
              ImFA->Canvas->LineTo(10+(i-OfftU)*Denc,ImFA->Height/2-double(ImFA->Height)*(Val-offU)/mulU);
           }
        }
        if(vIG){
           if(OfftU>WDataBiG->wcount)OfftI=0;
           ImFA->Canvas->Pen->Color=BIgCol;
           ImFA->Canvas->MoveTo(10,ImFA->Height/2);
           for(int i=OfftI;i<WDataBiG->wcount-1;i++){
              Val=(signed short)WDataBiG->get_w(i);
              ImFA->Canvas->LineTo(10+(i-OfftI)*Denc,ImFA->Height/2-ImFA->Height*(Val-offI)/mulI);
           }
        }

        PanS->BevelOuter=bvRaised;
        ImFA->BringToFront();
     }
//---------------- C -------------------------------
     if(PanFC->BevelOuter==bvLowered){

        if((IiC->Visible==false)&&(IitC->Visible==false)){ vI=false;vIG=false;
            if((IgC->Visible==false)&&(IgtC->Visible==false))
                {vU=true;vUG=false;}
            else{vUG=true;
                 if(IgC->Visible==false) vU=true;
                 else vU=false;
            }
        }
        else{
          if(IitC->Visible==false){  vU=vUG=false;
             if(IgC->Visible==true)
               {vI=false;vIG=true;}
             else{
               if(IgtC->Visible==true){vI=vIG=true;}
               else {vI=true;vIG=false;}
             }
          }
          else{
             if(IgC->Visible==true){vI=vU=false;vIG=vUG=true;}
             else{
               if(IgtC->Visible==true) vI=vIG=vU=vUG=true;
               else{ vI=vU=true;   vIG=vUG=false;}
             }
          }
        }
        if(vU){
           if(OfftU>WDataC->wcount)OfftU=0;
           ImFA->Canvas->Pen->Color=CCol;
           int tmpOff =360;
           ImFA->Canvas->MoveTo(10+(tmpOff),ImFA->Height/2);
           for(int i=OfftU;i<WDataC->wcount-1;i++){
              Val=((signed short)WDataC->get_w(i))/KUc;
              ImFA->Canvas->LineTo(10+(tmpOff)*Denc+(i-OfftU)*Denc*1.15,ImFA->Height/2-ImFA->Height*(Val-offU)/mulU);
           }
        }
        if(vI){
           if(OfftU>WDataCi->wcount)OfftI=0;
           ImFA->Canvas->Pen->Color=CICol;
            int tmpOff =360;
           ImFA->Canvas->MoveTo(10+(tmpOff),ImFA->Height/2);
           for(int i=OfftI;i<WDataCi->wcount-1;i++){
              Val=((signed short)WDataCi->get_w(i))/KIc;
              ImFA->Canvas->LineTo(10+(tmpOff)*Denc+(i-OfftI)*Denc*1.15,ImFA->Height/2-ImFA->Height*(Val-offI)/mulI);
           }
        }
        if(vUG){
           if(OfftU>WDataCG->wcount)OfftU=0;
           ImFA->Canvas->Pen->Color=CgCol;
           ImFA->Canvas->MoveTo(10,ImFA->Height/2);
           for(int i=OfftU;i<WDataCG->wcount-1;i++){
              Val=(signed short)WDataCG->get_w(i);
              ImFA->Canvas->LineTo(10+(i-OfftU)*Denc,ImFA->Height/2-ImFA->Height*(Val-offU)/mulU);
           }
        }
        if(vIG){
           if(OfftU>WDataCiG->wcount)OfftI=0;
           ImFA->Canvas->Pen->Color=CIgCol;
           ImFA->Canvas->MoveTo(10,ImFA->Height/2);
           for(int i=OfftI;i<WDataCiG->wcount-1;i++){
              Val=(signed short)WDataCiG->get_w(i);
              ImFA->Canvas->LineTo(10+(i-OfftI)*Denc,ImFA->Height/2-ImFA->Height*(Val-offI)/mulI);
           }
        }

        PanS->BevelOuter=bvRaised;
        ImFA->BringToFront();
     }
// ----------------- AB --------------
     if(PanAB->BevelOuter==bvLowered){

        if((IgAB->Visible==false)&&(IgtAB->Visible==false))
        {
            vUG=false;vU=true;
        }
        else{
           if(IgtAB->Visible==true){vUG=vU=true;}
           else{vUG=true;vU=false;}
        }
        if(vU){
           if(OfftU>WDataAB->wcount)OfftU=0;
           ImFA->Canvas->Pen->Color=ABCol;
           ImFA->Canvas->MoveTo(10,ImFA->Height/2);
           for(int i=OfftU;i<WDataAB->wcount-1;i++){
              Val=((signed short)WDataAB->get_w(i))/KUab;
              ImFA->Canvas->LineTo(10+(i-OfftU)*Denc,ImFA->Height/2-ImFA->Height*(Val-offU)/mulU);
           }
        }
        if(vUG){
           if(OfftU>WDataABG->wcount)OfftU=0;
           ImFA->Canvas->Pen->Color=ABgCol;
           ImFA->Canvas->MoveTo(10,ImFA->Height/2);
           for(int i=OfftI;i<WDataABG->wcount-1;i++){
              Val=(signed short)WDataABG->get_w(i);
              ImFA->Canvas->LineTo(10+(i-OfftU)*Denc,ImFA->Height/2-ImFA->Height*(Val-offU)/mulU);
           }
        }
        PanS->BevelOuter=bvRaised;
        ImFA->BringToFront();
     }
// ----------------- BC --------------
     if(PanBC->BevelOuter==bvLowered){

        if((IgBC->Visible==false)&&(IgtBC->Visible==false))
        {
            vUG=false;vU=true;
        }
        else{
           if(IgtBC->Visible==true){vUG=vU=true;}
           else{vUG=true;vU=false;}
        }
        if(vU){
           if(OfftU>WDataBC->wcount)OfftU=0;
           ImFA->Canvas->Pen->Color=BCCol;
           ImFA->Canvas->MoveTo(10,ImFA->Height/2);
           for(int i=OfftU;i<WDataBC->wcount-1;i++){
              Val=((signed short)WDataBC->get_w(i))/KUa;
              ImFA->Canvas->LineTo(10+(i-OfftU)*Denc,ImFA->Height/2-ImFA->Height*(Val-offU)/mulU);
           }
        }
        if(vUG){
           if(OfftU>WDataBCG->wcount)OfftU=0;
           ImFA->Canvas->Pen->Color=BCgCol;
           ImFA->Canvas->MoveTo(10,ImFA->Height/2);
           for(int i=OfftU;i<WDataBCG->wcount-1;i++){
              Val=(signed short)WDataBCG->get_w(i);
              ImFA->Canvas->LineTo(10+(i-OfftU)*Denc,ImFA->Height/2-ImFA->Height*(Val-offU)/mulU);
           }
        }
        PanS->BevelOuter=bvRaised;
        ImFA->BringToFront();
     }
// ----------------- CA --------------
     if(PanCA->BevelOuter==bvLowered){

        if((IgCA->Visible==false)&&(IgtCA->Visible==false))
        {
            vUG=false;vU=true;
        }
        else{
           if(IgtCA->Visible==true){vUG=vU=true;}
           else{vUG=true;vU=false;}
        }
        if(vU){
           if(OfftU>WDataCA->wcount)OfftU=0;
           ImFA->Canvas->Pen->Color=CACol;
           ImFA->Canvas->MoveTo(10,ImFA->Height/2);
           for(int i=OfftU;i<WDataCA->wcount-1;i++){
              Val=((signed short)WDataCA->get_w(i))/KUca;
              ImFA->Canvas->LineTo(10+(i-OfftU)*Denc,ImFA->Height/2-ImFA->Height*(Val-offU)/mulU);
           }
        }
        if(vUG){
           if(OfftU>WDataCAG->wcount)OfftU=0;
           ImFA->Canvas->Pen->Color=CAgCol;
           ImFA->Canvas->MoveTo(10,ImFA->Height/2);
           for(int i=OfftU;i<WDataCAG->wcount-1;i++){
              Val=(signed short)WDataCAG->get_w(i);
              ImFA->Canvas->LineTo(10+(i-OfftU)*Denc,ImFA->Height/2-ImFA->Height*(Val-offU)/mulU);
           }
        }
        PanS->BevelOuter=bvRaised;
        ImFA->BringToFront();
     }
/*
     LUA->Caption=" = "+IntToStr((int)UA);
     LUB->Caption=" = "+IntToStr((int)UB);
     LUC->Caption=" = "+IntToStr((int)UC);
     LIA->Caption=" = "+IntToStr((int)IA);
     LIB->Caption=" = "+IntToStr((int)IB);
     LIC->Caption=" = "+IntToStr((int)IC);
*/
}

void __fastcall TFMain::SpeedButton1Click(TObject *Sender)
{
  Denc++;RepaintF();
  LDenc->Caption="1:"+IntToStr(Denc);
}
//---------------------------------------------------------------------------

void __fastcall TFMain::SpeedButton2Click(TObject *Sender)
{
  Denc--;if(Denc<1) Denc=1;
  RepaintF();
  LDenc->Caption="1:"+IntToStr(Denc);
}
//---------------------------------------------------------------------------

void __fastcall TFMain::SpeedButton6Click(TObject *Sender)
{
  if(PanS->BevelOuter==bvLowered){ mulS/=1.5; RepaintS();return;}
  if(CBUZ->Checked) mulU/=1.5;
  if(CBIZ->Checked) mulI/=1.5;
  LUZoom->Caption="1x"+myFloatToStr(mulU,2);
  LIZoom->Caption="1x"+myFloatToStr(mulI,2);
  RepaintF();
}
//---------------------------------------------------------------------------

void __fastcall TFMain::SpeedButton5Click(TObject *Sender)
{
  if(PanS->BevelOuter==bvLowered){ mulS*=1.5; RepaintS();return;}
  if(CBUZ->Checked) mulU*=1.5;
  if(CBIZ->Checked) mulI*=1.5;
  LUZoom->Caption="1x"+myFloatToStr(mulU,2);
  LIZoom->Caption="1x"+myFloatToStr(mulI,2);
  RepaintF();
}
//---------------------------------------------------------------------------

void __fastcall TFMain::SpeedButton4Click(TObject *Sender)
{
  if(PanS->BevelOuter==bvLowered){
   OfftS++;if(OfftS>_nFGar)OfftS=_nFGar;
   LOfftU->Caption="S"+IntToStr(OfftS);
   RepaintS();
   return;
  }

  OfftU+=10;
  LOfftU->Caption=IntToStr(OfftU);
  RepaintF();
}
//---------------------------------------------------------------------------

void __fastcall TFMain::SpeedButton3Click(TObject *Sender)
{
  if(PanS->BevelOuter==bvLowered){
   OfftS--;if(OfftS<0)OfftS=0;
   LOfftU->Caption="S"+IntToStr(OfftS);
   RepaintS();
   return;
  }
  OfftU-=10;if(OfftU<0)OfftU=0;
  LOfftU->Caption=IntToStr(OfftU);
  RepaintF();
}
//---------------------------------------------------------------------------

void __fastcall TFMain::SpeedButton7Click(TObject *Sender)
{
  offU-=10;
  if(offU>=0)
      Lvoff->Caption="U+"+IntToStr(offU);
  else
      Lvoff->Caption="U"+IntToStr(offU);
  RepaintF();
}
//---------------------------------------------------------------------------

void __fastcall TFMain::SpeedButton8Click(TObject *Sender)
{
  offU+=10;
  if(offU>=0)
      Lvoff->Caption="U+"+IntToStr(offU);
  else
      Lvoff->Caption="U"+IntToStr(offU);
  RepaintF();
}
//---------------------------------------------------------------------------
void __fastcall TFMain::BCalcClick(TObject *Sender)
{
if(Sender!=NULL){
   if(DBGrid1->Visible) return;
   if(BCalc->Caption=="��������"){
      Panel4->Height+=200;
      DBGrid1->Visible=true;
      BGrHide->Visible=true;
      if(PanS->BevelOuter==bvLowered) RepaintS();
      else RepaintF();
      return;
   }
   if(cntOP<10) return;
   Panel4->Height+=200;
   if(!Calculated)
     Calculate();
}
else{
   Recalc=true;
   Calculate();
   DBGrid1->Visible=true;
   BGrHide->Visible=true;
   if(PanS->BevelOuter==bvLowered) RepaintS();
   else RepaintF();
}
}
//---------------------------------------------------------------------------

void __fastcall TFMain::BGrHideClick(TObject *Sender)
{
   DBGrid1->Visible=false;
   BGrHide->Visible=false;
   Panel4->Height-=200;
   if(PanS->BevelOuter==bvLowered) RepaintS();
   else RepaintF();
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//----------------------------Calculations-----------------------------------
//---------------------------------------------------------------------------
int TFMain::CreateNewTable(AnsiString _TabName){
   AnsiString S2,S;
   int expt;
   TableA->Close();
   TableB->Close();

   Query1->SQL->Clear();
   S="CREATE TABLE "+_TabName+"(No integer, UAi float,UBi float,UCi float,IAi float,IBi float,ICi float,\
                                            PsiA float,PsiB float,PsiC float,PsiAi float,PsiBi float,PsiCi float,\
                                            UAB float,UBC float,UCA float,\
                                            PsiAB float,PsiBC float,PsiCA float)";
   Query1->SQL->Add(S);
   try{
      Query1->ExecSQL();
   }
   catch(...){
    S2="� ���� ������ ��� ������� ������� � ������\n \
       \""+_TabName+"\". \n\
       ����� �������� ������ ������ ������ \n\
       ������������� �������     (���������),\n \
       ������������                (����������),\n \
       �������� ���������� ����������(��������)";

     expt=MessageBox(NULL,S2.c_str(),"Attention",MB_ICONEXCLAMATION|MB_ABORTRETRYIGNORE);
     switch(expt){
        case IDABORT:  return 0;
        case IDRETRY:
            FAskName->Label1->Caption="������� ����� ��� ��� ������� \""+_TabName+"\"";
            FAskName->ENewName->EnableAlign();
            FAskName->ShowModal();
            _TabName=FAskName->ENewName->Text;
            return CreateNewTable(_TabName);
        case IDIGNORE:
            Query1->SQL->Clear();
            S2="DROP TABLE "+_TabName;
            Query1->SQL->Add(S2);
            Query1->ExecSQL();
            Query1->SQL->Clear();
            Query1->SQL->Add(S);
            Query1->ExecSQL();
            break;
/*
            TableA->TableName="List";
            TableA->Open();
            TableA->First();
            while(TableA->FieldByName("Name")->AsString!=_TabName)
                  TableA->Next();
            TableA->Edit();
            TableA->FieldByName("Date")->AsDateTime=TDateTime::CurrentDate();
            TableA->Post();
            TableA->Close();
            return 1;
*/
     }
   }
   Query1->SQL->Clear();
   S="CREATE TABLE "+_TabName+"B(No integer,KUAi float,KUBi float,KUCi float,\
                       PAi float,PBi float,PCi float,QAi float,QBi float,QCi float)";
   Query1->SQL->Add(S);
   try{
      Query1->ExecSQL();
   }
   catch(...){
            Query1->SQL->Clear();
            S2="DROP TABLE "+_TabName+"B";
            Query1->SQL->Add(S2);
            Query1->ExecSQL();
            Query1->SQL->Clear();
            Query1->SQL->Add(S);
            Query1->ExecSQL();
   }


   TableA->TableName="List";
   TableA->Open();
   if(expt!=IDIGNORE)
   {
     TableA->Insert();
     TableA->FieldByName("Name")->AsString=_TabName;
     TableA->FieldByName("Date")->AsDateTime=TDateTime::CurrentDate();
   }
   else
   {
     TableA->First();
     while(TableA->FieldByName("Name")->AsString!=_TabName) TableA->Next();
     TableA->Edit();
     TableA->FieldByName("Date")->AsDateTime=TDateTime::CurrentDate();
   }
   TableA->Post();
   TableA->Close();
   TabName=_TabName;
   return 1;
}
//-----------------------------------------------------------------------------------
void TFMain::Calculate(){

CalculateEngineSpeed();

   TabName=TDateTime::CurrentDateTime().DateString();
   while(TabName.AnsiPos(".")) TabName.Delete(Sysutils::AnsiPos(".",TabName),1);
// if(SD1->Checked){
     FAskName->Label1->Caption="��� ��� ������� \""+TabName+"\"";
     FAskName->ENewName->Text=TabName;
     FAskName->ENewName->EnableAlign();
     FAskName->ShowModal();
     TabName=FAskName->ENewName->Text;
// }
   AnsiString S2,S;
   if(!CreateNewTable(TabName)) return;
   TableA->TableName=TabName;
   TableB->TableName=TabName+"B";
double P,B,Psi,C,Ui,alfa;
  if(!Recalc)    CutTo2pi();
  // ---- UA calculating -----
   alfa=2*M_PI/WDataA->wcount;
   TableA->Open();
   TableA->First();
   for(int i=0;i<_nFGar;i++){
      C=B=0;
      for(int j=0;j<WDataA->wcount;j++){
              B+=((short)WDataA->get_w(j))*sin(i*j*alfa)/KUa;
              C+=((short)WDataA->get_w(j))*cos(i*j*alfa)/KUa;
      }
      Ui=2*sqrt(B*B+C*C)/WDataA->wcount;
      if(i==0){Ua0=Ui;continue;}
      if(B!=0)Psi=atan(C/B);
      else Psi=0;
      if(B<0) Psi+=M_PI;
      TableA->Insert();
      TableA->FieldByName("No")->AsInteger=i;
      TableA->FieldByName("UAi")->AsFloat=Ui;
      TableA->FieldByName("PsiA")->AsFloat=180*Psi/M_PI;
   }
   TableA->Post();
// ---- IA calculating -----
   alfa=2*M_PI/WDataAi->wcount;
   TableA->Close();
   TableA->Open();
   TableA->First();
   TableB->Close();
   TableB->Open();
   TableB->First();

   for(int i=0;i<_nFGar;i++){
      C=B=0;
      for(int j=0;j<WDataAi->wcount;j++){
              B+=((short)WDataAi->get_w(j))*sin(i*j*alfa)/KIa;
              C+=((short)WDataAi->get_w(j))*cos(i*j*alfa)/KIa;
      }
      Ui=2*sqrt(B*B+C*C)/WDataAi->wcount;
      if(i==0){Ia0=Ui;continue;}
      if(B!=0)Psi=atan(C/B);
      else Psi=0;
      if(B<0) Psi+=M_PI;
      TableA->Edit();
      TableB->Insert();
      TableB->FieldByName("No")->AsInteger=i;

      TableA->FieldByName("IAi")->AsFloat=Ui;
      TableA->FieldByName("PsiAi")->AsFloat=180*Psi/M_PI;
      P=(Ui*TableA->FieldByName("UAi")->AsFloat/2)*
        cos(M_PI*TableA->FieldByName("PsiA")->AsFloat/180-Psi);
      TableB->FieldByName("PAi")->AsFloat=P;
      P=(Ui*TableA->FieldByName("UAi")->AsFloat/2)*
        sin(M_PI*TableA->FieldByName("PsiA")->AsFloat/180-Psi);
      TableB->FieldByName("QAi")->AsFloat=P;
      C=P;
      B=TableB->FieldByName("PAi")->AsFloat;
      P=sqrt(C*C+B*B);
//      TableA->FieldByName("SAi")->AsFloat=P;
      TableA->Next();
   }
   //TableA->Post();
// ---- UB calculating -----
   alfa=2*M_PI/WDataB->wcount;
   TableA->Close();
   TableA->Open();
   TableA->First();
   for(int i=0;i<_nFGar;i++){
      C=B=0;
      for(int j=0;j<WDataB->wcount;j++){
              B+=((short)WDataB->get_w(j))*sin(i*j*alfa)/KUb;
              C+=((short)WDataB->get_w(j))*cos(i*j*alfa)/KUb;
      }
      Ui=2*sqrt(B*B+C*C)/WDataB->wcount;
      if(i==0){Ub0=Ui;continue;}
      if(B!=0)Psi=atan(C/B);
      else Psi=0;
      if(B<0) Psi+=M_PI;
      TableA->Edit();
      TableA->FieldByName("UBi")->AsFloat=Ui;
      TableA->FieldByName("PsiB")->AsFloat=180*Psi/M_PI;
      TableA->Next();
   }
   //TableA->Post();
// ---- IB calculating -----
   alfa=2*M_PI/WDataBi->wcount;
   TableA->Close();
   TableA->Open();
   TableA->First();
   TableB->Close();
   TableB->Open();
   TableB->First();

   for(int i=0;i<_nFGar;i++){
      C=B=0;
      for(int j=0;j<WDataBi->wcount;j++){
              B+=((short)WDataBi->get_w(j))*sin(i*j*alfa)/KIb;
              C+=((short)WDataBi->get_w(j))*cos(i*j*alfa)/KIb;
      }
      Ui=2*sqrt(B*B+C*C)/WDataBi->wcount;
      if(i==0){Ib0=Ui;continue;}
      if(B!=0)Psi=atan(C/B);
      else Psi=0;
      if(B<0) Psi+=M_PI;
      TableA->Edit();
      TableB->Edit();
      TableA->FieldByName("IBi")->AsFloat=Ui;
      TableA->FieldByName("PsiBi")->AsFloat=180*Psi/M_PI;
      P=(Ui*TableA->FieldByName("UBi")->AsFloat/2)*
        cos(M_PI*TableA->FieldByName("PsiB")->AsFloat/180-Psi);
      TableB->FieldByName("PBi")->AsFloat=P;
      P=(Ui*TableA->FieldByName("UBi")->AsFloat/2)*
        sin(M_PI*TableA->FieldByName("PsiB")->AsFloat/180-Psi);
      TableB->FieldByName("QBi")->AsFloat=P;
      C=P;
      B=TableB->FieldByName("PBi")->AsFloat;
      P=sqrt(C*C+B*B);
//      TableA->FieldByName("SBi")->AsFloat=P;
      TableA->Next();
      TableB->Next();
   }
   //TableA->Post();
// ---- UC calculating -----
   alfa=2*M_PI/WDataC->wcount;
   TableA->Close();
   TableA->Open();
   TableA->First();
   for(int i=0;i<_nFGar;i++){
      C=B=0;
      for(int j=0;j<WDataC->wcount;j++){
              B+=((short)WDataC->get_w(j))*sin(i*j*alfa)/KUc;
              C+=((short)WDataC->get_w(j))*cos(i*j*alfa)/KUc;
      }
      Ui=2*sqrt(B*B+C*C)/WDataC->wcount;
      if(i==0){Uc0=Ui;continue;}
      if(B!=0)Psi=atan(C/B);
      else Psi=0;
      if(B<0) Psi+=M_PI;
      TableA->Edit();
      TableA->FieldByName("UCi")->AsFloat=Ui;
      TableA->FieldByName("PsiC")->AsFloat=180*Psi/M_PI;
      TableA->Next();
   }
   //TableA->Post();
// ---- IC calculating -----
   alfa=2*M_PI/WDataCi->wcount;
   TableA->Close();
   TableA->Open();
   TableA->First();
   TableB->Close();
   TableB->Open();
   TableB->First();

   for(int i=0;i<_nFGar;i++){
      C=B=0;
      for(int j=0;j<WDataCi->wcount;j++){
              B+=((short)WDataCi->get_w(j))*sin(i*j*alfa)/KIc;
              C+=((short)WDataCi->get_w(j))*cos(i*j*alfa)/KIc;
      }
      Ui=2*sqrt(B*B+C*C)/WDataCi->wcount;
      if(i==0){Ic0=Ui;continue;}
      if(B!=0)Psi=atan(C/B);
      else Psi=0;
      if(B<0) Psi+=M_PI;
      TableA->Edit();
      TableB->Edit();
      TableA->FieldByName("ICi")->AsFloat=Ui;
      TableA->FieldByName("PsiCi")->AsFloat=180*Psi/M_PI;
      P=(Ui*TableA->FieldByName("UCi")->AsFloat/2)*
        cos(M_PI*TableA->FieldByName("PsiC")->AsFloat/180-Psi);
      TableB->FieldByName("PCi")->AsFloat=P;
      P=(Ui*TableA->FieldByName("UCi")->AsFloat/2)*
        sin(M_PI*TableA->FieldByName("PsiC")->AsFloat/180-Psi);
      TableB->FieldByName("QCi")->AsFloat=P;
      C=P;
      B=TableB->FieldByName("PCi")->AsFloat;
      P=sqrt(C*C+B*B);
//      TableB->FieldByName("SCi")->AsFloat=P;

      TableA->Next();
      TableB->Next();
   }
//----AB-BC-CA

// ---- UAB calculating -----
   alfa=2*M_PI/WDataAB->wcount;
   TableA->Close();
   TableA->Open();
   TableA->First();
   for(int i=0;i<_nFGar;i++){
      C=B=0;
      for(int j=0;j<WDataAB->wcount;j++){
              B+=((short)WDataAB->get_w(j))*sin(i*j*alfa)/KUab;
              C+=((short)WDataAB->get_w(j))*cos(i*j*alfa)/KUab;
      }
      Ui=2*sqrt(B*B+C*C)/WDataAB->wcount;
      if(i==0){Uab0=Ui;continue;}
      if(B!=0)Psi=atan(C/B);
      else Psi=0;
      if(B<0) Psi+=M_PI;
      TableA->Edit();
      TableA->FieldByName("UAB")->AsFloat=Ui;
      TableA->FieldByName("PsiAB")->AsFloat=180*Psi/M_PI;
      TableA->Next();
   }
   //TableA->Post();
// ---- UBC calculating -----
   alfa=2*M_PI/WDataBC->wcount;
   TableA->Close();
   TableA->Open();
   TableA->First();
   for(int i=0;i<_nFGar;i++){
      C=B=0;
      for(int j=0;j<WDataBC->wcount;j++){
              B+=((short)WDataBC->get_w(j))*sin(i*j*alfa)/KUbc;
              C+=((short)WDataBC->get_w(j))*cos(i*j*alfa)/KUbc;
      }
      Ui=2*sqrt(B*B+C*C)/WDataAB->wcount;
      if(i==0){Ubc0=Ui;continue;}
      if(B!=0)Psi=atan(C/B);
      else Psi=0;
      if(B<0) Psi+=M_PI;
      TableA->Edit();
      TableA->FieldByName("UBC")->AsFloat=Ui;
      TableA->FieldByName("PsiBC")->AsFloat=180*Psi/M_PI;
      TableA->Next();
   }
   //TableA->Post();
// ---- UCA calculating -----
   alfa=2*M_PI/WDataCA->wcount;
   TableA->Close();
   TableA->Open();
   TableA->First();
   for(int i=0;i<_nFGar;i++){
      C=B=0;
      for(int j=0;j<WDataCA->wcount;j++){
              B+=((short)WDataCA->get_w(j))*sin(i*j*alfa)/KUca;
              C+=((short)WDataCA->get_w(j))*cos(i*j*alfa)/KUca;
      }
      Ui=2*sqrt(B*B+C*C)/WDataCA->wcount;
      if(i==0){Uca0=Ui;continue;}
      if(B!=0)Psi=atan(C/B);
      else Psi=0;
      if(B<0) Psi+=M_PI;
      TableA->Edit();
      TableA->FieldByName("UCA")->AsFloat=Ui;
      TableA->FieldByName("PsiCA")->AsFloat=180*Psi/M_PI;
      TableA->Next();
   }
   //TableA->Post();
//---------------------
   Calculated=true;
   //TableA->Post();
   TableA->Close();
   TableA->Open();
   TableB->Close();
   TableB->Open();

   double F,Val,ValA,ValAi,ValB,ValBi,ValC,ValCi,ValAB,ValBC,ValCA;
   short sVal;
   double sqA,sqAi,sqB,sqBi,sqC,sqCi,sqAB,sqBC,sqCA;
   int vcnt=WDataA->wcount;
   if(WDataAi->wcount>vcnt)vcnt=WDataAi->wcount;
   if(WDataB->wcount>vcnt)vcnt=WDataB->wcount;
   if(WDataBi->wcount>vcnt)vcnt=WDataBi->wcount;
   if(WDataC->wcount>vcnt)vcnt=WDataC->wcount;
   if(WDataCi->wcount>vcnt)vcnt=WDataCi->wcount;
   if(WDataAB->wcount>vcnt)vcnt=WDataAB->wcount;
   if(WDataBC->wcount>vcnt)vcnt=WDataBC->wcount;
   if(WDataCA->wcount>vcnt)vcnt=WDataCA->wcount;
   sqA=sqAi=sqB=sqBi=sqC=sqCi=0;
   sqAB=sqBC=sqCA=0;
   double w=M_PI*2/(vcnt);
   for(int i=0;i<vcnt;i++){
      ValA=ValAi=ValB=ValBi=ValC=ValCi=0;
      ValAB=ValBC=ValCA=0;
      TableA->First();
      for(int j=1;j<=_nFGar;j++){
         if(WDataA->wcount>i){
                Val=(TableA->FieldByName("UAi")->AsFloat);
                F=M_PI*TableA->FieldByName("PsiA")->AsFloat/180.0;
                ValA+=Val*sin(j*i*w+F);
                if(!i)sqA+=Val*Val/2;
         }
         if(WDataAi->wcount>i){
                Val=(TableA->FieldByName("IAi")->AsFloat);
                F=M_PI*TableA->FieldByName("PsiAi")->AsFloat/180;
                ValAi+=Val*sin(j*i*w+F);
                if(!i)sqAi+=Val*Val/2;
         }
         if(WDataB->wcount>i){
                Val=(TableA->FieldByName("UBi")->AsFloat);
                F=M_PI*TableA->FieldByName("PsiB")->AsFloat/180;
                ValB+=Val*sin(F+i*j*w);
                if(!i)sqB+=Val*Val/2;
         }
         if(WDataBi->wcount>i){
                Val=(TableA->FieldByName("IBi")->AsFloat);
                F=M_PI*TableA->FieldByName("PsiBi")->AsFloat/180;
                ValBi+=Val*sin(j*i*w+F);
                if(!i)sqBi+=Val*Val/2;
         }
         if(WDataC->wcount>i){
                Val=(TableA->FieldByName("UCi")->AsFloat);
                F=M_PI*TableA->FieldByName("PsiC")->AsFloat/180;
                ValC+=Val*sin(j*i*w+F);
                if(!i)sqC+=Val*Val/2;
         }
         if(WDataCi->wcount>i){
                Val=(TableA->FieldByName("ICi")->AsFloat);
                F=M_PI*TableA->FieldByName("PsiCi")->AsFloat/180;
                ValCi+=Val*sin(j*i*w+F);
                if(!i)sqCi+=Val*Val/2;
         }
         if(WDataAB->wcount>i){
                Val=(TableA->FieldByName("UAB")->AsFloat);
                F=M_PI*TableA->FieldByName("PsiAB")->AsFloat/180;
                ValAB+=Val*sin(j*i*w+F);
                if(!i)sqAB+=Val*Val/2;
         }
         if(WDataBC->wcount>i){
                Val=(TableA->FieldByName("UBC")->AsFloat);
                F=M_PI*TableA->FieldByName("PsiBC")->AsFloat/180;
                ValBC+=Val*sin(j*i*w+F);
                if(!i)sqBC+=Val*Val/2;
         }
         if(WDataCA->wcount>i){
                Val=(TableA->FieldByName("UCA")->AsFloat);
                F=M_PI*TableA->FieldByName("PsiCA")->AsFloat/180;
                ValCA+=Val*sin(j*i*w+F);
                if(!i)sqCA+=Val*Val/2;
         }
         TableA->Next();
      }
//      ValA+=Ua0;ValAi+=Ia0;ValB+=Ub0;ValBi+=Ib0;ValC+=Uc0;ValCi+=Ic0;
//      WDataAG->Addc((unsigned char*)&ValA,2); WDataAiG->Addc((unsigned char*)&ValAi,2);
//      WDataBG->Addc((unsigned char*)&ValB,2); WDataBiG->Addc((unsigned char*)&ValBi,2);
//      WDataCG->Addc((unsigned char*)&ValC,2); WDataCiG->Addc((unsigned char*)&ValCi,2);
        sVal=ValA;WDataAG->wData[i]=sVal; sVal=ValAi;WDataAiG->wData[i]=sVal;
        sVal=ValB;WDataBG->wData[i]=sVal; sVal=ValBi;WDataBiG->wData[i]=sVal;
        sVal=ValC;WDataCG->wData[i]=sVal; sVal=ValCi;WDataCiG->wData[i]=sVal;
        sVal=ValAB;WDataABG->wData[i]=sVal;
        sVal=ValBC;WDataBCG->wData[i]=sVal;
        sVal=ValCA;WDataCAG->wData[i]=sVal;
   }
   sqA+=Ua0*Ua0;sqAi+=Ia0*Ia0; sqB+=Ub0*Ub0;sqBi+=Ib0*Ib0; sqC+=Uc0*Uc0;sqCi+=Ic0*Ic0;
   sqAB+=Uab0*Uab0; sqBC+=Ubc0*Ubc0; sqCA+=Uca0*Uca0;
   UA=sqrt(sqA);UB=sqrt(sqB);UC=sqrt(sqC);IA=sqrt(sqAi);IB=sqrt(sqBi);IC=sqrt(sqCi);
   UAB=sqrt(sqAB);  UBC=sqrt(sqBC);  UCA=sqrt(sqCA);
   WDataAG->wcount=WDataA->wcount;WDataAiG->wcount=WDataAi->wcount;
   WDataAG->ccount=WDataA->ccount;WDataAiG->ccount=WDataAi->ccount;
   WDataBG->wcount=WDataB->wcount;WDataBiG->wcount=WDataBi->wcount;
   WDataBG->ccount=WDataB->ccount;WDataBiG->ccount=WDataBi->ccount;
   WDataCG->wcount=WDataC->wcount;WDataCiG->wcount=WDataCi->wcount;
   WDataCG->ccount=WDataC->ccount;WDataCiG->ccount=WDataCi->ccount;
   WDataABG->ccount=WDataAB->ccount; WDataABG->wcount=WDataAB->wcount;
   WDataBCG->ccount=WDataBC->ccount; WDataBCG->wcount=WDataBC->wcount;
   WDataCAG->ccount=WDataCA->ccount; WDataCAG->wcount=WDataCA->wcount;

    AnsiString filename; filename="base\\"+TabName+".rec";
    DWORD a;
    HANDLE RegFile=CreateFile(filename.c_str(),GENERIC_WRITE,0,0,CREATE_ALWAYS,0,0);

//    @@
    Pa0=Ua0*Ia0;Pb0=Ub0*Ib0;Pc0=Uc0*Ic0;
    TableB->Close();    TableB->Open();     TableB->First();
    lPA=Pa0;lPB=Pb0;lPC=Pc0;
    while(!TableB->Eof){
        lPA+=(TableB->FieldByName("PAi")->AsFloat);
        lPB+=(TableB->FieldByName("PBi")->AsFloat);
        lPC+=(TableB->FieldByName("PCi")->AsFloat);
        TableB->Next();
    }
    lP0=Pa0+Pb0+Pc0;
    lP=lP0+lPA+lPB+lPC;
    double Pg=PEdit->Text.ToDouble();
    int n_d=NEdit->Text.ToInt();
    ldP=lP-Pg/Eth_g;
//    lM=9.550*Pg/(n_d*Eth_g);
    if(lP)lNd=Pg/(Eth_g*lP);
    else lNd=0;
    lM=9.55*lP*lNd/n_d;
    double tmp;
    if(UAB)tmp=(UBC*UBC-UCA*UCA)/UAB;
    else tmp=0;
    if(2*UBC<tmp+UAB) lU1=sqrt(3)*UAB;
    else lU1=sqrt(3)*UAB+sqrt(4*UBC*UBC-(tmp+UAB)*(tmp+UAB));
        lU1*=lU1;
            lU1+=(tmp*tmp);
                if(lU1<0) lU1=0;
                else lU1=sqrt(lU1/12);
    if(2*UBC<tmp+UAB) lU2=sqrt(3)*UAB;
    else lU2=sqrt(3)*UAB-sqrt(4*UBC*UBC-(tmp+UAB)*(tmp+UAB));
        lU2*=lU2;
            lU2+=(tmp);     //  ???
                if(lU2<0) lU2=0;
                else lU2=sqrt(lU2/12);
    double tmp2;
    if(UAB)tmp2=(UB*UB-UA*UA)/UAB;
    else tmp2=0;
    lU0=(tmp-3*tmp2)*(tmp-3*tmp2);
        if(2*UBC>fabs(UAB-tmp))
            lU0+=sqrt(4*UBC*UBC-(UAB-tmp)*(UAB-tmp));
        if(2*UB>fabs(tmp2-UAB))
            lU0-=(3*sqrt(4*UB*UB-(UAB-tmp2)*(UAB-tmp2)) );
                if(lU0<0) lU0=0;
                else lU0=sqrt(lU0)/6;
    if(lU1){
      lK0U=sqrt(3)*lU0*100/lU1;
      lK2U=lU2*100/lU1;
    }
    else{lK0U=lK2U=0;}
    TableB->Close();    TableB->Open();     TableB->First();
    TableA->Close();    TableA->Open();     TableA->First();
    lKUA=lKUB=lKUC=0;
    for(int i=0;i<=_nFGar;i++){
        TableB->Edit();
        tmp=TableA->FieldByName("UAi")->AsFloat;
        TableB->FieldByName("KUAi")->AsFloat=tmp*sqrt(3)/lU1;
        if(i>0)lKUA+=(tmp*tmp);
        tmp=TableA->FieldByName("UBi")->AsFloat;
        TableB->FieldByName("KUBi")->AsFloat=tmp*sqrt(3)/lU1;
        if(i>0)lKUB+=(tmp*tmp);
        tmp=TableA->FieldByName("UCi")->AsFloat;
        TableB->FieldByName("KUCi")->AsFloat=tmp*sqrt(3)/lU1;
        if(i>0)lKUC+=(tmp*tmp);
        TableB->Next();TableA->Next();
    }
    lKUA=sqrt(3*lKUA/2)/lU1;
    lKUB=sqrt(3*lKUB/2)/lU1;
    lKUC=sqrt(3*lKUC/2)/lU1;
    lsU=100*lU1/380-100;

    WriteFile(RegFile,&WDataA->wcount,2,&a,0);WriteFile(RegFile,WDataA->cData,WDataA->ccount,&a,0);
    WriteFile(RegFile,&WDataB->wcount,2,&a,0);WriteFile(RegFile,WDataB->cData,WDataB->ccount,&a,0);
    WriteFile(RegFile,&WDataC->wcount,2,&a,0);WriteFile(RegFile,WDataC->cData,WDataC->ccount,&a,0);
    WriteFile(RegFile,&WDataAi->wcount,2,&a,0);WriteFile(RegFile,WDataAi->cData,WDataAi->ccount,&a,0);
    WriteFile(RegFile,&WDataBi->wcount,2,&a,0);WriteFile(RegFile,WDataBi->cData,WDataBi->ccount,&a,0);
    WriteFile(RegFile,&WDataCi->wcount,2,&a,0);WriteFile(RegFile,WDataCi->cData,WDataCi->ccount,&a,0);
    WriteFile(RegFile,&WDataAG->wcount,2,&a,0);WriteFile(RegFile,WDataAG->cData,WDataAG->ccount,&a,0);
    WriteFile(RegFile,&WDataBG->wcount,2,&a,0);WriteFile(RegFile,WDataBG->cData,WDataBG->ccount,&a,0);
    WriteFile(RegFile,&WDataCG->wcount,2,&a,0);WriteFile(RegFile,WDataCG->cData,WDataCG->ccount,&a,0);
    WriteFile(RegFile,&WDataAiG->wcount,2,&a,0);WriteFile(RegFile,WDataAiG->cData,WDataAiG->ccount,&a,0);
    WriteFile(RegFile,&WDataBiG->wcount,2,&a,0);WriteFile(RegFile,WDataBiG->cData,WDataBiG->ccount,&a,0);
    WriteFile(RegFile,&WDataCiG->wcount,2,&a,0);WriteFile(RegFile,WDataCiG->cData,WDataCiG->ccount,&a,0);
    WriteFile(RegFile,&WDataAB->wcount,2,&a,0);WriteFile(RegFile,WDataAB->cData,WDataAB->ccount,&a,0);
    WriteFile(RegFile,&WDataBC->wcount,2,&a,0);WriteFile(RegFile,WDataBC->cData,WDataBC->ccount,&a,0);
    WriteFile(RegFile,&WDataCA->wcount,2,&a,0);WriteFile(RegFile,WDataCA->cData,WDataCA->ccount,&a,0);
    WriteFile(RegFile,&WDataABG->wcount,2,&a,0);WriteFile(RegFile,WDataABG->cData,WDataABG->ccount,&a,0);
    WriteFile(RegFile,&WDataBCG->wcount,2,&a,0);WriteFile(RegFile,WDataBCG->cData,WDataBCG->ccount,&a,0);
    WriteFile(RegFile,&WDataCAG->wcount,2,&a,0);WriteFile(RegFile,WDataCAG->cData,WDataCAG->ccount,&a,0);

    WriteFile(RegFile,&lPA,sizeof(double),&a,0);    WriteFile(RegFile,&lPB,sizeof(double),&a,0);
    WriteFile(RegFile,&lPC,sizeof(double),&a,0);    WriteFile(RegFile,&ldP,sizeof(double),&a,0);
    WriteFile(RegFile,&lP,sizeof(double),&a,0);     WriteFile(RegFile,&lP0,sizeof(double),&a,0);
    WriteFile(RegFile,&lM,sizeof(double),&a,0);     WriteFile(RegFile,&lNd,sizeof(double),&a,0);
    WriteFile(RegFile,&lsU,sizeof(double),&a,0);    WriteFile(RegFile,&lU1,sizeof(double),&a,0);
    WriteFile(RegFile,&lU2,sizeof(double),&a,0);    WriteFile(RegFile,&lU0,sizeof(double),&a,0);
    WriteFile(RegFile,&lK2U,sizeof(double),&a,0);   WriteFile(RegFile,&lK0U,sizeof(double),&a,0);
    WriteFile(RegFile,&lKUA,sizeof(double),&a,0);   WriteFile(RegFile,&lKUB,sizeof(double),&a,0);
    WriteFile(RegFile,&lKUC,sizeof(double),&a,0);

    WriteFile(RegFile,&UA,sizeof(double),&a,0);    WriteFile(RegFile,&UB,sizeof(double),&a,0);
    WriteFile(RegFile,&UC,sizeof(double),&a,0);

    WriteFile(RegFile,&IA,sizeof(double),&a,0);    WriteFile(RegFile,&IB,sizeof(double),&a,0);
    WriteFile(RegFile,&IC,sizeof(double),&a,0);

    WriteFile(RegFile,&UAB,sizeof(double),&a,0);    WriteFile(RegFile,&UBC,sizeof(double),&a,0);
    WriteFile(RegFile,&UCA,sizeof(double),&a,0);

    ParForm->PA=lPA;     ParForm->PB=lPB;    ParForm->PC=lPC;
    ParForm->P=lP;       ParForm->dP=ldP;    ParForm->P0=lP0;
    ParForm->PA=lPA;     ParForm->Md=lM;     ParForm->Nd=lNd;
    ParForm->sU=lsU;     ParForm->U1=lU1;    ParForm->U2=lU2;
    ParForm->U0=lU0;     ParForm->K2U=lK2U;  ParForm->K0U=lK0U;
    ParForm->KUA=lKUA;   ParForm->KUB=lKUB;  ParForm->KUC=lKUC;
    ParItm->Enabled=true; GarmItm->Enabled=true;
    ParForm->Visible=true;
    ParForm->BringToFront();
    GarmForm->Visible=true;
    GarmForm->BringToFront();
    GarmForm->DBGrid1->DataSource=DataSource2;
    GarmForm->DBGrid1->Refresh();

    LUA->Caption="UA="+FloatToStr(UA);
    LUB->Caption="UB="+FloatToStr(UB);
    LUC->Caption="UC="+FloatToStr(UC);
    LUAB->Caption="UAB="+FloatToStr(UAB);
    LUBC->Caption="UBC="+FloatToStr(UBC);
    LUCA->Caption="UCA="+FloatToStr(UCA);
    LIA->Caption="IA="+FloatToStr(IA);
    LIB->Caption="IB="+FloatToStr(IB);
    LIC->Caption="IC="+FloatToStr(IC);

/*
  double  PA,PB,PC,dP,P,P0,Md,Nd;    // ��������� ���������
  double  sU,U1,U2,U0,K2U,K0U;     // ������������ �����������
  double  KUA,KUB,KUC;          // ������������ ������������������
*/

//    WriteFile(RegFile,&UA,4,&a,0);WriteFile(RegFile,&UB,4,&a,0);WriteFile(RegFile,&UC,4,&a,0);
//    WriteFile(RegFile,&IA,4,&a,0);WriteFile(RegFile,&IB,4,&a,0);WriteFile(RegFile,&IC,4,&a,0);


    CloseHandle(RegFile);
    TableA->Close();
//    BGrHide->Visible=true;
    BRecord->Visible=false;
    TableA->TableName=TabName;
    TableA->Open();
    DBGrid1->Visible=true;
    DBGrid1->Columns->Items[0]->Width=50;
    for(int i=1;i<DBGrid1->Columns->Count;i++) DBGrid1->Columns->Items[i]->Width=150;
    DBGrid1->Refresh();
    RepaintF();
   Recalc=true;
   Save1->Enabled=true;
}
void __fastcall TFMain::CBLXClick(TObject *Sender)
{
   if(PanS->BevelOuter!=bvLowered)  RepaintF();
   else RepaintS();
}
//-----------------------------------------------------------------------------------
void TFMain::CalculateEngineSpeed(){
   double bd_val=0;
   double mid_t;
   int cnt=0,off1,off2,off=0;
   for(int i=0;i<WDataES->wcount;i++)
    if((short)WDataES->get_w(i)>bd_val) bd_val=(short)WDataES->get_w(i);
   bd_val*=0.8;
   while(off<WDataES->wcount){
     while((short)WDataES->get_w(off)<bd_val)
      if((off+++5)>=WDataES->wcount) break;
     int moff=off,maxv=WDataES->get_w(off);
     for(int i=0;i<20;i++){
       if((short)WDataES->get_w(moff)<(short)WDataES->get_w(off+i)){
         moff=off+i;
       }
       if((off+i+5)>=WDataES->wcount) break;
     }
     if((off+25)>=WDataES->wcount) break;
     off=moff;
     off1=off;
     off+=10;
     while((short)WDataES->get_w(off)>0)
      if((off+++5)>=WDataES->wcount) break;
     if((off+5)>=WDataES->wcount) break;
     while((short)WDataES->get_w(off)<bd_val)
      if((off+++5)>=WDataES->wcount) break;
     if((off+5)>=WDataES->wcount) break;
     moff=off;
     for(int i=0;i<20;i++){
       if((short)WDataES->get_w(moff)<(short)WDataES->get_w(off+i)){
         moff=off+i;
       }
       if((off+i+5)>=WDataES->wcount) break;
     }
     if((off+25)>=WDataES->wcount) break;
     off2=moff;
     off=moff;
     mid_t+=(off2-off1);
     cnt++;
   }
   mid_t/=cnt;
   mid_t=ESKoef*16000/mid_t;
   int t=mid_t;
   NEdit->Text=IntToStr(t);
}
//---------------------------------------------------------------------------
void  TFMain::CutTo2pi(){
   double max=0,min=10000000,mid_val=0;
   double disp=0,disp20=0;
   int off2,off=50;
   while(short(WDataA->get_w(off))>0)if(++off>WDataA->wcount) break;off+=10;
   while(short(WDataA->get_w(off))<0)if(++off>WDataA->wcount) break;off2=off;
   Ua0=0;
   while(short(WDataA->get_w(off2))>0){if(++off2>WDataA->wcount) break;
     Ua0+=short(WDataA->get_w(off2));
   }off2+=10;
   while(short(WDataA->get_w(off2))<0){if(++off2>WDataA->wcount) break;
     Ua0+=short(WDataA->get_w(off2));
   }
   Ua0/=(off2-off);
   memcpy(WDataA->wData,WDataA->wData+off,2*(off2-off));
   WDataA->ccount=2*(off2-off);
   WDataA->wcount=(off2-off);

   off=50;
   while(short(WDataB->get_w(off))>0)if(++off>WDataB->wcount) break;off+=10;
   while(short(WDataB->get_w(off))<0)if(++off>WDataB->wcount) break;off2=off;
   Ub0=0;
   while(short(WDataB->get_w(off2))>0){if(++off2>WDataB->wcount) break;
     Ub0+=short(WDataB->get_w(off2));
   }off2+=10;
   while(short(WDataB->get_w(off2))<0){if(++off2>WDataB->wcount) break;
     Ub0+=short(WDataB->get_w(off2));
   }
   Ub0/=(off2-off);
   memcpy(WDataB->wData,WDataB->wData+off,2*(off2-off));
   WDataB->ccount=2*(off2-off);
   WDataB->wcount=(off2-off);

   off=50;
   while(short(WDataC->get_w(off))>0)if(++off>WDataC->wcount) break;off+=10;
   while(short(WDataC->get_w(off))<0)if(++off>WDataC->wcount) break;off2=off;
   Uc0=0;
   while(short(WDataC->get_w(off2))>0){if(++off2>WDataC->wcount) break;
     Uc0+=short(WDataC->get_w(off2));
   }off2+=10;
   while(short(WDataC->get_w(off2))<0){if(++off2>WDataC->wcount) break;
     Uc0+=short(WDataC->get_w(off2));
   }
   Uc0/=(off2-off);
   memcpy(WDataC->wData,WDataC->wData+off,2*(off2-off));
   WDataC->ccount=2*(off2-off);
   WDataC->wcount=(off2-off);

off=50;
   while(short(WDataAi->get_w(off))>0)if(++off>WDataAi->wcount) break;off+=10;
   while(short(WDataAi->get_w(off))<0)if(++off>WDataAi->wcount) break;off2=off;
   Ia0=0;
   while(short(WDataAi->get_w(off2))>0){if(++off2>WDataAi->wcount) break;
     Ia0+=short(WDataAi->get_w(off2));
   }off2+=10;
   while(short(WDataAi->get_w(off2))<0){if(++off2>WDataAi->wcount) break;
     Ia0+=short(WDataAi->get_w(off2));
   }
   Ia0/=(off2-off);
   memcpy(WDataAi->wData,WDataAi->wData+off,2*(off2-off));
   WDataAi->ccount=2*(off2-off);
   WDataAi->wcount=(off2-off);

   off=50;
   while(short(WDataBi->get_w(off))>0)if(++off>WDataBi->wcount) break;off+=10;
   while(short(WDataBi->get_w(off))<0)if(++off>WDataBi->wcount) break;off2=off;
   Ib0=0;
   while(short(WDataBi->get_w(off2))>0){if(++off2>WDataBi->wcount) break;
     Ib0+=short(WDataBi->get_w(off2));
   }off2+=10;
   while(short(WDataBi->get_w(off2))<0){if(++off2>WDataBi->wcount) break;
     Ib0+=short(WDataBi->get_w(off2));
   }
   Ib0/=(off2-off);
   memcpy(WDataBi->wData,WDataBi->wData+off,2*(off2-off));
   WDataBi->ccount=2*(off2-off);
   WDataBi->wcount=(off2-off);

   off=50;
   while(short(WDataCi->get_w(off))>0)if(++off>WDataCi->wcount) break;off+=10;
   while(short(WDataCi->get_w(off))<0)if(++off>WDataCi->wcount) break;off2=off;
   Ic0=0;
   while(short(WDataCi->get_w(off2))>0){if(++off2>WDataCi->wcount) break;
     Ic0+=short(WDataCi->get_w(off2));
   }off2+=10;
   while(short(WDataCi->get_w(off2))<0){if(++off2>WDataCi->wcount) break;
     Ic0+=short(WDataCi->get_w(off2));
   }
   Ic0/=(off2-off);
   memcpy(WDataCi->wData,WDataCi->wData+off,2*(off2-off));
   WDataCi->ccount=2*(off2-off);
   WDataCi->wcount=(off2-off);

   //------------AB-BC-CA
   off=50;
   while(short(WDataAB->get_w(off))>0)if(++off>WDataAB->wcount) break;off+=10;
   while(short(WDataAB->get_w(off))<0)if(++off>WDataAB->wcount) break;off2=off;
   Uab0=0;
   while(short(WDataAB->get_w(off2))>0){if(++off2>WDataAB->wcount) break;
     Uab0+=short(WDataAB->get_w(off2));
   }off2+=10;
   while(short(WDataAB->get_w(off2))<0){if(++off2>WDataAB->wcount) break;
     Uab0+=short(WDataAB->get_w(off2));
   }
   Uab0/=(off2-off);
   memcpy(WDataAB->wData,WDataAB->wData+off,2*(off2-off));
   WDataAB->ccount=2*(off2-off);
   WDataAB->wcount=(off2-off);

   off=50;
   while(short(WDataBC->get_w(off))>0)if(++off>WDataBC->wcount) break;off+=10;
   while(short(WDataBC->get_w(off))<0)if(++off>WDataBC->wcount) break;off2=off;
   Ubc0=0;
   while(short(WDataBC->get_w(off2))>0){if(++off2>WDataBC->wcount) break;
     Ubc0+=short(WDataBC->get_w(off2));
   }off2+=10;
   while(short(WDataBC->get_w(off2))<0){if(++off2>WDataBC->wcount) break;
     Ubc0+=short(WDataBC->get_w(off2));
   }
   Ubc0/=(off2-off);
   memcpy(WDataBC->wData,WDataBC->wData+off,2*(off2-off));
   WDataBC->ccount=2*(off2-off);
   WDataBC->wcount=(off2-off);

   off=50;
   while(short(WDataCA->get_w(off))>0)if(++off>WDataCA->wcount) break;off+=10;
   while(short(WDataCA->get_w(off))<0)if(++off>WDataCA->wcount) break;off2=off;
   Uca0=0;
   while(short(WDataCA->get_w(off2))>0){if(++off2>WDataCA->wcount) break;
     Uca0+=short(WDataCA->get_w(off2));
   }off2+=10;
   while(short(WDataCA->get_w(off2))<0){if(++off2>WDataCA->wcount) break;
     Uca0+=short(WDataCA->get_w(off2));
   }
   Uca0/=(off2-off);
   memcpy(WDataCA->wData,WDataCA->wData+off,2*(off2-off));
   WDataCA->ccount=2*(off2-off);
   WDataCA->wcount=(off2-off);

}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TFMain::CBLYClick(TObject *Sender){
   if(PanS->BevelOuter!=bvLowered)  RepaintF();
   else RepaintS();
}
//---------------------------------------------------------------------------
void __fastcall TFMain::ImFAClick(TObject *Sender){RepaintF();}
//---------------------------------------------------------------------------
void __fastcall TFMain::FormResize(TObject *Sender){RepaintF();RepaintS();}
//---------------------------------------------------------------------------


void __fastcall TFMain::PanSMouseDown(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
 int _tmp;
  TPanel* pp=(TPanel*)Sender;
  if(pp==PanFA){
     _tmp=A_state;
     if(Shift.Contains(ssShift)){
        _tmp=A_state%10;
        if(!_tmp){IiA->Visible=!IiA->Visible; A_state+=1; }
        else{
          if(_tmp==1){ IiA->Visible=false;IitA->Visible=true;A_state+=1;}
          if(_tmp==2){ IiA->Visible=false;IitA->Visible=false;A_state-=2;}
        }
        RepaintF();
        return;
     }
     if(Shift.Contains(ssCtrl)) {
        _tmp=A_state/10;
        if(!_tmp){IgA->Visible=!IgA->Visible; A_state+=10; }
        else{
          if(_tmp==1){ IgA->Visible=false;IgtA->Visible=true;A_state+=10;}
          if(_tmp==2){ IgA->Visible=false;IgtA->Visible=false;A_state-=20;}
        }
        RepaintF();
        return;
     }
  }
  if(pp==PanFB){
     if(Shift.Contains(ssShift)) {
        _tmp=B_state%10;
        if(!_tmp){IiB->Visible=!IiB->Visible; B_state+=1; }
        else{
          if(_tmp==1){ IiB->Visible=false;IitB->Visible=true;B_state+=1;}
          if(_tmp==2){ IiB->Visible=false;IitB->Visible=false;B_state-=2;}
        }
        RepaintF();
        return;
     }
     if(Shift.Contains(ssCtrl)) {
        _tmp=B_state/10;
        if(!_tmp){IgB->Visible=!IgB->Visible; B_state+=10; }
        else{
          if(_tmp==1){ IgB->Visible=false;IgtB->Visible=true;B_state+=10;}
          if(_tmp==2){ IgB->Visible=false;IgtB->Visible=false;B_state-=20;}
        }
        RepaintF();
        return;
     }
  }
  if(pp==PanFC){
        _tmp=C_state%10;
     if(Shift.Contains(ssShift)) {
        if(!_tmp){IiC->Visible=!IiC->Visible; C_state+=1; }
        else{
          if(_tmp==1){ IiC->Visible=false;IitC->Visible=true;C_state+=1;}
          if(_tmp==2){ IiC->Visible=false;IitC->Visible=false;C_state-=2;}
        }
        RepaintF();
        return;
     }
     if(Shift.Contains(ssCtrl)) {
        _tmp=C_state/10;
        if(!_tmp){IgC->Visible=!IgC->Visible; C_state+=10; }
        else{
          if(_tmp==1){ IgC->Visible=false;IgtC->Visible=true;C_state+=10;}
          if(_tmp==2){ IgC->Visible=false;IgtC->Visible=false;C_state-=20;}
        }
        RepaintF();
        return;
     }
  }
//----AB-BC-CA
  if(pp==PanAB){
        _tmp=C_state/10;
     if(Shift.Contains(ssCtrl)) {
        if(!_tmp){IgAB->Visible=!IgAB->Visible; C_state+=10; }
        else{
          if(_tmp==1){ IgAB->Visible=false;IgtAB->Visible=true;C_state+=10;}
          if(_tmp==2){ IgAB->Visible=false;IgtAB->Visible=false;C_state-=20;}
        }
        RepaintF();
        return;
     }
  }
  if(pp==PanBC){
        _tmp=C_state/10;
     if(Shift.Contains(ssCtrl)) {
        if(!_tmp){IgBC->Visible=!IgBC->Visible; C_state+=10; }
        else{
          if(_tmp==1){ IgBC->Visible=false;IgtBC->Visible=true;C_state+=10;}
          if(_tmp==2){ IgBC->Visible=false;IgtBC->Visible=false;C_state-=20;}
        }
        RepaintF();
        return;
     }
  }
  if(pp==PanCA){
        _tmp=C_state/10;
     if(Shift.Contains(ssCtrl)) {
        if(!_tmp){IgCA->Visible=!IgCA->Visible; C_state+=10; }
        else{
          if(_tmp==1){ IgCA->Visible=false;IgtCA->Visible=true;C_state+=10;}
          if(_tmp==2){ IgCA->Visible=false;IgtCA->Visible=false;C_state-=20;}
        }
        RepaintF();
        return;
     }
  }
  if(pp->BevelOuter==bvRaised)pp->BevelOuter=bvLowered;
  else pp->BevelOuter=bvRaised;

//  if(pp==PanS)
  if(PanS->BevelOuter==bvLowered){
//     PanFA->BevelOuter=bvRaised;
//     PanFB->BevelOuter=bvRaised;
//     PanFC->BevelOuter=bvRaised;
     RepaintS(); return;
  }
  RepaintF();

}
//---------------------------------------------------------------------------

void __fastcall TFMain::IitAMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  TImage* pp=(TImage*)Sender;
  PanSMouseDown(pp->Parent,Button,Shift,X,Y);
}
//---------------------------------------------------------------------------

void __fastcall TFMain::DBGrid1CellClick(TColumn *Column)
{
  if(Column->Index==0){
     OfftU=DBGrid1->SelectedField->AsInteger;
     RepaintS();
  }

}
//---------------------------------------------------------------------------
void __fastcall TFMain::TimerToReadTimer(TObject *Sender)
{
  BRecordClick(Sender);
  if(cntOP<10)  BRecordClick(Sender);
}
//---------------------------------------------------------------------------


void __fastcall TFMain::SpeedButton9Click(TObject *Sender)
{
  offI+=10;
  if(offI>=0)
       Lioff->Caption="I+"+IntToStr(offI);
  else
       Lioff->Caption="I"+IntToStr(offI);
  RepaintF();
}
//---------------------------------------------------------------------------

void __fastcall TFMain::SpeedButton10Click(TObject *Sender)
{
  offI-=10;
  if(offI>=0)
       Lioff->Caption="I+"+IntToStr(offI);
  else
       Lioff->Caption="I"+IntToStr(offI);
  RepaintF();
}
//---------------------------------------------------------------------------

void __fastcall TFMain::SpeedButton11Click(TObject *Sender)
{
  if(PanS->BevelOuter==bvLowered){
   OfftU--;if(OfftU<0)OfftU=0;
   LOfftU->Caption=IntToStr(OfftU);
   RepaintS();
   return;
  }
  OfftI-=10;if(OfftI<0)OfftI=0;
  LOfftI->Caption=IntToStr(OfftI);
  RepaintF();
}
//---------------------------------------------------------------------------

void __fastcall TFMain::SpeedButton12Click(TObject *Sender)
{
  if(PanS->BevelOuter==bvLowered){
   OfftU++;if(OfftU>_nFGar)OfftU=_nFGar;
   LOfftU->Caption=IntToStr(OfftU);
   RepaintS();
   return;
  }

  OfftI+=10;
  LOfftI->Caption=IntToStr(OfftI);
  RepaintF();
}
//---------------------------------------------------------------------------

void __fastcall TFMain::Open1Click(TObject *Sender)
{
  if(DBView->Deleted){
     AnsiString filename=DBView->ListBox1->Items->Strings[DBView->ListBox1->ItemIndex];
     filename.Delete(1,filename.AnsiPos("file ")+4);
     TableA->Close();
     Query1->SQL->Clear();
     Query1->SQL->Add("DELETE FROM List WHERE Name ='"+ filename+"'");
     try{
        Query1->ExecSQL();
     }
     catch(...){
        MessageBox(this->Handle,"�������� �������� ���-� �������������� ������","DB Error",MB_ICONERROR);
     }
     Query1->SQL->Clear();
     Query1->SQL->Add("DROP TABLE "+ filename);
     try{
        Query1->ExecSQL();
     }
     catch(...){
        MessageBox(this->Handle,"��� ���� ������ ������ ��������� �� ����","DB Warning",MB_ICONEXCLAMATION);
     }
     Query1->SQL->Clear();
     Query1->SQL->Add("DROP TABLE "+ filename+"B");
     try{
        Query1->ExecSQL();
     }
     catch(...){
        MessageBox(this->Handle,"��� ���� ������ ������ ��������� �� ����","DB Warning",MB_ICONEXCLAMATION);
     }

     filename="base\\"+filename+".rec";
     if(!DeleteFile(filename.c_str())){
       filename="�� ���������� ������� ���� "+filename+" ���������� ������� ��� �������";
       MessageBox(this->Handle,filename.c_str(),"Error",MB_ICONERROR);
     }


     DBView->Deleted=false;
  }
  if(DBView->Rename){
     AnsiString filename=DBView->ListBox1->Items->Strings[DBView->ListBox1->ItemIndex];
     filename.Delete(1,filename.AnsiPos("file ")+4);
     FAskName->Label1->Caption="������� ����� ��� ��� ������� \""+filename+"\"";
     FAskName->ENewName->Text=TabName;
     FAskName->ENewName->EnableAlign();
     FAskName->ShowModal();
     TabName=FAskName->ENewName->Text;
     if(TabName=="List"){
       MessageBox(this->Handle,"��� ��� ������� ������. �� �� ������ ��� ������������.\n\
                                ������� ������ ���.","Warning",MB_ICONEXCLAMATION);
       Open1Click(NULL);    // @recursion
       return;
     }
     TableA->Close();
     TableA->TableName="List";
     TableA->Open();
     TableA->First();
     DBView->ListBox1->Clear();
     while(!TableA->Eof){
       if(TableA->FieldByName("Name")->AsString==filename) break;
       TableA->Next();
     }

     if(!TableA->Eof){
       Query1->SQL->Clear();
       Query1->SQL->Add("DELETE FROM List WHERE Name ='"+ filename+"'");
       try{
           Query1->ExecSQL();
       }
       catch(...){
          MessageBox(this->Handle,"������� ���","DB Warning",MB_ICONEXCLAMATION);
       }

       CreateNewTable(TabName);
       Query1->SQL->Clear();
       Query1->SQL->Add("INSERT INTO "+TabName+" SELECT * FROM "+filename);
       Query1->ExecSQL();
       Query1->SQL->Clear();
       Query1->SQL->Add("INSERT INTO "+TabName+"B SELECT * FROM "+filename+"B");
       Query1->ExecSQL();
       Query1->SQL->Clear();
       Query1->SQL->Add("DROP TABLE "+filename);
       Query1->ExecSQL();
       Query1->SQL->Clear();
       Query1->SQL->Add("DROP TABLE "+filename+"B");
       Query1->ExecSQL();

       if(!RenameFile("base\\"+filename+".rec","base\\"+TabName+".rec")){
          filename="����������� ���� "+filename+".rec"+"\n ������������� ������� �������� ������.";
          MessageBox(this->Handle,filename.c_str(),"Warning",MB_ICONEXCLAMATION);
       }
     }//!Eof
     else{
         MessageBox(this->Handle,"������ ���� ������.������ �� �������. \n \
                                  ���������� ��� ��������� �������","Warning",MB_ICONEXCLAMATION);
         DBView->Rename=false;
         ShellExecute(NULL,"open","msaccess C:\Work2\Sound2_\Base\Enrgy.mdb",NULL,NULL,SW_SHOWNORMAL);
         MessageBox(this->Handle,"�������� ������ �� ��������� ��-�� ������","Warning",MB_ICONEXCLAMATION);

         return;
     }

     DBView->Rename=false;
     Open1Click(NULL);
  }
  TableA->Close();
  TableA->TableName="List";
  TableA->Open();
  TableA->First();
  DBView->ListBox1->Clear();
  while(!TableA->Eof){
    DBView->ListBox1->Items->Add(TableA->FieldByName("Date")->AsString+"     file "+TableA->FieldByName("Name")->AsString);
    TableA->Next();
  }
  DBView->ShowModal();
  if(DBView->Deleted) {Open1Click(NULL); return;}       // @recursion
  if(DBView->Rename)  {Open1Click(NULL); return;}       // @recursion
  if (DBView->ListBox1->ItemIndex<0) return;
  AnsiString filename=DBView->ListBox1->Items->Strings[DBView->ListBox1->ItemIndex];
  filename.Delete(1,filename.AnsiPos("file ")+4);
  TableA->Close();
  TableA->TableName=filename;
  TableA->Open();
  TFloatField *f = (TFloatField*) TableA->FieldByName("UBi");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("UAi");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("UCi");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("IAi");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("IBi");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("ICi");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("PsiA");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("PsiB");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("PsiC");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("PsiAi");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("PsiBi");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("PsiCi");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("UAB");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("UBC");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("UCA");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("PsiAB");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("PsiBC");
  f->Precision = 4;
  f = (TFloatField*) TableA->FieldByName("PsiCa");
  f->Precision = 4;
  TableB->Close();
  TableB->TableName=filename+"B";
  TableB->Open();
  

  DWORD a;
    filename="base\\"+filename+".rec";
    HANDLE RegFile=CreateFile(filename.c_str(),GENERIC_READ,0,0,OPEN_ALWAYS,0,0);
    ReadFile(RegFile,&WDataA->wcount,2,&a,0);WDataA->ccount=WDataA->wcount*2;
         ReadFile(RegFile,WDataA->cData,WDataA->ccount,&a,0);
    ReadFile(RegFile,&WDataB->wcount,2,&a,0);WDataB->ccount=WDataB->wcount*2;
         ReadFile(RegFile,WDataB->cData,WDataB->ccount,&a,0);
    ReadFile(RegFile,&WDataC->wcount,2,&a,0);WDataC->ccount=WDataC->wcount*2;
         ReadFile(RegFile,WDataC->cData,WDataC->ccount,&a,0);
    ReadFile(RegFile,&WDataAi->wcount,2,&a,0);WDataAi->ccount=WDataAi->wcount*2;
         ReadFile(RegFile,WDataAi->cData,WDataAi->ccount,&a,0);
    ReadFile(RegFile,&WDataBi->wcount,2,&a,0);WDataBi->ccount=WDataBi->wcount*2;
         ReadFile(RegFile,WDataBi->cData,WDataBi->ccount,&a,0);
    ReadFile(RegFile,&WDataCi->wcount,2,&a,0);WDataCi->ccount=WDataCi->wcount*2;
         ReadFile(RegFile,WDataCi->cData,WDataCi->ccount,&a,0);
    ReadFile(RegFile,&WDataAG->wcount,2,&a,0);WDataAG->ccount=WDataAG->wcount*2;
         ReadFile(RegFile,WDataAG->cData,WDataAG->ccount,&a,0);
    ReadFile(RegFile,&WDataBG->wcount,2,&a,0);WDataBG->ccount=WDataBG->wcount*2;
         ReadFile(RegFile,WDataBG->cData,WDataBG->ccount,&a,0);
    ReadFile(RegFile,&WDataCG->wcount,2,&a,0);WDataCG->ccount=WDataCG->wcount*2;
         ReadFile(RegFile,WDataCG->cData,WDataCG->ccount,&a,0);
    ReadFile(RegFile,&WDataAiG->wcount,2,&a,0);WDataAiG->ccount=WDataAiG->wcount*2;
         ReadFile(RegFile,WDataAiG->cData,WDataAiG->ccount,&a,0);
    ReadFile(RegFile,&WDataBiG->wcount,2,&a,0);WDataBiG->ccount=WDataBiG->wcount*2;
         ReadFile(RegFile,WDataBiG->cData,WDataBiG->ccount,&a,0);
    ReadFile(RegFile,&WDataCiG->wcount,2,&a,0);WDataCiG->ccount=WDataCiG->wcount*2;
         ReadFile(RegFile,WDataCiG->cData,WDataCiG->ccount,&a,0);

    ReadFile(RegFile,&WDataAB->wcount,2,&a,0);WDataAB->ccount=WDataAB->wcount*2;
         ReadFile(RegFile,WDataAB->cData,WDataAB->ccount,&a,0);
    ReadFile(RegFile,&WDataBC->wcount,2,&a,0);WDataBC->ccount=WDataBC->wcount*2;
         ReadFile(RegFile,WDataBC->cData,WDataBC->ccount,&a,0);
    ReadFile(RegFile,&WDataCA->wcount,2,&a,0);WDataCA->ccount=WDataCA->wcount*2;
         ReadFile(RegFile,WDataCA->cData,WDataCA->ccount,&a,0);

    ReadFile(RegFile,&WDataABG->wcount,2,&a,0);WDataABG->ccount=WDataABG->wcount*2;
         ReadFile(RegFile,WDataABG->cData,WDataABG->ccount,&a,0);
    ReadFile(RegFile,&WDataBCG->wcount,2,&a,0);WDataBCG->ccount=WDataBCG->wcount*2;
         ReadFile(RegFile,WDataBCG->cData,WDataBCG->ccount,&a,0);
    ReadFile(RegFile,&WDataCAG->wcount,2,&a,0);WDataCAG->ccount=WDataCAG->wcount*2;
         ReadFile(RegFile,WDataCAG->cData,WDataCAG->ccount,&a,0);

    ReadFile(RegFile,&lPA,sizeof(double),&a,0);    ReadFile(RegFile,&lPB,sizeof(double),&a,0);
    ReadFile(RegFile,&lPC,sizeof(double),&a,0);    ReadFile(RegFile,&ldP,sizeof(double),&a,0);
    ReadFile(RegFile,&lP,sizeof(double),&a,0);     ReadFile(RegFile,&lP0,sizeof(double),&a,0);
    ReadFile(RegFile,&lM,sizeof(double),&a,0);     ReadFile(RegFile,&lNd,sizeof(double),&a,0);
    ReadFile(RegFile,&lsU,sizeof(double),&a,0);    ReadFile(RegFile,&lU1,sizeof(double),&a,0);
    ReadFile(RegFile,&lU2,sizeof(double),&a,0);    ReadFile(RegFile,&lU0,sizeof(double),&a,0);
    ReadFile(RegFile,&lK2U,sizeof(double),&a,0);   ReadFile(RegFile,&lK0U,sizeof(double),&a,0);
    ReadFile(RegFile,&lKUA,sizeof(double),&a,0);   ReadFile(RegFile,&lKUB,sizeof(double),&a,0);
    ReadFile(RegFile,&lKUC,sizeof(double),&a,0);

    ReadFile(RegFile,&UA,sizeof(double),&a,0);    ReadFile(RegFile,&UB,sizeof(double),&a,0);
    ReadFile(RegFile,&UC,sizeof(double),&a,0);

    ReadFile(RegFile,&IA,sizeof(double),&a,0);    ReadFile(RegFile,&IB,sizeof(double),&a,0);
    ReadFile(RegFile,&IC,sizeof(double),&a,0);

    ReadFile(RegFile,&UAB,sizeof(double),&a,0);    ReadFile(RegFile,&UBC,sizeof(double),&a,0);
    ReadFile(RegFile,&UCA,sizeof(double),&a,0);


    ParForm->PA=lPA;     ParForm->PB=lPB;    ParForm->PC=lPC;
    ParForm->P=lP;       ParForm->dP=ldP;    ParForm->P0=lP0;
    ParForm->PA=lPA;     ParForm->Md=lM;     ParForm->Nd=lNd;
    ParForm->sU=lsU;     ParForm->U1=lU1;    ParForm->U2=lU2;
    ParForm->U0=lU0;     ParForm->K2U=lK2U;  ParForm->K0U=lK0U;
    ParForm->KUA=lKUA;   ParForm->KUB=lKUB;  ParForm->KUC=lKUC;
    ParItm->Enabled=true; GarmItm->Enabled=true;
    ParForm->Visible=true;
    ParForm->BringToFront();
    GarmForm->Visible=true;
    GarmForm->BringToFront();
    GarmForm->DBGrid1->DataSource=DataSource2;
    GarmForm->DBGrid1->Refresh();
    AnsiString tmpS;
    LUA->Caption="UA="+tmpS.sprintf("%.2E", UA);
    LUB->Caption="UB="+tmpS.sprintf("%.2E", UB);
    LUC->Caption="UC="+tmpS.sprintf("%.2E", UC);
    LUAB->Caption="UAB="+tmpS.sprintf("%.2E", UAB);
    LUBC->Caption="UBC="+tmpS.sprintf("%.2E", UBC);
    LUCA->Caption="UCA="+tmpS.sprintf("%.2E", UCA);
    LIA->Caption="IA="+tmpS.sprintf("%.2E", IA);
    LIB->Caption="IB="+tmpS.sprintf("%.2E", IB);
    LIC->Caption="IC="+tmpS.sprintf("%.2E", IC);

//      @@
//    ReadFile(RegFile,&UA,4,&a,0);ReadFile(RegFile,&UB,4,&a,0);ReadFile(RegFile,&UC,4,&a,0);
//    ReadFile(RegFile,&IA,4,&a,0);ReadFile(RegFile,&IB,4,&a,0);ReadFile(RegFile,&IC,4,&a,0);

    CloseHandle(RegFile);
    RepaintF();

   if(!DBGrid1->Visible)Panel4->Height+=200;
   DBGrid1->Visible=true;
   BGrHide->Visible=true;
   DBGrid1->Fields[2]->DisplayWidth=5;
   if(PanS->BevelOuter==bvLowered) RepaintS();  Calculated=true;
   BCalc->Caption="��������";
   BCalc->Enabled=true;

   Save1->Enabled=true;
}
//---------------------------------------------------------------------------


void __fastcall TFMain::About1Click(TObject *Sender)
{
  char c=0xA9;
  AnsiString C=" ",S;C[1]=c;
  S="Energetic v3.2\n"+C+" 2008 DNT";
  MessageBox(this->Handle,S.c_str(),"",MB_ICONINFORMATION);
}
//---------------------------------------------------------------------------

void __fastcall TFMain::Save1Click(TObject *Sender)
{
  BCalcClick(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFMain::PanSClick(TObject *Sender)
{
  if(PanS->BevelOuter==bvLowered){
     SpeedButton11->Visible=false;
     SpeedButton12->Visible=false;
  }
  else{
     SpeedButton11->Visible=true;
     SpeedButton12->Visible=true;
  }
}
//---------------------------------------------------------------------------



void __fastcall TFMain::Options1Click(TObject *Sender)
{
  if(ParForm->Visible) ParItm->Checked=true;
  else ParItm->Checked=false;

  if(GarmForm->Visible) GarmItm->Checked=true;
  else GarmItm->Checked=false;

}
//---------------------------------------------------------------------------

void __fastcall TFMain::N1Click(TObject *Sender)
{
  N1->Checked=!N1->Checked;
       AxisSh->Brush->Color=AxisCol;
       NetSh->Brush->Color=GridCol;
       OXSh->Brush->Color=LablICol;
       OYSh->Brush->Color=LablUCol;
       FonSh->Brush->Color=BkCol;
       AxisSh->Brush->Color=SCol;
       UASh->Brush->Color=ACol;
       UBSh->Brush->Color=BCol;
       UCSh->Brush->Color=CCol;
       TimeSh->Brush->Color=TimeCol;
       IASh->Brush->Color=AICol;
       IBSh->Brush->Color=BICol;
       ICSh->Brush->Color=CICol;
       UAgSh->Brush->Color=AgCol;
       FonSh->Brush->Color=BgCol;
       UCgSh->Brush->Color=CgCol;
       IAgSh->Brush->Color=AIgCol;
       IBgSh->Brush->Color=BIgCol;
       ICgSh->Brush->Color=CIgCol;
       UABSh->Brush->Color=ABCol;
       UBCSh->Brush->Color=BCCol;
       UCASh->Brush->Color=CACol;
       UABgSh->Brush->Color=ABgCol;
       UBCgSh->Brush->Color=BCgCol;
       UCAgSh->Brush->Color=CAgCol;
       
  if(N1->Checked)
      ColorPan->Visible=true;
  else
      ColorPan->Visible=false;
}
//---------------------------------------------------------------------------


void __fastcall TFMain::ParItmClick(TObject *Sender)
{
  ParForm->Visible=true;
  ParForm->BringToFront();
  ParItm->Checked=true;

}
//---------------------------------------------------------------------------

void __fastcall TFMain::GarmItmClick(TObject *Sender)
{
  GarmForm->Visible=true;
  GarmForm->BringToFront();
  GarmItm->Checked=true;
}
//---------------------------------------------------------------------------
void TFMain::ResetColor(TForm* _f)
{
}
void TFMain::ResetKoef()
{
}

void __fastcall TFMain::N3Click(TObject *Sender)
{
  N3->Checked=!N3->Checked;
    EESKoef->Text=FloatToStr(ESKoef);
    Eksc->Text=FloatToStr(ksc);
    EGarm->Text=IntToStr(_nFGar);
    EEth->Text=FloatToStr(Eth_g);
    EKUa->Text=FloatToStr(KUa);
    EKUb->Text=FloatToStr(KUb);
    EKUc->Text=FloatToStr(KUc);
    EKIa->Text=FloatToStr(KIa);
    EKIb->Text=FloatToStr(KIb);
    EKIc->Text=FloatToStr(KIc);
    EKUab->Text=FloatToStr(KUab);
    EKUbc->Text=FloatToStr(KUbc);
    EKUca->Text=FloatToStr(KUca);
  if(N3->Checked)
    KoefPan->Visible=true;
  else
    KoefPan->Visible=false;
}
//---------------------------------------------------------------------------

void __fastcall TFMain::UAShMouseDown(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
    TShape* ptr=(TShape*)Sender;
    ColorDialog1->Execute();
    if(ptr==UASh)UASh->Brush->Color=ColorDialog1->Color;
    if(ptr==UBSh)UBSh->Brush->Color=ColorDialog1->Color;
    if(ptr==UCSh)UCSh->Brush->Color=ColorDialog1->Color;
    if(ptr==UAgSh)UAgSh->Brush->Color=ColorDialog1->Color;
    if(ptr==UBgSh)UBgSh->Brush->Color=ColorDialog1->Color;
    if(ptr==UCgSh)UCgSh->Brush->Color=ColorDialog1->Color;

    if(ptr==IASh)IASh->Brush->Color=ColorDialog1->Color;
    if(ptr==IBSh)IBSh->Brush->Color=ColorDialog1->Color;
    if(ptr==ICSh)ICSh->Brush->Color=ColorDialog1->Color;
    if(ptr==IAgSh)IAgSh->Brush->Color=ColorDialog1->Color;
    if(ptr==IBgSh)IBgSh->Brush->Color=ColorDialog1->Color;
    if(ptr==ICgSh)ICgSh->Brush->Color=ColorDialog1->Color;

    if(ptr==UABSh)UABSh->Brush->Color=ColorDialog1->Color;
    if(ptr==UBCSh)UBCSh->Brush->Color=ColorDialog1->Color;
    if(ptr==UCASh)UCASh->Brush->Color=ColorDialog1->Color;
    if(ptr==UABgSh)UABgSh->Brush->Color=ColorDialog1->Color;
    if(ptr==UBCgSh)UBCgSh->Brush->Color=ColorDialog1->Color;
    if(ptr==UCAgSh)UCAgSh->Brush->Color=ColorDialog1->Color;

    if(ptr==FonSh)FonSh->Brush->Color=ColorDialog1->Color;
    if(ptr==SpSh)SpSh->Brush->Color=ColorDialog1->Color;
    if(ptr==AxisSh)AxisSh->Brush->Color=ColorDialog1->Color;
    if(ptr==OXSh)OXSh->Brush->Color=ColorDialog1->Color;
    if(ptr==OYSh)OYSh->Brush->Color=ColorDialog1->Color;
    if(ptr==NetSh)NetSh->Brush->Color=ColorDialog1->Color;
    if(ptr==TimeSh)TimeSh->Brush->Color=ColorDialog1->Color;

AxisCol=       AxisSh->Brush->Color;
GridCol=       NetSh->Brush->Color;
LablICol=       OXSh->Brush->Color;
LablUCol=       OYSh->Brush->Color;
BkCol=       FonSh->Brush->Color;
SCol=       AxisSh->Brush->Color;
ACol=       UASh->Brush->Color;
BCol=       UBSh->Brush->Color;
CCol=       UCSh->Brush->Color;
TimeCol=       TimeSh->Brush->Color;
AICol=       IASh->Brush->Color;
BICol=       IBSh->Brush->Color;
CICol=       ICSh->Brush->Color;
AgCol=       UAgSh->Brush->Color;
BgCol=       FonSh->Brush->Color;
CgCol=       UCgSh->Brush->Color;
AIgCol=       IAgSh->Brush->Color;
BIgCol=       IBgSh->Brush->Color;
CIgCol=       ICgSh->Brush->Color;
ABCol=       UABSh->Brush->Color;
BCCol=       UBCSh->Brush->Color;
CACol       =UCASh->Brush->Color;
ABgCol=       UABgSh->Brush->Color;
BCgCol=       UBCgSh->Brush->Color;
CAgCol=       UCAgSh->Brush->Color;
       RepaintF();
       RepaintS();

}
//---------------------------------------------------------------------------

void __fastcall TFMain::EkscChange(TObject *Sender)
{
if(Initializing) return;
ESKoef=EESKoef->Text.ToDouble();
 ksc=   Eksc->Text.ToDouble();
_nFGar=    EGarm->Text.ToInt();
Eth_g=    EEth->Text.ToDouble();
KUa=  EKUa->Text.ToDouble();
KUb=  EKUb->Text.ToDouble();
KUc=  EKUc->Text.ToDouble();
KIa=  EKIa->Text.ToDouble();
KIb=  EKIb->Text.ToDouble();
KIc=  EKIc->Text.ToDouble();
KUab=  EKUab->Text.ToDouble();
KUbc=  EKUbc->Text.ToDouble();
KUca=  EKUca->Text.ToDouble();
}
//---------------------------------------------------------------------------




