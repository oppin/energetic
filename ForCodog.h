//---------------------------------------------------------------------------

#ifndef ForCodogH
#define ForCodogH

#pragma pack(push)
#pragma pack(1)

// ������ ���� �������� �������� ��������� ������� ������������ ������ ����� ��02
//  ���� ����� ������ �� ��� ������� ������������ ����������, �� ����
// ������� ��� ���� ����� �����������.

#define MAX_CUCODOGR_LEN  256
#define MAX_HEADER_LEN  10

// ���������� ���� ���������
#define CU02_TYPE_AM                   0x01  // ������������ �����
#define CU02_TYPE_PRL_MARK             0x02  // ��������� ������� - ������������
#define CU02_TYPE_VRL_MARK             0x03  // ��������� ������� - �����������
#define CU02_TYPE_PELENG               0x04  // ������
#define CU02_TYPE_STROB                0x05  // ������
#define CU02_TYPE_COMMAND              0x06  // ������� ���������
#define CU02_TYPE_StateNRZ             0x07  //��������� ���
#define CU02_TYPE_TRACE                0x10  // ������
#define CU02_TYPE_TRACE_ON_PELENG      0x11  // ������ �� �������

// ���������� ������ ����� ��������� (� �������� ��� ������ ��������� � sizeof(�����. ��� ������) )
#define CU02_LEN_AM                    26  // ����� ���������� "������������ �����" - 8+18
#define CU02_LEN_PRL_MARK              32  // ����� ���������� "��������� �������" - 8+24
#define CU02_LEN_VRL_MARK              26  // ����� ���������� "��������� �������" - 8+18
#define CU02_LEN_PELENG                18  // ����� ���������� "������" - 8+10
// ����� �������������� ����� ����������   "������" ���������� �� 4 �� 1086 ����
#define CU02_LEN_STROB               1094  // ������   8+1086
// ����� �������������� ����� ���������  "������� ���������" ���������� �� 8 �� 18 ����
#define CU02_LEN_COMMAND              26  // ������� ��������� 8+18
#define CU02_LEN_StateNRZ             14   // ������� ��������� 8+6
// ����� �������������� ����� ��������� "������"  ���������� 36 ��� 44 �����.
#define CU02_LEN_TRACE                 52  // ����� ���������� "������" 8+44
#define CU02_LEN_TRACE_ON_PELENG       24  // ����� ���������� "������ �� �������" 8+16

#ifdef _WIN32   // ��� �����  ----------------------
typedef    __int64 LTime;
#else        // ��� QNX     ----------------------
typedef    unsigned long long LTime;
#endif              //     ----------------------

//     ���������
struct CodogrHeader{
      unsigned short HeaderSI:16;  // ����������� �������� :��� ������� "S" � ��� ������� "I"
      unsigned short Header1K:16;  // ����������� �������� :��� ��������� = 01h � ��� ������� "K"
      unsigned short Type79K6:16;    // ��� ������� 79�6 :01 h � 03 h
      unsigned short Type:16;    // ��� ����������
      unsigned short Len:16;     // ����� �������������� ����� ��������� � ������ - �������� ���
};
// ----------
// ������������ �����, ��� - 0x01, ����� - 8 + 18 ����
struct SAm{

// WORD 1...5
   CodogrHeader  Header;

// WORD 5+1
   unsigned short Azimuth  :14;    // ������ ������� (LSB 180/8192)
   unsigned short          :2;       //
// WORD 5+2
   unsigned short KNCh     :3; // ��� ������� ������� (000 - F1,... 111 - F8)
   unsigned short Litera   :1; // ������: 0 - ������ 1, 1 - ������ 2
   unsigned short TurnRate :1; // �������� �������� �������: 0 - 6 ��/���, 1 - 12 ��/���
   unsigned short          :11;       //
// WORD 5+3
   unsigned short HeightAnt:10;  // ������ �.�.��� (LSB 10m)
   unsigned short          :6;       //
// WORD 5+4
   unsigned short Eps      :14;    // ��o� ������� ������� (LSB 360/16384)
   unsigned short EpsSign  :1;   // ���� ������ ���� ������� �������
   unsigned short          :1;
// WORD 5+5(8)   - ������� ����� ����� ��������������� ������ ������������� �������
   LTime Time_ms;    // �����������
// WORD 5+9   - ������ �������
   unsigned short FairChan :12;
   unsigned short          :4;
};

// ----------
// ��������� �������, ��� - 0x02, ����� - 8 + 24 ����
struct SPRLMark{
// WORD 1...5
   CodogrHeader  Header;
// WORD 5+1
   unsigned short         :4;
   unsigned short PBitTren   :1;       // ��������� �������
   unsigned short PBitWideBar:1;       // ������� ��������������� �����������
   unsigned short PBitNarrowBar  :1;       // ������� ������������� �����������
   unsigned short         :9;
// WORD 5+2
   unsigned short Azimuth :14;    // ������ ������� (LSB 360/16384)
   unsigned short         :2;       //
// WORD 5+3
   unsigned short Range   :14;    // ��������� ������� (LSB 50m)
   unsigned short         :2;       //
// WORD 5+4
   unsigned short Elev    :14;       // ������ �������� ���� ����� ������� (LSB 360/16384)
   unsigned short ElevSign:1;    // ���� ���� ����� (1-�������������)
   unsigned short         :1;
// WORD 5+5
   unsigned short Height  :12;  // ������ (LSB 10m)
   unsigned short         :4;       //
// WORD 5+6
   unsigned short ArcWidth:14;    // ������ ����� (LSB 360/16384)
   unsigned short         :2;       //
// WORD 5+7
   unsigned short Ampl    :12;  // ���������
   unsigned short         :4;       //
// WORD 5+8
   unsigned short FilterN1:5;   // ����� ������� �� i-� �������
   unsigned short         :1;
   unsigned short FilterN3:5;
   unsigned short         :1;
   unsigned short NumberChannel:4;   // ����� ������
// WORD 5+9
   unsigned short FilterN0:5;
   unsigned short         :1;
   unsigned short FilterN2:5;
   unsigned short         :1;
   unsigned short TurnRate:1;    // �������� �������� ������� (6\12 ��������)
   unsigned short         :3;
// WORD 5+10
   unsigned short VRad    :12;    // ���������� �������� 1�/�)
   unsigned short VRadSign:1;
   unsigned short PBitVRad:1;
   unsigned short         :2;
// WORD 5+11
   unsigned short NumberMark:16;
};
// ----------
// ��������� �������, ��� - 0x03, ����� - 8 + 18 ����
struct SVRLMark{
// WORD 1...5
   CodogrHeader  Header;
// WORD 5+1
   unsigned short                    :8;       //
   unsigned short AnswerIdentif      :5;       //��� ������ �����������
   unsigned short                    :2;       //
   unsigned short PBitTren           :1;       // ��������� �������
// WORD 5+2
   unsigned short Azimuth            :12;    // ������ ������� (LSB 360/4096)
   unsigned short                    :4;       //
// WORD 5+3
   unsigned short Range              :12;    // ��������� ������� (LSB 250m)
   unsigned short                    :4;       //
// WORD 5+4
   unsigned short ArcWidth           :12;       // ������ ������� �� ������� (LSB 360/4096)
   unsigned short                    :4;
// WORD 5+5  �������� �����
   unsigned short SideNumber         :16;  // 15 ������� ��������
// WORD 5+6
   unsigned short SideNumberSt       :1;  // ����� ������� ������ ��������� ������
   unsigned short PBitSideNumber     :1;  // ������� ������� ���.� �������� ������
   unsigned short                    :6;
   unsigned short StockFuel          :7;  //  ����� ������� � %
   unsigned short PBitStockFuel      :1;  // ������� ������� ���. � ������ �������
// WORD 5+7
   unsigned short BarHeight             :12;  // ������ (�.�.�. 10 �)
   unsigned short PBitBarHeight         :1;  // �������   (1 - ����,0 - ���)
   unsigned short TypeBarHeight      :1;  // �������  ��������������� ������  (1 - ���.,0 - ���.)
   unsigned short Calamity           :1;  // ������ "��������" (1- ����, 0 -���)
   unsigned short PBitCalamity       :1;  // ������� ������� ������� "��������"

// WORD 5+8
   unsigned short NumberMark         :16;
};

// ----------
// ������, ��� - 0x04, ����� - 8 + 10 ����
struct SPeleng{
// WORD 1...5
   CodogrHeader  Header;
// WORD 5+1
   unsigned short Azimuth        :14;    // ������ ������� (LSB ?)
   unsigned short                :1;
   unsigned short TrenazhPeleneg :1;       // ��������� ������
// WORD 5+2
   unsigned short HalfArcWidth   :14;       //�������� ������ ������� �� ������� (LSB ?)
   unsigned short                :2;       //
// WORD 5+3
   unsigned short Elev           :14;       // ���� �����  (LSB ?)
   unsigned short                :2;
// WORD 5+4
   unsigned short NumberMark     :16;
};
// ----------
// ������, ��� - 0x05, ����� - 8 + (4...1086) ����
struct SStrob{
// WORD 1...5
   CodogrHeader  Header;
// WORD 5+1
   unsigned short PBitCirculStrob    :1;    // ������� ������� ��������� ������
   unsigned short NumberStrobAZ      :4;       //
   unsigned short                    :3;
   unsigned short NumberStrobBlank   :8;       //
// WORD 5+2
   unsigned short RangeStrob         :14;    // ��������� ��������� ������ (LSB 100m)
   unsigned short                    :2;       //
};
// ----------

struct ForStrobQNX { // ����� ��� ������� �� � ������� �������������
/* WORD 1*/
   unsigned short RangeCentre   :8;// �����. ������ ������ (��/������)(���-2��)
   unsigned short HalfWidthRange :8;// �������� ������ �� ���������
/* WORD 2*/
   unsigned short AzimuthCentre  :8;// ������ ������ ������ (��/������)(���-2��)
   unsigned short HalfWidthAz    :8;// �������� ������ �� �������
};
// ----------
// ������� ��������� ��� - 0x06, ����� - 16 ����
struct SCommand{
// WORD 1...5
   CodogrHeader  Header;
// WORD 5+1
   unsigned short Cod     :8;
   unsigned short         :8;
// WORD 5+2
   unsigned short Azimuth :14;    // ������ ������� (LSB 360/16384)
   unsigned short         :2;       //
// WORD 5+3
   unsigned short Range   :14;    // ��������� ������� (LSB 50m)
   unsigned short         :2;       //
// WORD 5+4
   unsigned short Word1:16;    // ���������� � �������
// WORD 5+5
   unsigned short Word2:16;    // ���������� � �������
// WORD 5+6
   unsigned short Word3:16;    // ���������� � �������
// WORD 5+7
   unsigned short Word4:16;    // ���������� � �������
// WORD 5+8
   unsigned short Word5:16;    // ���������� � �������
};
// ----------
// ��������� ��� ��� - 0x07, ����� - 8 + 6 ����
struct SNRZ{
// WORD 1...5
   CodogrHeader  Header;
// WORD 5+1
   unsigned short   Includ :8;
/*   unsigned short IndManip :1;//��������� ����� (1 - ���������� ��� �������, 0 - ���������� ��� ��������)
   unsigned short Strobir  :1;  //�������    (1- ��������)
   unsigned short AirOrShip:1;//������� ��� �������   (1- �������, 0- �������)
   unsigned short P2       :1;//����� 2   (1- �������)
   unsigned short P3       :1;//����� 3   (1- �������)
   unsigned short P4_6     :1; //����� 4/6    (1- �������)
   unsigned short Number   :1;   // �����
   unsigned short Height   :1;   // ������*/
   unsigned short          :8;
// WORD 5+2
   unsigned short Azimuth   :14;    // ������ ��� (LSB 360/16384)
   unsigned short           :2;       //
};
// ----------
// ���������� � ������, ��� - 0x10, ����� - 8 + (36 ��� 44 ����)
struct STrace{
// WORD 1...5
   CodogrHeader  Header;
// WORD 5+1
   unsigned short TraceNo            :16;     // ����� ������
// WORD 5+2
   unsigned short Azimuth            :14;    // ������ ������ (LSB 360/16384)
   unsigned short                    :2;
// WORD 5+3
   unsigned short Range              :14;      // ��������� ��������� ������ (LSB 50 �)
   unsigned short                    :2;
// WORD 5+4
   unsigned short Elev               :14;       // ������ �������� ���� ����� ������ (LSB 360/16384)
   unsigned short ElevSign           :1;    // ���� ���� ����� (1-�������������)
   unsigned short                    :1;
// WORD 5+5
   unsigned short Height             :12;     // ������ ������ (LSB 50 �)
   unsigned short                    :4;
// WORD 5+6
   unsigned short Course             :14;     // ���� (LSB 360/16384)
   unsigned short                    :2;
// WORD 5+7
   unsigned short Speed              :12;      // �������� ������ (LSB 1 �/�)
   unsigned short                    :4;
// WORD 5+8
   unsigned short SignsTrack       :16;         // �������� ������
/*   unsigned short PRLCount           :3;            // �-�� ��������� ���
   unsigned short IconType           :4;             // ��� ����� ����������� ��������
   unsigned short Friend             :1;               // "����"
   unsigned short Enemy              :1;                // "�����"
   unsigned short PointType          :2;            // ��� �������
   unsigned short VRLCount           :3;       // �-�� ��������� ���
   unsigned short IndexPresent       :1;         // ������� ������� ������� ������
   unsigned short ImitTrace          :1;            // ��������� ������*/
// WORD 5+9
   unsigned short RadialSpeed        :12;         // ���������� ��������
   unsigned short RadialSpeedSign    :1;      // ���� ���������� �������� (1-������������)
   unsigned short PBitRadialSpeed    :1;   // ������� ������� ���������� ��������
   unsigned short                    :2;
// WORD 5+10
   unsigned short AzimuthSpeed       :13;    // ������������ �������� �� ������(LSB 360/16384)???????
   unsigned short AzimuthSpeedSign   :1;      // ����  ��������
   unsigned short                    :2;
// WORD 5+11  �������� �����
   unsigned short SideNumber         :16;  // 15 ������� �������� ��������� ������
// WORD 5+12
   unsigned short SideNumberSt       :1;  // ����� ������� ������ ��������� ������
   unsigned short PBitSideNumber     :1;  // ������� ������� ���.� �������� ������
   unsigned short                    :6;
   unsigned short StockFuel          :7;  //  ����� ������� � %
   unsigned short PBitStockFuel      :1;  // ������� ������� ���. � ������ �������
// WORD 5+13
   unsigned short BarHeight          :12;  // ������ (�.�.�. 10 �)
   unsigned short PBitBarHeight      :1;  // �������   (1 - ����,0 - ���)
   unsigned short TypeBarHeight      :1;  // �������  ��������������� ������  (1 - ���.,0 - ���.)
   unsigned short Calamity           :1;  // ������ "��������" (1- ����, 0 -���)
   unsigned short PBitCalamity       :1;  // ������� ������� ������� "��������"
// WORD 5+14
   unsigned short AzimuthET          :14;    // ������ ����������������� ����� �������������� �� ������ �������� ������� (LSB 360/16384)
   unsigned short                    :2;       //
// WORD 5+15
   unsigned short IncludIdentif      :8;
/*   unsigned short IndManip         :1;//��������� ����� (1 - ���������� ��� �������, 0 - ���������� ��� ��������)
   unsigned short Strobir            :1;  //�������    (1- ��������)
   unsigned short AirOrShip          :1;//������� ��� �������   (1- �������, 0- �������)
   unsigned short P2                 :1;//����� 2   (1- �������)
   unsigned short P3                 :1;//����� 3   (1- �������)
   unsigned short P4_6               :1; //����� 4/6    (1- �������)
   unsigned short Number             :1;   // �����
   unsigned short HeightTwo          :1;   // ������
  */
   unsigned short AnswerIdentif      :4;
/*   unsigned short GarantIdentif      :1;       // ��������������� �����������
   unsigned short Calamity           :1;       // ������ "��������"
   unsigned short IndIdentif         :1;       // �������������� �����������
   unsigned short CommonIdentif      :1;       // ����� �����������*/
   unsigned short                    :4;       //
// WORD 5+16(-19)
   unsigned char  Index[8];               // ������ ������ (���� ������ ���� ���������� ��� tr_IndexPresent)
};
// ----------
// ���������� � ������ �� �������, ��� - 0x11, ����� - 8 + 16 ����
struct SPelengTrace{
// WORD 1...5
   CodogrHeader  Header;
// WORD 5+1
   unsigned short TraceNo            :16;     // ����� ������
// WORD 5+2
   unsigned short Azimuth            :14;    // ������ ������ (LSB 360/16384)
   unsigned short                    :2;
// WORD 5+3
   unsigned short Range              :14;      // ��������� ��������� ������ (LSB 50 �)
   unsigned short                    :2;
// WORD 5+4
   unsigned short Elev               :14;       // ������ �������� ���� ����� ������ (LSB 360/16384)
   unsigned short ElevSign           :1;    // ���� ���� ����� (1-�������������)
   unsigned short                    :1;
// WORD 5+5
   unsigned short Width              :14;    // ������ ������� �� ������� (LSB 360/16384)
   unsigned short                    :2;       //
// WORD 5+6
   unsigned short AzimuthSpeed       :13;    // ������������ �������� �� ������(LSB 360/16384)???????
   unsigned short AzimuthSpeedSign   :1;      // ����  ��������
   unsigned short                    :2;
// WORD 5+7
   unsigned short Pn                 :1;      // ������� �������, �������� ��� ������ ������ ������ �� �������
   unsigned short NewElev            :1;     //���������� ���� ����� (��������� �������)
   unsigned short Te                 :1;      // ���������� ������ �� ������� ������������������
   unsigned short                    :12;
   unsigned short T                  :1;       //��������� ������ �� �������
};


union SCu02Codogr{
   unsigned char  RawData[MAX_CUCODOGR_LEN];

   // ��� ������-���������� (������������ ������ ��� ����
   // ����� ����� ���� ������ ��� ���������� � ��������� ���������)
   struct {
      CodogrHeader Header;
   };

   SAm            Am;
   SPRLMark       PRL;
   SVRLMark       VRL;
   SPeleng        Peleng;
   SStrob         Strob; // ?????????????
   SCommand       Command;
   SNRZ           NRZ;
   STrace         Trace;
   SPelengTrace   PelTr;
   ForStrobQNX    ForStrob[10 + 4 + MAX_CUCODOGR_LEN ];           //�����?
};
// ----------
// ���������� �� ������� ��� Track, ��� - 0x??, ����� -  ?? ����
// ������� ������� (1 -  ����, 0 -  ���)
struct SMarkForTrack{
/* WORD 1,2*/     unsigned char   Type;            // ��� �������
                  unsigned char   Sourse[3];       // �������� ����������
/* WORD 3*/       unsigned short  NumberMark;      // ����� �������
/* WORD 4-7 */    LTime    Time;            // ����� � ������������ !!!!!
/* WORD 8*/       unsigned short  PBitAzimuth;     // ������� ������� �������
/* WORD 9-12*/    double          Azimuth;         // ������
/* WORD 13*/      unsigned short  PBitRange;       // ������� ������� ���������
/* WORD 14-17*/   double          Range;           // ���������
/* WORD 18-21*/   double          X;               // ���������� �
/* WORD 22-25*/   double          Y;               // ���������� Y
/* WORD 26*/      unsigned short  PBitElev ;       // ������� ������� ���� �����
/* WORD 27-30*/   double          Elev;            // ���� �����
/* WORD 31*/      unsigned short  PBitHeight;      // ������� ������� ������
/* WORD 32-35*/   double          Height;          // ������
/* WORD 36*/      unsigned short  PBitLat;         // ������� ������� ������
/* WORD 37-40*/   double          Lat;             // ������
/* WORD 41*/      unsigned short  PBitLon;         // ������� ������� �������
/* WORD 42-45*/   double          Lon;             // �������
/* WORD 46*/      unsigned short  PBitVRad;        // ������� ������� ���.��-��
/* WORD 47-50*/   double          VRad;            // ���������� ��������
/* WORD 51-52*/   unsigned char   Filter0;         // ����� ������� 0
                  unsigned char   Filter1;         // ����� ������� 1
                  unsigned char   Filter2;         // ����� ������� 2
                  unsigned char   Filter3;         // ����� ������� 3
/* WORD 53*/      unsigned short  PBitArcWidth;    // ������� ������� ��� �����
/* WORD 54-57*/   double          ArcWidth;        // ������ �����
/* WORD 58*/      unsigned short  PBitAmpl;        // ������� ������� ���������
/* WORD 59-62*/   double          Ampl;            // ���������
                //  �������� PrimaryBit: (������� � �������� ����)
                //            ����� ������ - 4 ���
                //            ��������     - 1 ���
                //            ��������� ������� - 1 ���
                //            �������������� ����������� - 1 ���
                //            ������������ ����������� - 1 ���
/* WORD 63-64*/   unsigned int    PrimaryBit;      // ��������
/* WORD 65*/      unsigned short  PBitBarHeight;   // ������� ������� ���.������
/* WORD 66-69*/   double          BarHeight;       // ��������������� ������
/* WORD 70*/      unsigned short  PBitNumber1;     // ������� ������� ���.� ���1
/* WORD 71-74*/   unsigned char   SideNumber1[8];  // �������� ����� 1
/* WORD 75*/      unsigned short  PBitNumber2;     // ������� ������� ���.� ���2
/* WORD 76-79*/   unsigned  char  SideNumber2[8];  // �������� ����� 2
/* WORD 80*/      unsigned short  PBitNumber3;     // ������� ������� ���.� ���3
/* WORD 81-84*/   unsigned char   SideNumber3[8];  // �������� ����� 3
/* WORD 85*/      unsigned short  PBitStockFuel;   // ������� ������� � ��� ����
/* WORD 86*/      unsigned short  StockFuel;       // ����� �������
/* WORD 87*/      unsigned short  BitAnswer;       // ������� ������� ���.�� ��� �����
/* WORD 88*/      unsigned short  AnswerIdentif;   // ��� ������ �����������
/* WORD 89*/      unsigned short  BitInclud;       // ������� ������� ���.� ��� �����
/* WORD 90*/      unsigned short  IncludIdentif;   // ��� ����������� �����������
/* WORD 91-92*/   unsigned int    MarkIdentif;     // �������� ������� �������:
// ������� � �������� ���� :      - ��� ��������������� ������ (1 ����������, 0 - �������������)
                            //    - ���������� � �������� �� (1 - ���� ��������, 0 -���)
                            //  - ������� ������� ���������� � �������� ���� (1 - ���� ����������, 0 -���)
/* WORD 91*/      unsigned short  PBitHeightRLS;   // ������� ������� ��� �� ���
/* WORD 92-95*/   double          HeightRLS;       // ������ ����� ������� ���
};
// ----------
// ���������� � ������ ��� QNX, ��� - 0x??, ����� -  ?? ����
// ������� ������� (1 -  ����, 0 -  ���)
struct STraceFromTrack{

/* WORD 1,2*/     unsigned short NumberTrack;      // ����� �����
/* WORD 3*/       unsigned short  PBitAzimuth;     // ������� ������� �������
/* WORD 4-6*/     double          Azimuth;         // ������ ������ (���)
                  double          AzimuthET;         // ������ �� (���)
/* WORD 7*/       unsigned short  PBitRange;       // ������� ������� ���������
/* WORD 8-11*/    double          Range;           // ��������� ������
/* WORD 12-15*/   double          X;               // ���������� �
/* WORD 16-19*/   double          Y;               // ���������� Y
/* WORD 20*/      unsigned short  PBitElev ;       // ������� ������� ���� �����
/* WORD 21-24*/   double          Elev;            // ���� ����� ������
/* WORD 25*/      unsigned short  PBitHeight;      // ������� ������� ������
/* WORD 26-29*/   double          Height;          // ������ ������
/* WORD 30*/      unsigned short  PBitVRad;        // ������� ������� ���.��-��
/* WORD 31-34*/   double          VRad;            // ���������� ��������
/* WORD 35*/      unsigned short  PBitVAz;         // ������� ������� ��.��-��
/* WORD 36-39*/   double          VAz;             // �������� ������������
/* WORD 40*/      unsigned short  PBitVCourse;     // ������� ������� ����.��-��
/* WORD 41-44*/   double          VCourse;         // �������� ��������
/* WORD 45-48*/   double          Course;          // ���� (���)
/* WORD 49*/      unsigned short  PBitIndex;        // ������� ������� �������
/* WORD 50-59*/   unsigned char   Index[10];        // ������ ������
/* WORD 60*/      unsigned int    SignsTrack;      //  �������� ������
/* 0 -� ���� (������� � �������� �������) :
    - ���-�� ��������� ��� (3 �������)
    - ���� ����������� �������� (4 �������)
    - � - "����" (1 ������)
   1 -� ���� (������� � �������� �������) :
    - � - "�����" (1 ������)
    - ��� ������� (2 �������)
    - ���-�� ��������� ��� (3 �������)
    - � - ������� ������� (1 ������)
    - � - ��������� ������ (1 ������)
   2 -�  ���� (������� � �������� �������) :
   - ��� - ��� ��������������� ������
   - �� -  ���������� � �������� (1 ��������, 0 - ���)
   - ���� - ������� ������� ���������� � �������� (1 - ���� ����������, 0 - ���)


/* WORD 61*/      unsigned short  PBitBarHeight;   // ������� ������� ���.������
/* WORD 62-65*/   double          BarHeight;       // ��������������� ������
/* WORD 66*/      unsigned short  PBitNumber1;     // ������� ������� ���.� ���1
/* WORD 67-70*/   unsigned char   SideNumber1[8];  // �������� ����� 1
/* WORD 71*/      unsigned short  PBitNumber2;     // ������� ������� ���.� ���2
/* WORD 72-75*/   unsigned  char  SideNumber2[8];  // �������� ����� 2
/* WORD 76*/      unsigned short  PBitNumber3;     // ������� ������� ���.� ���3
/* WORD 77-80*/   unsigned char   SideNumber3[8];  // �������� ����� 3
/* WORD 81*/      unsigned short  PBitStockFuel;   // ������� ������� � ���.����
/* WORD 82*/      unsigned short  StockFuel;       // ����� �������
/* WORD 83-86 */  LTime    Time;            // ����� � ������������
             // ������� ������������� (3 ���������, ������.� ������ ���)
/* WORD 87-88*/   unsigned char   Type1;            // ��� �������
                  unsigned char   Sourse1[3];       // �������� ����������
/* WORD 89*/      unsigned short  NumberMark1;      // ����� �������
/* WORD 90-93 */  LTime    Time1;            // ����� � ������������
/* WORD 94-95*/   unsigned char   Type2;            // ��� �������
                  unsigned char   Sourse2[3];       // �������� ����������
/* WORD 96*/      unsigned short  NumberMark2;      // ����� �������
/* WORD 97-100 */ LTime    Time2;            // ����� � ������������
/* WORD 101,102*/   unsigned char Type3;            // ��� �������
                  unsigned char   Sourse3[3];       // �������� ����������
/* WORD 103*/      unsigned short NumberMark3;      // ����� �������
/* WORD 104-107 */LTime   Time3;            // ����� � ������������
             // ������� ����������� (3 ���������, ������.� ������ ���)
/* WORD 108*/      unsigned short  IncludIdentif1;   // ��� ����� ����� ��� ���1
/* WORD 109*/      unsigned short  AnswerIdentif1;   // ��� ��� �����  ��� ���1
/* WORD 110-113*/  unsigned int    TimeIdentif1;     // ����� ����� ��� ���1
/* WORD 114*/      unsigned short  IncludIdentif2;   // ��� ����� ����� ��� ���2
/* WORD 115*/      unsigned short  AnswerIdentif2;   // ��� ��� �����  ��� ���2
/* WORD 116-119*/  unsigned int    TimeIdentif2;     // ����� ����� ��� ���2
/* WORD 120*/      unsigned short  IncludIdentif3;   // ��� ����� ����� ��� ���3
/* WORD 121*/      unsigned short  AnswerIdentif3;   // ��� ��� �����  ��� ���3
/* WORD 122-125*/  unsigned int    TimeIdentif3;     // ����� ����� ��� ���3

/* WORD 126-129*/  LTime    TimeExist;       // ����� ������������� ������
};
// ----------
// ���������� � ������� ��������� ��� Track, ��� - 0x??, ����� -  ?? ����
struct SCommandForTrack{
/* WORD 1,2*/     unsigned short  Cod;           // ��� �������
/* WORD 3-6*/     double          Azimuth;       // ������  (���)
/* WORD 7-10*/    double          Range;         // ���������
/* WORD 11-14*/   double          X;             // ���������� �
/* WORD 15-18*/   double          Y;             // ���������� Y
/* WORD 20,21*/   unsigned short  NumberTrack;   // ����� �����
/* WORD 22,23*/   unsigned short  NumberMark;    // ����� �������
/* WORD 24-33*/   unsigned char   Index[10];     // ������ ������
};
// ----------
// ���������� � ������� �� � ������� ��� Track, ��� - 0x??, ����� -  ?? ����
// ����� �������������� ����� ��������� ���������� �� 4 �� 1086 ����
struct SStrobForTrack{
/* WORD 1*/     unsigned short PBitCirculStrob; // ������� ������� ����.������
/* WORD 2*/     unsigned short NumberStrobAZ;   // ���-�� ������� ��
/* WORD 3*/     unsigned short NumberStrobBlank;// ���-�� ������� �����������.
/* WORD 4*/     unsigned short RangeStrob;      // ��������� ��������� ������
//  NumberStrobAZ ��������  ForStrob ��� ������� ��
//  NumberStrobBlank ��������  ForStrob ��� �������
};
struct ForStrob { // ����� ��� ������� �� � ������� �������������
/* WORD 1,2*/   double  AzimuthCentre; // ������ ������ ������ (��/������) (���)
/* WORD 3,4*/   double  RangeCentre;   // ��������� ������ ������ (��/������)(�)
/* WORD 5,6*/   double  HalfWidthAz;   // �������� ������ �� �������     (���)
/* WORD 7,8*/   double  HalfWidthRange;// �������� ������ �� ���������   (�)
};
#pragma pack(pop)
//---------------------------------------------------------------------------
#endif
