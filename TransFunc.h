//---------------------------------------------------------------------------
#include<vcl.h>
#ifndef Unit1H
char*   StrToHex(char* str1,int len1);
AnsiString HexToStr(AnsiString _In);
AnsiString FloatToStr(float _In,int _density=2);
AnsiString myFloatToStr(float _In,int _density=2);
float myStrToFloat(AnsiString _In,int _density=2);

#define Unit1H
//---------------------------------------------------------------------------
#endif
