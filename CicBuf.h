//---------------------------------------------------------------------------

#ifndef CicBufH
#define CicBufH
#include <vector.h>
//---------------------------------------------------------------------------
//template class<CCicBuf>

class CCicBuf{
private:
   unsigned long head;
public:
   long size;
   unsigned char* cData;
   unsigned short* wData;
   long ccount;
   long wcount;
   CCicBuf(unsigned long n);
   ~CCicBuf();
   unsigned short get_w(unsigned long n);
   unsigned char get_c(unsigned long n);
   void Addc(unsigned char* cbuf,unsigned long sz);
   void Addw(unsigned short* wbuf,unsigned long sz);
   void erasec(int,int);
   void Clear();
};
#endif
