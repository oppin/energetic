//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//USERES("Energetic3.res");
USEFORM("Osc3.cpp", FMain);
USEUNIT("dsutil.cpp");
USELIB("dsound.lib");
USEUNIT("TransFunc.cpp");
USEUNIT("CicBuf.cpp");
USEFORM("FAsk.cpp", FAskName);
USE("g.bmp", File);
USE("i.bmp", File);
USEFORM("ViewUnit.cpp", DBView);
USEFORM("FParam.cpp", ParForm);
USEFORM("FGarm.cpp", GarmForm);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
   try
   {
       Application->Initialize();
       Application->CreateForm(__classid(TFMain), &FMain);
     Application->CreateForm(__classid(TFAskName), &FAskName);
     Application->CreateForm(__classid(TDBView), &DBView);
     Application->CreateForm(__classid(TParForm), &ParForm);
     Application->CreateForm(__classid(TGarmForm), &GarmForm);
     Application->Run();
      
   }
   catch (Exception &exception)
   {
       Application->ShowException(&exception);
   }
   return 0;
}
//---------------------------------------------------------------------------
