//---------------------------------------------------------------------------

#include "TransFunc.h"
#include <math.h>

char*   StrToHex(char* str1,int len1)
{
  int   len2=int(len1/2);
  char* s;
//union  S_hex{int* i2;char* c1;char* c2[1];} kl;kl.c1=str1;

  char* exst =new char[len2];
  str1=StrLower(str1);
     for(int i=0;i<len2;i++){
      switch(str1[i*2]){
        case 'f': exst[i]=15;break;
        case 'e': exst[i]=14;break;
        case 'd': exst[i]=13;break;
        case 'c': exst[i]=12;break;
        case 'b': exst[i]=11;break;
        case 'a': exst[i]=10;break;
        default :
                  char c=str1[i*2];
                  if((c<58)&&(c>47))
                    exst[i]=exst[i]*16+StrToInt(c);
                  else        exst[i]=exst[i]*16+'0';
      }//switch
      switch(str1[i*2+1]){
        case 'f': exst[i]=exst[i]*16+15;break;
        case 'e': exst[i]=exst[i]*16+14;break;
        case 'd': exst[i]=exst[i]*16+13;break;
        case 'c': exst[i]=exst[i]*16+12;break;
        case 'b': exst[i]=exst[i]*16+11;break;
        case 'a': exst[i]=exst[i]*16+10;break;
        default :
                  char c=str1[i*2+1];
                  if((c<58)&&(c>47))
                    exst[i]=exst[i]*16+StrToInt(c);
                  else        exst[i]=exst[i]*16+'0';
      }//switch
     }//for
     return exst;
}
AnsiString HexToStr(AnsiString _In){
   AnsiString Stmp,LI;
   char* bbuf=_In.c_str();
         for(int _i=0;_i<_In.Length();_i++){
            Stmp="";
            Stmp.sprintf("%02x ",bbuf[_i]);
            if(Stmp.Length()>2)
                Stmp=Stmp.SubString(Stmp.Length()-2,2);
            LI=LI+Stmp+" ";
         }//for
         LI=LI.UpperCase();
   return LI;
}
AnsiString myFloatToStr(float _In,int _density){
  return FloatToStr(_In,_density);
}
AnsiString FloatToStr(float _In,int _density){
AnsiString _Out;
int   itmp,i=0;
bool neg=false;
      if(_In<0){neg=true;_In=-_In;}
      itmp=_In;_In=_In-itmp;
      _Out=IntToStr(itmp)+'.';
      while((_In)&&(i<_density)){
         itmp=_In=10*_In;
         _In=_In-itmp;
         i++;
         _Out=_Out+IntToStr(itmp);
      }//while
if(neg) _Out="-"+_Out;
return _Out;

}
float myStrToFloat(AnsiString _In,int _density){
int   i=1;
float out;
bool neg=false;
AnsiString Tmp;
      if(_In<0){neg=true;_In.Delete(1,1);}
      Tmp=_In;
      Tmp.Delete(Tmp.AnsiPos("."),Tmp.Length()-Tmp.AnsiPos(".")+1);
      out=StrToInt(Tmp);
      Tmp=_In;
      Tmp.Delete(1,Tmp.AnsiPos("."));
      if(Tmp.Length()>_density){
         Tmp.Delete(_density,Tmp.Length()-_density);
      }//if
      float div=pow10(Tmp.Length());
      if(Tmp.Length()) out=out + (float)StrToInt(Tmp)/div;
if(neg)return -out;
else return out;
}

//---------------------------------------------------------------------------


