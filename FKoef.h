//---------------------------------------------------------------------------

#ifndef FKoefH
#define FKoefH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "Osc3.h"
//---------------------------------------------------------------------------
class TKoefForm : public TForm
{
__published:	// IDE-managed Components
    TEdit *Eksc;
    TEdit *EGarm;
    TEdit *EEth;
    TLabel *Label1;
    TLabel *Label2;
    TLabel *Label3;
    TEdit *EKUa;
    TLabel *Label4;
    TEdit *EKUb;
    TLabel *Label5;
    TEdit *EKUc;
    TLabel *Label6;
    TEdit *EKIa;
    TLabel *Label7;
    TEdit *EKIb;
    TLabel *Label8;
    TEdit *EKIc;
    TLabel *Label9;
    TEdit *EKUab;
    TLabel *Label10;
    TEdit *EKUbc;
    TLabel *Label11;
    TEdit *EKUca;
    TLabel *Label12;
    TLabel *Label13;
    void __fastcall EkscChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TKoefForm(TComponent* Owner);
    int SetAll(TFMain* _ptr);
};
//---------------------------------------------------------------------------
extern PACKAGE TKoefForm *KoefForm;
//---------------------------------------------------------------------------
#endif
