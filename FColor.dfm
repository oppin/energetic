object ColorForm: TColorForm
  Left = 383
  Top = 73
  Width = 330
  Height = 516
  Caption = '�����'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Visible = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label19: TLabel
    Left = 0
    Top = 0
    Width = 91
    Height = 13
    Caption = '--------- ���� A,B,C'
  end
  object Label1: TLabel
    Left = 16
    Top = 48
    Width = 15
    Height = 13
    Caption = 'UA'
  end
  object Label2: TLabel
    Left = 16
    Top = 96
    Width = 15
    Height = 13
    Caption = 'UC'
  end
  object Label3: TLabel
    Left = 16
    Top = 72
    Width = 15
    Height = 13
    Caption = 'UB'
  end
  object UASh: TShape
    Left = 57
    Top = 45
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object Label4: TLabel
    Left = 32
    Top = 136
    Width = 20
    Height = 13
    Caption = 'UA i'
  end
  object Label5: TLabel
    Left = 32
    Top = 184
    Width = 20
    Height = 13
    Caption = 'UC i'
  end
  object Label6: TLabel
    Left = 32
    Top = 160
    Width = 20
    Height = 13
    Caption = 'UB i'
  end
  object Label20: TLabel
    Left = 16
    Top = 24
    Width = 74
    Height = 13
    Caption = '----����������'
  end
  object Label21: TLabel
    Left = 24
    Top = 112
    Width = 76
    Height = 13
    Caption = '-----  ���������'
  end
  object Label22: TLabel
    Left = 160
    Top = 232
    Width = 22
    Height = 13
    Caption = 'UAB'
  end
  object Label23: TLabel
    Left = 160
    Top = 280
    Width = 22
    Height = 13
    Caption = 'UCA'
  end
  object Label24: TLabel
    Left = 160
    Top = 256
    Width = 22
    Height = 13
    Caption = 'UBC'
  end
  object Label25: TLabel
    Left = 176
    Top = 320
    Width = 27
    Height = 13
    Caption = 'UAB i'
  end
  object Label26: TLabel
    Left = 176
    Top = 368
    Width = 27
    Height = 13
    Caption = 'UCA i'
  end
  object Label27: TLabel
    Left = 176
    Top = 344
    Width = 27
    Height = 13
    Caption = 'UBC i'
  end
  object Label28: TLabel
    Left = 144
    Top = 184
    Width = 158
    Height = 13
    Caption = '--------- �����������  AB,BC,CA'
  end
  object Label29: TLabel
    Left = 160
    Top = 208
    Width = 74
    Height = 13
    Caption = '----����������'
  end
  object Label30: TLabel
    Left = 168
    Top = 296
    Width = 76
    Height = 13
    Caption = '-----  ���������'
  end
  object Label7: TLabel
    Left = 16
    Top = 232
    Width = 10
    Height = 13
    Caption = 'IA'
  end
  object Label8: TLabel
    Left = 16
    Top = 280
    Width = 10
    Height = 13
    Caption = 'IC'
  end
  object Label9: TLabel
    Left = 16
    Top = 256
    Width = 10
    Height = 13
    Caption = 'IB'
  end
  object Label10: TLabel
    Left = 32
    Top = 320
    Width = 15
    Height = 13
    Caption = 'IA i'
  end
  object Label11: TLabel
    Left = 32
    Top = 368
    Width = 15
    Height = 13
    Caption = 'IC i'
  end
  object Label12: TLabel
    Left = 32
    Top = 344
    Width = 15
    Height = 13
    Caption = 'IB i'
  end
  object Label13: TLabel
    Left = 16
    Top = 208
    Width = 38
    Height = 13
    Caption = '---- ����'
  end
  object Label14: TLabel
    Left = 24
    Top = 296
    Width = 76
    Height = 13
    Caption = '-----  ���������'
  end
  object UBSh: TShape
    Left = 57
    Top = 69
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object UCSh: TShape
    Left = 57
    Top = 93
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object UAgSh: TShape
    Left = 65
    Top = 133
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object UBgSh: TShape
    Left = 65
    Top = 157
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object UCgSh: TShape
    Left = 65
    Top = 181
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object IASh: TShape
    Left = 57
    Top = 229
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object IBSh: TShape
    Left = 57
    Top = 253
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object ICSh: TShape
    Left = 57
    Top = 277
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object IAgSh: TShape
    Left = 65
    Top = 317
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object IBgSh: TShape
    Left = 65
    Top = 341
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object ICgSh: TShape
    Left = 65
    Top = 365
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object UABSh: TShape
    Left = 201
    Top = 229
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object UBCSh: TShape
    Left = 201
    Top = 253
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object UCASh: TShape
    Left = 201
    Top = 277
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object UABgSh: TShape
    Left = 217
    Top = 317
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object UBCgSh: TShape
    Left = 217
    Top = 341
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object UCAgSh: TShape
    Left = 217
    Top = 365
    Width = 33
    Height = 17
    OnMouseDown = UAShMouseDown
  end
  object Panel1: TPanel
    Left = 144
    Top = 1
    Width = 166
    Height = 169
    TabOrder = 0
    object Label15: TLabel
      Left = 10
      Top = 8
      Width = 23
      Height = 13
      Caption = '���'
    end
    object Label16: TLabel
      Left = 13
      Top = 32
      Width = 20
      Height = 13
      Caption = '���'
    end
    object Label17: TLabel
      Left = 6
      Top = 128
      Width = 84
      Height = 13
      Caption = '��������� ��� I'
    end
    object Label18: TLabel
      Left = 5
      Top = 152
      Width = 89
      Height = 13
      Caption = '��������� ��� U'
    end
    object Label31: TLabel
      Left = 13
      Top = 80
      Width = 36
      Height = 13
      Caption = '������'
    end
    object FonSh: TShape
      Left = 57
      Top = 5
      Width = 33
      Height = 17
      OnMouseDown = UAShMouseDown
    end
    object AxisSh: TShape
      Left = 57
      Top = 29
      Width = 33
      Height = 17
      OnMouseDown = UAShMouseDown
    end
    object SpSh: TShape
      Left = 57
      Top = 77
      Width = 33
      Height = 17
      OnMouseDown = UAShMouseDown
    end
    object OXSh: TShape
      Left = 105
      Top = 125
      Width = 33
      Height = 17
      OnMouseDown = UAShMouseDown
    end
    object OYSh: TShape
      Left = 105
      Top = 149
      Width = 33
      Height = 17
      OnMouseDown = UAShMouseDown
    end
    object NetSh: TShape
      Left = 57
      Top = 53
      Width = 33
      Height = 17
      OnMouseDown = UAShMouseDown
    end
    object Label32: TLabel
      Left = 13
      Top = 56
      Width = 30
      Height = 13
      Caption = '�����'
    end
    object Label33: TLabel
      Left = 6
      Top = 104
      Width = 88
      Height = 13
      Caption = '��������� ��� T'
    end
    object TimeSh: TShape
      Left = 105
      Top = 101
      Width = 33
      Height = 17
      OnMouseDown = UAShMouseDown
    end
  end
  object ColorDialog1: TColorDialog
    Ctl3D = True
    Top = 8
  end
end
