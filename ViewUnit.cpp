//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ViewUnit.h"
#include "Osc3.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TDBView *DBView;
//---------------------------------------------------------------------------
__fastcall TDBView::TDBView(TComponent* Owner)
   : TForm(Owner)
{
  Deleted=false;
  Rename=false;
  NewName="";  
}
//---------------------------------------------------------------------------
void __fastcall TDBView::Button1Click(TObject *Sender)
{
 if(ListBox1->ItemIndex<0) MessageBox(this->Handle,"�� ������ ������� ������� ����������� ������","",MB_ICONEXCLAMATION);
 else Close();
}
//---------------------------------------------------------------------------
void __fastcall TDBView::Button2Click(TObject *Sender)
{
  ListBox1->ItemIndex=-1;
  Close();
}
//---------------------------------------------------------------------------
void __fastcall TDBView::BDelClick(TObject *Sender)
{
  Deleted=true;
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TDBView::Button3Click(TObject *Sender)
{
  Rename=true;
  Close();
}
//---------------------------------------------------------------------------

