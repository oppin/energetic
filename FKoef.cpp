//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FKoef.h"
#include "Osc3.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TKoefForm *KoefForm;
//---------------------------------------------------------------------------
__fastcall TKoefForm::TKoefForm(TComponent* Owner)
    : TForm(Owner)
{
}
int TKoefForm::SetAll(TFMain* _ptr){
/*___*/

    Eksc->Text=FloatToStr(_ptr->ksc);
    EGarm->Text=IntToStr(_ptr->_nFGar);
    EEth->Text=FloatToStr(_ptr->Eth_g);
    EKUa->Text=FloatToStr(_ptr->KUa);
    EKUb->Text=FloatToStr(_ptr->KUb);
    EKUc->Text=FloatToStr(_ptr->KUc);
    EKIa->Text=FloatToStr(_ptr->KIa);
    EKIb->Text=FloatToStr(_ptr->KIb);
    EKIc->Text=FloatToStr(_ptr->KIc);
    EKUab->Text=FloatToStr(_ptr->KUab);
    EKUbc->Text=FloatToStr(_ptr->KUbc);
    EKUca->Text=FloatToStr(_ptr->KUca);
/*___*/

}
//---------------------------------------------------------------------------
void __fastcall TKoefForm::EkscChange(TObject *Sender)
{
  FMain->ResetKoef();    
}
//---------------------------------------------------------------------------
