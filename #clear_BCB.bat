@echo off
if exist *.ilc del *.ilc 
if exist *.ild del *.ild 
if exist *.ilf del *.ilf 
if exist *.ils del *.ils 
if exist *.obj del *.obj 
if exist *.tds del *.tds 
if exist *.~* del *.~* 
if exist *.csm del *.csm
if exist *.dsw del *.dsw
if exist *.mbr del *.mbr
if exist *.mrt del *.mrt
if exist *.r$p del *.r$p
if exist *.mbt del *.mbt
if exist *.bak del *.bak
if exist *. del *.
if exist *.obr del *.obr
if exist *.dsk del *.dsk
exit