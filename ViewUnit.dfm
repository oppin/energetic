object DBView: TDBView
  Left = 263
  Top = 121
  Width = 354
  Height = 255
  Caption = 'DBView'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ListBox1: TListBox
    Left = 0
    Top = 0
    Width = 345
    Height = 169
    ItemHeight = 13
    TabOrder = 0
  end
  object Button1: TButton
    Left = 14
    Top = 184
    Width = 75
    Height = 25
    Caption = 'Open'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 254
    Top = 184
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 2
    OnClick = Button2Click
  end
  object BDel: TButton
    Left = 94
    Top = 184
    Width = 75
    Height = 25
    Caption = 'Delete'
    TabOrder = 3
    OnClick = BDelClick
  end
  object Button3: TButton
    Left = 174
    Top = 184
    Width = 75
    Height = 25
    Caption = 'Rename'
    TabOrder = 4
    OnClick = Button3Click
  end
end
